% clear all
close all
clc


%specify which sections to run
runsection=5;

%use fixed or varying reference (where supported)
varying_reference=false;

%use ForcesPro?%
 bForces=0;
 
 T=5; %simulation length
    

% adding the subfolders to the path
addpath(genpath('functions'))
addpath(genpath('data'))
addpath(genpath('../FORCES_client'))

% loads:
%    hovering equilibrium (xs,us)
%    continuous time matrices Ac,Bc of the linearization
%    matrices sys.A, sys.B of the inner-loop discretized with sampling period sys.Ts
%    outerController optimizer instance for the outer controller
load('quadData.mat')
outerController = getOuterController(Ac);
disp('Data successfully loaded')



%%%%%%%%%%%%%%%% ADD YOUR CODE BELOW THIS LINE %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%constants%%
N=30; %timehorizon
dZMax=1;
alphaMax=10*pi/180;
betaMax=10*pi/180;
dAlphaMax=15*pi/180;
dBetaMax=15*pi/180;
dGammaMax=60*pi/180;

XMax=[dZMax; alphaMax; betaMax; inf; dAlphaMax; dBetaMax; dGammaMax];
XMin=-XMax;
UMin=0-us;
UMax=1-us;

%%Objective Parameters
lam = 0.01;
Q = 100 * diag([1 1 1 lam lam lam lam]);
R = 0.1*eye(4);
    

P = dare(sys.A, sys.B, Q, R);


% Invariant set for linearized autonomous system (around us)
system=LTISystem('A', sys.A, 'B', sys.B, 'Ts', sys.Ts);
system.x.min=XMin;
system.x.max=XMax;
system.u.min=UMin;
system.u.max=UMax;


%terminal set
system.x.penalty=QuadFunction(Q);
system.u.penalty=QuadFunction(R);
Pn=system.LQRPenalty;

InvSet = system.LQRSet;
A_term = InvSet.A;
b_term = InvSet.b;

%statevariables (sdpvar) used for any problem
x1 = sdpvar(7, N); %now for the whole trajectory
u = sdpvar(4, N-1);
r1 = sdpvar(4,1);
dbar = sdpvar(7,1);
ulast = sdpvar(4,1);
epsu = sdpvar(4,N);
epsx = sdpvar(7,N);


%basic constraints
constraints = [];
for i = 1:N
    %state constraint
    constraints = [constraints, (XMin <= x1(:,i) <= XMax)];
 
    if (i<N)
        %u constraints%
        constraints = [constraints, (UMin <= u(:,i) <= UMax)];
        %update constraints%
        constraints = [constraints, (x1(:,i+1)==sys.A*x1(:,i)+sys.B*u(:,i))];
    end
end

%disturbed constraints
disturbedConstraints = [];
for i = 1:N
    %state constraint
    disturbedConstraints = [disturbedConstraints, (XMin+dbar <= x1(:,i) <= XMax-dbar)];
 
    if (i<N)
        %u constraints%
        disturbedConstraints = [disturbedConstraints, (UMin <= u(:,i) <= UMax)];
        %update constraints%
        disturbedConstraints = [disturbedConstraints, (x1(:,i+1)==sys.A*x1(:,i)+sys.B*u(:,i)+dbar)];
    end
end

%soft constraints
softConstraints = [];
for i = 1:N
    %state constraint
    softConstraints = [softConstraints, (XMin+dbar-epsx(:,i) <= x1(:,i) <= XMax-dbar+epsx(:,i))];
 
    if (i<N)
        %u constraints%
        %softConstraints = [softConstraints, (UMin-epsu(:,i) <= u(:,i) <= UMax+epsu(:,i))];
        softConstraints = [softConstraints, (UMin<=u(:,i)<=UMax)];
        %update constraints%
        softConstraints = [softConstraints, (x1(:,i+1)==sys.A*x1(:,i)+sys.B*u(:,i)+dbar)];
    end
end

%outputmatrix for reference tracking%
Ctracking=[eye(4), zeros(4,3)];

%matrix for estimator%
Cest=eye(7);

%matrices for augmented system%
Aaug = [sys.A, eye(7); zeros(7), eye(7)];
Baug = [sys.B;zeros(7,4)];
Caug=[eye(7), eye(7)];


%filter for disturbed 
%this would be the propper way of doing it but doesn't work rn
%     sigma=dare(Aaug, Baug,[Q,zeros(7);zeros(7),10*eye(7)],R);
%     L=(Caug*sigma*Caug'+eye(7))\[sys.A, eye(7);zeros(7), eye(7)]*sigma*Caug';
%Define the observer matrix, stabilize using pole placing
poles = [0.85 * ones(7, 1);0.7*ones(7,1)];
L = place([sys.A, Cest;zeros(7), Cest]',Caug',poles);
L = L';
Af=Aaug-L*Caug;
Bf=[Baug,L];

filter.Af=Af;
filter.Bf=Bf;
    
%reference values%
%fixed reference
if(~varying_reference)
    %%computing steady state%%
    refval=[1;0.1745;-0.1745;1.7453];
end
%varying reference%
if(varying_reference)
    refval=[];
    for i = 1:T/sys.Ts+1
        refval=[refval, [1;0.1745*sin(sys.Ts*i);-0.1745*sin(sys.Ts*i);1.7453]];
    end
end

%reference values (not equal to constraint so offsetfree is feasible)%
%fixed reference
if(~varying_reference)
    %%computing steady state%%
    refval2=[0.8;0.12;-0.12;pi/2];
end
%varying reference%
if(varying_reference)
    refval2=[];
    for i = 1:T/sys.Ts+1
        refval2=[refval2, [0.8;0.12*sin(sys.Ts*i);-0.12*sin(sys.Ts*i);pi/2]];
    end
end
     
%%%%%%%%%%%%%%%%%%%%%    First MPC controller %%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(any(runsection==1))
    fprintf('PART I - First MPC controller...\n')
    firstMPCobjective = 0;
    firstMPCconstraints=constraints;
    for i = 1:(N-1)
        firstMPCobjective = firstMPCobjective + x1(:,i)'*Q*x1(:,i)+u(:,i)'*R*u(:,i);
    end
    %terminal cost%
    firstMPCobjective = firstMPCobjective + x1(:,N)'*P*x1(:,N);
    %terminal constraint
    firstMPCconstraints=[firstMPCconstraints, (A_term*x1(:,N)<=b_term)]; 

    initialState=[-1, 0.1745, -0.1745, 0.8727, 0, 0, 0]';
    initialControl=us;

    %initial state
    x0=initialState;
    options = sdpsettings; %??
    innerController = optimizer(firstMPCconstraints, firstMPCobjective, options, x1(:,1), u(:,1));
    [xt, ut, t, rt] = simQuad( sys, innerController, bForces, x0, T);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%  Reference Tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(runsection==2)||any(runsection==3)||any(runsection==8))
    fprintf('PART II - Reference tracking...\n')
    
    %compute steady state
    LHS=[eye(7)-sys.A, -sys.B; eye(4), zeros(4,7)]; 
    RHS=[zeros(7,1);r1];
    sol=LHS\RHS;
    xsr=sol(1:7);
    usr=sol(8:end);
    
    
     %%Constraints%%
    refTrackingConstraints = constraints;
    refTrackingObjective = 0;
    for i = 1:(N-1)
        refTrackingObjective = refTrackingObjective + (x1(:,i)'-xsr')*Ctracking'*Q(1:4,1:4)*Ctracking*(x1(:,i)-xsr)+(u(:,i)'-usr')*R*(u(:,i)-usr);
    end
    refTrackingObjective = refTrackingObjective + (x1(:,i)'-xsr')*Ctracking'*P(1:4,1:4)*Ctracking*(x1(:,i)-xsr);


    initialState=[0, 0, 0, 0, 0, 0, 0]';
    initialControl=us;
    %initial state
    x0=initialState;
    options = sdpsettings('solver','quadprog'); %??
    trackingController = optimizer(refTrackingConstraints, refTrackingObjective, options, [x1(:,1); r1], u(:,1));
    if(any(runsection==8))
        codeoptions=getOptions('trackingControllerForces');
        trackingControllerForces=optimizerFORCES(refTrackingConstraints, refTrackingObjective,codeoptions,[x1(:,1); r1],u(:,1),{'xinint','ref'},{'u0'});
    end
    if(any(runsection==2))
        [xt, ut, t, rt] = simQuad( sys, trackingController, bForces, x0, T, refval);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%  First simulation of the nonlinear model %%%%%%%%%%%%%%%%%
if(any(runsection==3))
    fprintf('PART III - First simulation of the nonlinear model...\n')
    innerController=trackingController;
    sim('simulation1');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%  Offset free MPC  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(runsection==4)||any(runsection==5)||any(runsection==8))
    fprintf('PART IV - Offset free MPC...\n')
     
    %%steady state under distrubance%
    %formulas from lecture
    LHS=[sys.A-eye(7), sys.B; Ctracking*Cest, zeros(4)];
    RHS=[-eye(7)*dbar; r1];
    sol=LHS\RHS;
    
    xsd=sol(1:7);
    usd=sol(8:end);
    

 
    offsetFreeConstraints = disturbedConstraints;

    offsetFreeObjective = 0;
    for i = 1:(N-1)
        offsetFreeObjective = offsetFreeObjective + (x1(:,i)'-xsd')*Ctracking'*Q(1:4,1:4)*Ctracking*(x1(:,i)-xsd)+(u(:,i)'-usd')*R*(u(:,i)-usd);
    end
    offsetFreeObjective = offsetFreeObjective + (x1(:,i)'-xsd')*Ctracking'*P(1:4,1:4)*Ctracking*(x1(:,i)-xsd);
    %terminal set
    offsetFreeConstraints=[offsetFreeConstraints, A_term*(x1(:,N)-xsd)<=b_term];


    
    initialState=[0, 0, 0, 0, 0, 0, 0]';
    initialControl2=us;
    %initial state
    x0=initialState;
    options = sdpsettings('solver','quadprog'); %??
    offsetFreeController = optimizer(offsetFreeConstraints, offsetFreeObjective, options, [x1(:,1); r1; dbar], u(:,1));
    if(any(runsection==8))
        codeoptions=getOptions('offsetFreeControllerForces');
        offsetFreeControllerForces=optimizerFORCES(offsetFreeConstraints, offsetFreeObjective,codeoptions,[x1(:,1); r1; dbar],u(:,1),{'xinint','ref','dist'},{'u0'});
    end
    if (any(runsection==4))
        [xt ut t rt] = simQuad( sys, offsetFreeController, bForces, x0, T, refval2, filter);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%  Simulation of the nonlinear model %%%%%%%%%%%%%%%%%%%%
if(any(runsection==5))
    fprintf('PART V - simulation of the nonlinear model...\n')
    sim('simulation2')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%  Slew Rate Constraints %%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(runsection==6))
    fprintf('PART VI - Slew Rate Constraints...\n')
  
    slewdelta=1;
    %slewdelta=0.5; this already is infeasible
     %%steady state under distrubance%
    %formulas from lecture
    LHS=[sys.A-eye(7), sys.B; Ctracking*Cest, zeros(4)];
    RHS=[-eye(7)*dbar; r1];
    sol=LHS\RHS;
    
    xsd=sol(1:7);
    usd=sol(8:end);
    
    
    %Q = diag([3 20 20 0.1]);   
    slewRateConstraints = disturbedConstraints;
    slewRateObjective = 0;
    for i = 1:(N-1)
        if (i==1)
            slewRateConstraints = [slewRateConstraints, (abs(ulast-u(:,i))<=slewdelta)];
        else
            slewRateConstraints= [slewRateConstraints, (abs(u(:,i)-u(:,i-1))<=slewdelta)];
        end
        
        slewRateObjective = slewRateObjective + (x1(:,i)'-xsd')*Ctracking'*Q(1:4,1:4)*Ctracking*(x1(:,i)-xsd)+(u(:,i)'-usd')*R*(u(:,i)-usd);
        
    end
    slewRateObjective = slewRateObjective + (x1(:,i)'-xsd')*Ctracking'*P(1:4,1:4)*Ctracking*(x1(:,i)-xsd);
    %terminal set
    slewRateConstraints=[slewRateConstraints, A_term*(x1(:,N)-xsd)<=b_term];


    
    initialState=[0, 0, 0, 0, 0, 0, 0]';
    initialControl2=us;
    %initial state
    x0=initialState;
    options = sdpsettings; %??
    slewRateController = optimizer(slewRateConstraints, slewRateObjective, options, [x1(:,1); r1; dbar], u(:,1));
    [xt ut t rt] = simQuad( sys, slewRateController, bForces, x0, T, refval2, filter);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%  Soft Constraints %%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(any(runsection==7))
    fprintf('PART VII - Soft Constraints...\n')
     
    %parameters for constraint violation cost%
    %not sure about that one
    Qu=200*eye(4);
    lu=100;
    Qx=100;
    lx=1010; 
    slewdelta=1;
     %%steady state under distrubance%
    %formulas from lecture
    LHS=[sys.A-eye(7), sys.B; Ctracking*Cest, zeros(4)];
    RHS=[-eye(7)*dbar; r1];
    sol=LHS\RHS;
    
    xsd=sol(1:7);
    usd=sol(8:end);
    
    
    %Q = diag([3 20 20 0.1]);   
    softConstraints = softConstraints;
    softObjective = 0;
    for i = 1:(N-1)
        if (i==1)
            softConstraints = [softConstraints, (abs(ulast-u(:,i))<=slewdelta)];
        else
            softConstraints= [softConstraints, (abs(u(:,i)-u(:,i-1))<=slewdelta)];
        end
        
        softObjective = softObjective + (x1(:,i)'-xsd')*Ctracking'*Q(1:4,1:4)*Ctracking*(x1(:,i)-xsd)+(u(:,i)'-usd')*R*(u(:,i)-usd);
        %cost on constraint violations
        %softObjective = softObjective + epsu(:,i)'*Qu*epsu(:,i) + lu*sum(abs(epsu(:,i)))+epsx(:,i)'*Qx*epsx(:,i) + lx*sum(abs(epsx(:,i)));
        softObjective = softObjective +epsx(:,i)'*Qx*epsx(:,i) + lx*sum(abs(epsx(:,i)));
    end
    softObjective = softObjective + (x1(:,i)'-xsd')*Ctracking'*P(1:4,1:4)*Ctracking*(x1(:,i)-xsd);
    %terminal set
    softConstraints=[softConstraints, A_term*(x1(:,N)-xsd)<=b_term];


    
    initialState=[0, 0, 0, 0, 0, 0, 0]';
    initialControl2=us;
    %initial state
    x0=initialState;
    options = sdpsettings; %??
    softController = optimizer(softConstraints, softObjective, options, [x1(:,1); r1; dbar], u(:,1));
    [xt ut t rt] = simQuad( sys,softController, bForces, x0, T, refval2, filter);

    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  FORCES Pro %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (any(runsection==8))
    fprintf('PART VIII - FORCES Pro...\n')
    [xt ut t rt_5Q] = simQuad(sys,trackingController,0,x0,T,refval);
    [xt ut t rt_5F] = simQuad(sys,trackingControllerForces,1,x0,T,refval);
    
    [xt ut t rt_9Q] = simQuad(sys,offsetFreeController,0,x0,T,refval2,filter);
    [xt ut t rt_9F] = simQuad(sys,offsetFreeControllerForces,1,x0,T,refval2,filter);
    
    disp('Ex 5');
    fprintf('Runtime with quadprog: %f\n', rt_5Q);
    fprintf('Runtime with FORCESPro: %f\n', rt_5F);
    
    disp('Ex 9');
    fprintf('Runtime with quadprog: %f\n', rt_9Q);
    fprintf('Runtime with FORCESPro: %f\n', rt_9F);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
