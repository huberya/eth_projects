% The testflow for IEEE Template is http://www.michaelshell.org/tex/testflow/

\documentclass[conference]{IEEEtran}

\usepackage[numbers]{natbib}
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
\bibliographystyle{IEEEtranN}
%\usepackage{bibentry}
%\nobibliography*

% Graphics Packages
%\RequirePackage[demo]{graphicx}
\ifCLASSINFOpdf

  \usepackage[pdftex]{graphicx}

  % declare the path(s) where your graphic files are
  \graphicspath{{./images/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  \DeclareGraphicsExtensions{.pdf,.jpeg,.png,jpg}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.

  \usepackage[dvips]{graphicx}

  % declare the path(s) where your graphic files are
  \graphicspath{{./eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  \DeclareGraphicsExtensions{.eps}
\fi


\usepackage{fancyref}
\usepackage{csquotes}
\usepackage{verbatimbox}

\usepackage{amsmath}
\usepackage{amssymb}
\interdisplaylinepenalty=2500
%% Define Maths functions here %%
\DeclareMathOperator{\sign}{sgn}
\DeclareMathOperator{\bern}{Bernoulli}

%\usepackage{algorithmic}
%\usepackage{array}

\ifCLASSOPTIONcompsoc
  \usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf]{subfig}
\else
  \usepackage[caption=false,font=footnotesize]{subfig}
\fi

\usepackage{url}

% correct bad hyphenation here
%\hyphenation{op-tical net-works semi-conduc-tor}

% Custom packages (non-ieee template)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\title{Data Fusion approach for Visual Question Answering}


\author{\IEEEauthorblockN{Christian Baumann, Oliver Harley, Yannick Huber, and Nicolas Schmid}
\IEEEauthorblockA{ETH Zurich\\
Email: \{chbauman,oharley,huberya,schminic\}@ethz.ch}
}

\maketitle

%Not sure what this is for, included in the IEEEtrans example
%\hfill mds
%\hfill January 18, 2015

\begin{abstract}
    Visual question answering combines the difficulties of the problems of producing and processing
    natural language and additionally image interpretation.
    The output produced by the answering system is dependent on jointly interpreting the textual
    modality of the question text, and the visual modality of the image. We investigate this as a
    data fusion problem and investigate the effectiveness of learning fusion and compare to
    approaches used currently. The results show that, while more complex fusion approaches might
    slightly increase performance of VQA it does not seem to be the critical part of the system.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
Visual Question Answering (VQA) requires interpreting both an image, and a related question. The
system is then required to determine a correct answer (See~\fref{fig:vqaIntro} for an example). This necessitates at
some point within the system fusing the different modalities together. This challenge is
significantly harder than traditional computer vision challenges. VQA combines the challenges of
visual interpretation, general reasoning, natural language interpretation and generation. Many
early attempts at VQA involve stitching \enquote{\textit{existing solutions of the above
sub-problems.}}\citep{Vinyals2017} How existing solutions may be better combined, focussing on the
fusion methods used in neural network architectures is the subject of
this paper.
\\ 
The aim of this paper is to explore different fusion approaches currently used in the context of VQA
and how they might be applied to other multi-modal reasoning systems.\\

We empirically compare different fusion approaches and summarise recent results that have applied
these methods. Specifically, we show; Early Fusion as described by
~\citet*{6907237}, Late Fusion~\citep{Liu2015}, Modout~\citep{7961772}, and Deep Fusion~\citep{MV3D}.  %or ~\citep{MultimDL}.
See~\fref{fig:compare_fusion_methods}.
We use the terminology from \citet*{8103116}.
\\
All code is supplied in the supplementary material, or can be found online
~\footnote{\url{https://gitlab.ethz.ch/huberya/DL_visualquestionanswering}}.

\begin{figure}[!t]
    \includegraphics[width=3in]{vqaIntro}
    \caption{Answering the question using the image requires combining both the visual concepts
        embedded within an image, and the interpretation of question text. Examples taken
        from~\citep{Agrawal2017a}.
    }\label{fig:vqaIntro}
\end{figure}

\begin{figure*}[!t]
    \centering
    \subfloat[]{\includegraphics[width=2.5in]{EarlyFusionFeatPrec}\label{fig:early_fusion}}
    \hfil
    \subfloat[]{\includegraphics[width=3in]{LateFusion}\label{fig:late_fusion}}
    \hfil
    \subfloat[]{\includegraphics[width=4.5in]{Modout}\label{fig:modout_fusion}}
    \hfil
    \subfloat[]{\includegraphics[width=4.5in]{DeepFusion}\label{fig:deep_fusion}}
    \caption{Different fusion approaches.}\label{fig:compare_fusion_methods}
\end{figure*}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Background
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background}
Fusion approaches have been used specifically for feature fusion for the task of Semantic Segmentation~
\citep{Garcia-Garcia2017}. The most common approaches are Early and Late fusion. It has been noted
by~\citet{Garcia-Garcia2017} that feature fusion approaches have made progress,
though have not proven to be the definitive solution for context knowledge for semantic segmentation.

The VQA challenge~\citep{Agrawal2017a} offers two very different modalities, simple natural
language queries and traditional image modalities. Much prior work to VQA tasks
comes from semantic segmentation, however the modality of the class labels lacks the
complexity of the sentences of the queries.
\citet{Garcia-Garcia2017} provide an overview of differing techniques applied to semantic
segmentation. In the similar realm of image captioning, RNNs (in particular LSTMs) have been used
to incorporated the captions of the images as in~\citep{Vinyals2017,Vinyals2015,Krause2016,Socher}.
However as \citet{Vinyals2017} note merely reusing existing solutions to sub-problems will not
necessarily result in the best performance, we examine what methods of fusion will provide better
results.

We use, and modify the baseline~\citep{Zhou2015}. The baseline, unmodified, essentially performs
late fusion. See the experimental setup~\fref{sec:experimentalsetup}.

\subsection{Comparison of fusion approaches}
Fusion embeds different modalities' features in a latent space. Doing so has the benefit of enabling context from outside a
particular section of the network to clarify local network confusion~\citep{Liu2015}.

Early and late fusion are effectively a description of how much pre-processing the
network may do before combining the
features. These fusion types are determined by the network architecture. Early and late fusion are
similar in their implementation, often a concatenation or similar operation. An example of early fusion is shown in the context module of
ParseNet~\citep{Liu2015}. \citet{Liu2015} note that Early Fusion often performs similarly to Late Fusion.
However these features require normalization to allow combining the features from each modality in a balanced
manner~\citep{Liu2015}, else performance robustness to parameter and dataset selection is reduced.
Late fusion must combine the different classification scorings of each modality. Both of these
methods are sensitive to the design of the network and the hyper parameters, which may have a
pronounced effect on performance.~\citep{Teney2017} %Or could cite [22] in Teney2017
In addition it has been found beneficial to fuse modalities with higher correlation earlier within the network~\citep{Li2017}.

Alternatively Modout and Deep Fusion allow the network to determine the underlying fusion strategy.
Modout, and similar stochastic methods use stochastic regularization, similar to the well-known
Dropout method, but the units in adjacent layers are dropped using the prior knowledge of their
modality groupings~\citep{Li2017}. For a comparison of similar stochastic dropout methods for model
selection the reader is referred to~\citet{Li2017}. Deep Fusion involves repeating units of feature
extraction layers followed by fusion layers that combine the intermediate feature representations
layers, see~\citep{Wang2016}. The resulting network learns multi-scale representations. Popular networks
such as ResNet~\citep{DBLP:journals/corr/HeZRS15}, and the inception module in
GoogLeNet~\citep{7298594} can be interpreted as Deep Fusion networks~\citep{Wang2016,MV3D}
combines features region wise from differently orientated lidar sensors (bird view and front view), and RGB
images uses Deep Fusion for differing sensor types.

Alternatively methods such as Modout, its predecessors, and Deep Fusion are able
to learn, to some extent, the model structure, overcoming the problematic decoupling of
network design from the data sources~\citep{Li2017}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Empirical comparison of fusion approaches}\label{sec:experimentalsetup}
\subsection{Implementation}
The baseline~\citep{Zhou2015} used in this paper performs late fusion in the form of a simple softmax layer upon the concatenated
word features of the question and the visual features of the image. This simple model performs
comparably to more complex architectures involving either deep and/or recursive neural networks~\citep{Zhou2015}.
Details about our implementation follow below.

\subsubsection{Experiments}
All following experiments were conducted on the `Leonhard' cluster~\citep{leonhardcluster}. Each experiment
was run on a single Nvidia GTX 1080 GPU and a 10-core Xeon E5{-}2650v4 CPU with 10GB of memory.
The code ran on Python 2.7.13 and used Tensorflow version 1.3. The GPU used CUDA 8.0.61 and cuDNN 6.0

\subsubsection{Reference Model}
As a Reference Model we implemented the model as described in~\citep{Zhou2015}. The question features are preprocessed in a fully connected layer with shape $512$.
Then question and image features are simply concatenated and fed into a fully connected hidden layer. The prediction is made through a softmax layer with shape corresponding to
the answer vocabulary. We used a polynomially decaying learning rate with initial value $0.01$ and decay power $0.5$. The batchsize was fixed at $32$. No additional regularizers
were applied and the standard cross-entropy loss was chosen as the loss function.
\subsubsection{Fusion Models}
\paragraph{Early Fusion}
In the case of early fusion we separately extracted the visual features of the images
and the word features of the questions similar to the baseline approach.
After encoding the questions features in a one-hot vector $x_q$, we added a linear word embedding, $x_q'=W_q*x_q+b_q$,
corresponding to a fully connected layer for them. In the next step the word embeddings
and the visual features were fused by concatenating the two layers of the modalities.
After the fusion a non-linear fully connected post processing layer was added,
before outputting the post processed network with a softmax.

\paragraph{Late Fusion}
The late fusion is very similar to the early fusion approach.
We added again a linear word embedding for the question features.
Instead of using a post-processing layer after the fusion we just introduced a dense non-linear preprocessing layer for each modality before fusing them together.


\paragraph{Modout Fusion}
Modout Fusion performs model selection based on stochastic regularization. The key idea is to use
prior knowledge of features belonging to modalities. As in~\fref{fig:modout_fusion} these modalities are fused stochastically
based on parameters $p^i$ which are learned together with the remaining network. Practically this splitting and fusing
is implemented using a mask matrix similar to dropout, but instead of dropping single connections it drops connections
between modalities. The mask Matrix is defined for the $j$th layer as follows:
\[ M_j=C_j U_j C_{j-1}^T \]
where $ C_j \in \mathbb{R}^{ N_j \times N_m } $. $N_m$ is the number of modalities which is $2$ in our case. $C_j$ is a binary matrix which associates
each input with exactly one modality
$U_j \sim \bern{\left( P \right)} $ defines which modalities to fuse. Its diagonal is constrained to be 1.
To be able to learn the fusion probability via gradient descent, $U_j$ is defined as $U_j=\frac{1+\sign(\sigma(P')-r)}{2}$ where $P'$ is unconstrained off diagonal
and part of the loss.
For regularization we used dropout for all Modout layers as well as the inputs. In addition we use a $L_{2}$-regularizer with a scaling factor of $0.1$
was used for the weight matrices of the Modout layers.
The decrease of the training error during the whole training is shown in \fref{fig:mfte}.


\paragraph{Deep Fusion}
Our Deep Fusion approach uses a linear word embedding of size $1024$ which is the same
as the dimensionality of the image input features. Let $x_{i}$ be the image features
and $x_{q}$ be the embedded question features, then the output of the
Deep Fusion approach used here is computed as follows. The predictions $\hat{y}$ are computed as
\[
    \hat{y} = \sigma_{sm}(W_{q}^{o} c(\sigma(W_{i} c(x_{i}, x_{q}) + b_{i}), \sigma(W_{q} c(x_{i}, x_{q}))) + b_{c}^{o})
\]

where $c(x_{1}, x_{2})$ is the function that combines the two representations, in our
case $c(x_{1}, x_{2}) = \frac{x_{1} + x_{2}}{2}$. $\sigma_{sm}$ denotes the softmax activation,
and $\sigma$ denotes some nonlinearity, in this case the $\tanh$. The dimensionality of
the fusion layer was chosen as $600$.
The loss that we minimize using the additional loss path as described in \citep{MV3D}
is the following:
\[
    L(x_{i}, x_{q}, y) = L_{ce}(\hat{y}, y) + \lambda [L_{ce}(\hat{y_{i}}, y) + L_{ce}(\hat{y_{r}}, y)]
\]
where $L_{ce}(\hat{y}, y)$ is the cross-entropy loss between $\hat{y}$ and $y$,
and $\lambda$ is some hyperparameter, chosen as $0.5$ here. $\hat{y}_{i} = \sigma_{sm}(W_{i}^{o} \sigma(W_{i}x_{i} + b_{i}) + b_{i}^{o})$
is the prediction using only the image features $x_{i}$, and $\hat{y}_{q}$ is defined similar
with the question features $x_{q} = \sigma_{sm}(W_{q}^{o} \sigma(W_{q}x_{q} + b_{q}) + b_{q}^{o})$.
Note that here the same parameters $W_{q}, W_{i}, b_{q}$ and $b_{i}$ are used resulting in the
required weight sharing. Further a $L2$-regularizer with a scaling factor of $0.1$
was used for the weight matrices $W_{i}$ and $W_{q}$.
To further regularize, a Dropout layer with a Dropout probability of $0.2$ was used for the input features.
For the training a polynomially decreasing learning rate, starting at $10^{-6}$ with rate $0.5$ was used.
The decrease of the training error during the whole training is shown in~\fref{fig:dfte}.
\begin{figure}[!t]
    \includegraphics[width=3in]{DeepFusionTrainingError.pdf}
    \caption{Development of training error for Deep Fusion model.}~\label{fig:dfte}
\end{figure}

\subsection{Results}
\paragraph{Early Fusion}
For the early fusion the nonlinearity $\sigma$ corresponded to a $\tanh$. The size of the non-linear fully connected
layer was the same as the size of the preceding fusion layer. For the optimization we used a SGD derivate call RMSProp.
The RMSProp parameters with polynomial decay for the learning rate were the following: Initial learning rate=$0.01$, final learning rate=$10^{-6}$, number of decay steps=$300000$,
decay power=$0.5$.
\begin{figure}[!t]
    \includegraphics[width=3in]{Plot_early_fusion.pdf}
    \caption{Development of training error for the early fusion model.}~\label{fig:efte}
\end{figure}
\paragraph{Late Fusion}
In terms of comparability we used the same nonlinearity $\sigma=\tanh$ as in the early fusion
approach for late fusion. The parameters of the RMSProp optimizer were chosen equally.
The size of the non-linear fully connected layer after the linear word embedding layer was
the same as the size of the latter. For the fully connected non-linear visual layer the size was chosen the same as the
size of the preceding layer.
\paragraph{Modout Fusion}
We again used the same nonlinearity $\sigma=\tanh$ as in previous models. The dimension of the word embedding was $1024$ which corresponds to the dimension of the image input features.
We used three Modout fusion layers with dimension $1024, 1024, 512$. For the output layer we choose the same as the reference model. The hidden layers applied dropout with rate $0.5$
and the input layer with rate $0.2$. The $L2$-regularizer had a scaling factor of $0.1$. For the optimization we again used a SGD derivate call RMSProp using the following parameters:
Initial learning rate=$10^{-7}$, final learning rate=$10^{-8}$, number of decay steps=$300000$,
decay rate=$0.5$.
\begin{figure}[!t]
    \includegraphics[width=3in]{ModoutFusionTrainingError.pdf}
    \caption{Development of training error for Modout Fusion model.}\label{fig:mfte}
\end{figure}


\begin{figure}[!t]
    \includegraphics[width=3in]{Plot_late_fusion.pdf}
    \caption{Development of training error for the late fusion model.}~\label{fig:lfte}
\end{figure}
\paragraph{Comparison}
The following table shows a comparison between the different approaches taken.
\addvbuffer[12pt 8pt]{\begin{tabular}{l*{2}{c}r}
Model & Training Error & Evaluation Accuracy \\
\hline
Reference Model & 41634 & 0.42920 \\
Early Fusion & 39785 & 0.42618  \\
Late Fusion & 41634 & 0.42920\\
Modout Fusion & 39869 & 0.33803 \\
Deep Fusion & NA & 0.44541 \\
\end{tabular}}
Note that the training loss of the Deep Fusion model is not comparable to
the other approaches since it includes the additional losses from the
paths added during training.



\section{Discussion}

The challenge of VQA is the multi-modal fusing of the question with that of the caption and image
embedding space. In addition this fusion must be active during runtime. The training
data, images, captions and question, does not necessarily need all to be in the same embedding
space as the space that the question sentence is embedded in, but at some point must be combined.

It is also to be noted there has been criticism that many deep-learning approaches to natural
language processing is simplistic, and ignorant of prior work~\citep{Marcus}.
We acknowledge that the approach taken for the natural language processing in this paper is simplistic,
however the model we used we believe is capable of demonstrating the merits and drawbacks of the different fusion
approaches.

The fusion approaches discussed here may prove to be beneficial in incorporating non
deep-learning natural language models' data that may capture more nuances, an approach that may address some of the criticisms
levelled~\citep{Marcus} towards a deep-learning exclusive approach to NLP.\@

Concerning the convergence all of our approaches reached an optimum or are seem to be close to one,
since the reduction of the training error eventually becomes very small.
In contrast to the more complex models deep fusion and modout fusion which
already are close to an optimum after on epoch, the late fusion and early fusion approaches converge just after 30 epoches.
Maybe another optimizer such as ADAM would further improve the result. However we don't think that
the convergences is a critical issue in our case.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
Although our approaches were more complex than the baseline approach we didn't
exceed the score as presented in~\citep{Zhou2015}.
We think that one reason for this, is that hyperparameters,
such as learning rate, regularizers as well as the dimensions of the layers,
were optimized than in our implementation.
We did not optimize our models' hyperparameters, such as layer sizes, the non-linear activations, and so on.
However the Deep Fusion approach scored higher than our implementation of the baseline score which
shows that more complex models can indeed beat the very simple concatenation model. This is expected
as the model structure is learnt from data rather than set from of optimizing hyperparameters.
The two more simple models, early and late fusion produced very similar results as the reference model, since their complexity and structure is very similar.
Concerning the more complex approaches Deep Fusion and Modout fusion,
we assume that they are much more data hungry than the simple linear baseline approach and hence more training data could have been beneficial, since they decrease the training error compared to the more simple models, but Modout does not improve the score and Deep Fusion only improves it by a little.

More complex fusion approaches, such as using RNNs might be more beneficial for problems where the number of modalities is greater than two.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{DL_visualquestionanswering}
%\bibliography{IEEEabrv,DL_visualquestionanswering}
%\bibliography{\jobname}
%\bibliography{IEEEabrv,\jobname}

\end{document}
\grid
