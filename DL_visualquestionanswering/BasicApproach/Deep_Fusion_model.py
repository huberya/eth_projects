import tensorflow as tf

def build_Deep_Fusion_model(n_img_features, quest_emb_dim, answer_vocab_size, max_sequence_length, batchsize = 32, seed = 42):
	
	# Model architecture params
	ETA = 0.001
	DECAY_POWER = 0.5
	loss_lambda = 1.0
	dropout_rate_input = 0
	dropout_rate_hidden = 0.5
	n_hidden_RNN = n_img_features # needs to be the same!
	number_LSTM_layers = 2
	layer_shapes = [1024]
	n_fusion_layers = len(layer_shapes)
	#activ = tf.nn.tanh
	activ = tf.nn.relu
	regularization_param = 1.0
	
	print("Network Parameters:")
	print("\tInitial Learning Rate: " + str(ETA))
	print("\tRate of Polynomial Decay: " + str(DECAY_POWER))
	print("\tFusion layer sizes: " + str(layer_shapes))
	print("\tDropout probability for input: " + str(dropout_rate_input))
	print("\tDropout probability for hidden layers: " + str(dropout_rate_hidden))
	print("")
	
	########################################################################
	# Define the Model
	
	# Learning rate
	global_step = tf.Variable(0, trainable=False)
	end_learning_rate = 0.000001
	decay_steps = 300000
	learning_rate = tf.train.polynomial_decay(ETA, global_step,
											  decay_steps, end_learning_rate,
											  power = DECAY_POWER)

	# Inputs Paceholders
	img_feats_inpt = tf.placeholder(tf.float32, shape = [batchsize, n_img_features], name = "input_image_features")
	quest_feats_inpt = tf.placeholder(tf.float32, shape = [batchsize, quest_emb_dim * max_sequence_length], name = "input_questions_embedded")
	labels = tf.placeholder(tf.int64, shape = [batchsize], name = "input_labels_integer_encoded")
	all_answers_input = tf.placeholder(tf.int64, shape = [batchsize, 10], name = "input_all_answers")
	#~ drop_prob_input = tf.placeholder(tf.float32, name = "input_dropout_probability_for_input")
	#~ drop_prob_hidden = tf.placeholder(tf.float32, name = "input_dropout_probability_for_hidden_layers")
	#~ regularization_param = tf.placeholder(tf.float32, name = "input_regularization_parameter")
	drop_prob_input = dropout_rate_input
	drop_prob_hidden = dropout_rate_hidden

	# Refularization and Initialization
	tf.set_random_seed(seed)
	regularizer = tf.contrib.layers.l2_regularizer(scale = regularization_param)
	norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

	# Image features with dropout
	if drop_prob_input > 0:
		img_feats_inpt_drop = tf.nn.dropout(img_feats_inpt, 1. - drop_prob_input, name = "image_feats_dropout")
	else: 
		img_feats_inpt_drop = img_feats_inpt

	# The LSTM with additional dropout
	quest_feats_inpt_list = tf.reshape(quest_feats_inpt, shape = [batchsize, max_sequence_length, quest_emb_dim], name = "reshaped_input")

	with tf.name_scope("LSTM_layers"):
		def lstm_cell():
			return tf.contrib.rnn.LSTMCell(n_hidden_RNN, use_peepholes = True, activation = activ)
		stacked_lstm = tf.contrib.rnn.MultiRNNCell([lstm_cell() for _ in range(number_LSTM_layers)])

		
		rnn_output, _ = tf.nn.dynamic_rnn(stacked_lstm, quest_feats_inpt_list, dtype=tf.float32)
	
	if drop_prob_input > 0:
		rnn_outputs_drop = tf.nn.dropout(rnn_output[:,-1,:], 1. - drop_prob_input, name = "rnn_output_dropout")
	else:
		rnn_outputs_drop = rnn_output[:,-1,:]
	
	
	# The Deep Fusion layers
	img_indep_proc = img_feats_inpt_drop
	quest_indep_proc = rnn_outputs_drop

	# Define the combine function
	def combine(tensor1, tensor2):
		return (tensor1 + tensor2) / 2.0
		
	# Combine the features
	comb = combine(rnn_outputs_drop, img_feats_inpt_drop)
	
	last_layer_size = n_img_features
	
	# Deep Fusion layers
	for k in range(n_fusion_layers):
		curr_layer_size = layer_shapes[k]
		# Use dense layers
		layer_name = "deep_fusion_layer_" + str(k+1)
		dropout_name = "fusion_layer_dropout" + str(k+1)
		with tf.name_scope(layer_name):
			quest_weights = tf.Variable(tf.random_normal([last_layer_size, curr_layer_size]))
			quest_biases = tf.Variable(tf.random_normal([curr_layer_size]))
			img_weights = tf.Variable(tf.random_normal([last_layer_size, curr_layer_size]))
			img_biases = tf.Variable(tf.random_normal([curr_layer_size]))
			
			# Process the combined representations
			img_proc = activ(tf.matmul(comb, img_weights) + img_biases, name = "img_processed_rep")
			quest_proc = activ(tf.matmul(comb, quest_weights) + quest_biases, name = "quest_processed_rep")
			with tf.name_scope("comb_processed_rep"):
				comb = combine(img_proc, quest_proc)
			
			# Process the individual representations
			img_indep_proc = activ(tf.matmul(img_indep_proc, img_weights) + img_biases, name = "img_indiv_processed_rep")
			quest_indep_proc = activ(tf.matmul(quest_indep_proc, quest_weights) + quest_biases, name = "quest_indiv_processed_rep")
		last_layer_size = curr_layer_size
			
			
	# The output, a dense layer without activation and a skip conection not using a bias for the second layer because of redundancy
	comb_out_acts = tf.layers.dense(comb, answer_vocab_size, name = "combined_output_activations")
	img_out_acts = tf.layers.dense(img_indep_proc, answer_vocab_size, name = "image_output_activations")
	quest_out_acts = tf.layers.dense(quest_indep_proc, answer_vocab_size, name = "question_output_activations")
	
	one_hot_labels = tf.one_hot(labels, answer_vocab_size, name = "one_hot_encoded_labels")
	predictions = tf.nn.softmax(comb_out_acts, name = "output_softmax")
	# Use a namescopes here to make the graph much much more nice to look at
	with tf.name_scope("combination_cross_entropy_with_ligits"):
		comb_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=comb_out_acts, name = "combination_cross_entropy"), name = "combination_cross_entropy_mean")
	with tf.name_scope("image_cross_entropy_with_ligits"):
		img_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=img_out_acts, name = "image_cross_entropy"), name = "image_cross_entropy_mean")
	with tf.name_scope("question_cross_entropy_with_ligits"):
		quest_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=quest_out_acts, name = "question_cross_entropy"), name = "question_cross_entropy_mean")
	loss = tf.add(comb_cross_entropy, loss_lambda * (img_cross_entropy + quest_cross_entropy), name = "final_loss")
	
	correct_prediction = tf.equal(tf.argmax(predictions,1), labels, name = "correct_predicted")
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name = "accuracy")
	max_predictions = tf.argmax(predictions,1, name = "prediction")
	number_true_preds = tf.reduce_sum(tf.cast(tf.equal(all_answers_input, tf.reshape(max_predictions, [batchsize, 1], name = "reshaped_max_preds_to_bs_1_matrix")), tf.float32), 1, name = "number_of_equal_answers")
	clipped_number = tf.clip_by_value(number_true_preds, 0.0, 3.0, name = "clipped_n_of_eq_answers") / 3.0
	modif_accur = tf.reduce_mean(clipped_number, name = "evaluation_accuracy")
	
	
	tf.summary.scalar("accuracy", accuracy)
	
	# Define loss and optimizer
	train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(loss, global_step=global_step)

	# Return training step, loss, predictions, accuracy and input placeholders
	return [train_step, loss, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, all_answers_input]












