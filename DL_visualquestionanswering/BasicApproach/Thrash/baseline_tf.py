import tensorflow as tf
import numpy as np
from data_loading import *

########################################################################
# Choose a name for your model
model_name = "MySecondModel"
model_path = "./Model/"
# Hyperparameters (global constants)
BATCHSIZE = 32
N_EPOCHS = 240
LEARNING_RATE = 0.002
# Max. number of words to use per question
max_sequence_length = 10
# If question is longer use first part (True) or last part (False)
first = True
# Save options
n_steps_per_save = 500
use_stored_model = False
verbose = False
# Model architecture params
# This is actually the drop probability
dropout_rate_input = 0.2
dropout_rate_hidden = 0.5
n_hidden_RNN = 1024
hidden_fcl_sizes = [512, 512]
SEED = 42
np.random.seed(SEED)
activ = tf.nn.tanh
#~ activ = tf.nn.relu
SCALE = 1.0


print("Training the model: " + model_name + " using the following parameters:")
print("\tNumber of epochs: " + str(N_EPOCHS))
print("\tBatch Size: " + str(BATCHSIZE))
print("\tMax. Input Question Length: " + str(max_sequence_length))
print("\tLearning Rate: " + str(LEARNING_RATE))
print("\tSave model parameters every: " + str(n_steps_per_save) + " steps.")
print("\tUsing saved model? " + str(use_stored_model))
print("\tHidden layer sizes: " + str(hidden_fcl_sizes))
print("\tDropout probability for input: " + str(dropout_rate_input))
print("\tDropout probability for hidden layers: " + str(dropout_rate_hidden))
print("")

########################################################################
# Setup Data
# Path into data folder
path_dataset = './Data'
path_images = "./ImageData"

# Choose Training and validation dataset
name_t = "trainval2014_train"
name_v = "trainval2014_val"

# Import Image Features
name_train = "train2014"
name_val = "val2014"
data_train = np.load(path_images + "/coco_" + name_train + "_googlenetFCdense_feat.npy")
data_val = np.load(path_images + "/coco_" + name_val + "_googlenetFCdense_feat.npy")
n_img_features = data_train.shape[1]
print("Image features with dimensions " + str(data_val.shape) + " and " + str(data_train.shape) + " imported.\n")

# Create a file map
img_dict = load_img_dict([name_train, name_val], path_images)

# Import question and answer vocabulary, this is the same for training and validation
quest_vocab_embd = np.load(path_dataset + "/question_vocab_embedded_" + name_t + ".npy")
quest_emb_dim = quest_vocab_embd.shape[1]
quest_vocab_dim = quest_vocab_embd.shape[0]
answer_vocab_size = sum(1 for line in open(path_dataset + "/answer_vocab_" + name_t + ".csv"))
with open(path_dataset + "/answer_vocab_" + name_t + ".csv") as f:
	content = [x.strip() for x in f.readlines()] 
print("Total: " + str(answer_vocab_size) + " possible answers.")
print("Question Vocabulary of size: " + str(quest_vocab_dim) + ".\n")

# Import question to image map, questions, answers and all_answers
[num_quests_train, quest_to_img_map_train] = compute_quest_to_img_map(name_t, path_dataset)
[num_quests_val, quest_to_img_map_val] = compute_quest_to_img_map(name_v, path_dataset)
train_quests = import_questions(num_quests_train, path_dataset, name_t, first, max_sequence_length)
val_quests = import_questions(num_quests_val, path_dataset, name_v, first, max_sequence_length)
answers_indxs_train = load_answer_indices(num_quests_train, name_t, path_dataset)
answers_indxs_val = load_answer_indices(num_quests_val, name_v, path_dataset)
all_answers = np.loadtxt(path_dataset + "/allanswers_" + name_v + ".csv" , delimiter=',', dtype = int)
num_ans = all_answers.shape[1]
all_answers -= np.ones(all_answers.shape, dtype = int)
print("Imported questions and answers, total: " + str(num_quests_train) + ".\n")

########################################################################
# Define the Model

# Inputs Paceholders
img_feats_inpt = tf.placeholder(tf.float32, shape=[BATCHSIZE, n_img_features], name = "input_image_features")
quest_feats_inpt = tf.placeholder(tf.float32, shape=[BATCHSIZE, quest_emb_dim * max_sequence_length], name = "input_questions_embedded")
labels = tf.placeholder(tf.int64, shape=[BATCHSIZE], name = "input_labels_integer_encoded")
drop_prob_input = tf.placeholder(tf.float32, name = "input_dropout_probability_for_input")
drop_prob_hidden = tf.placeholder(tf.float32, name = "input_dropout_probability_for_hidden_layers")
regularization_param = tf.placeholder(tf.float32, name = "input_regularization_parameter")

# Refularization and Initialization
tf.set_random_seed(SEED)
regularizer = tf.contrib.layers.l2_regularizer(scale = regularization_param)
norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

# Image features with dropout
img_feats_inpt_drop = tf.nn.dropout(img_feats_inpt, drop_prob_input, name = "image_feats_dropout")

# The LSTM with additional dropout
quest_feats_inpt_list = tf.split(quest_feats_inpt, max_sequence_length, 1)
rnn_cell = tf.contrib.rnn.BasicLSTMCell(n_hidden_RNN)
rnn_outputs, _ = tf.contrib.rnn.static_rnn(rnn_cell, quest_feats_inpt_list, dtype=tf.float32)
rnn_outputs_drop = tf.nn.dropout(rnn_outputs[-1], drop_prob_input, name = "rnn_output_dropout")

# The Fully connected hidden layers
n_hidd_l = len(hidden_fcl_sizes)

# Concatenate the features
concated_feats = tf.concat([img_feats_inpt_drop, rnn_outputs_drop], 1, name = "concatenated_features")
fcl_drop_nonl = concated_feats

# Dense hidden layers
for k in range(n_hidd_l):
	# Use dense layers
	layer_name = "dense_hidden_layer_" + str(k+1)
	dropout_name = "dense_hidden_layer_dropout" + str(k+1)
	with tf.name_scope(layer_name):
		fcl = tf.layers.dense(fcl_drop_nonl, hidden_fcl_sizes[k],
						name = layer_name,
						activation=activ,
						kernel_initializer = norm_initializer,
						kernel_regularizer = regularizer)
		fcl_drop_nonl = tf.nn.dropout(fcl, drop_prob_hidden, name = dropout_name)

# The output, a dense layer without activation and a skip conection
out_acts = tf.layers.dense(fcl_drop_nonl, answer_vocab_size, name = "output_activations_part1") + tf.layers.dense(concated_feats, answer_vocab_size, name = "output_activations_part2")
one_hot_labels = tf.one_hot(labels, answer_vocab_size, name = "one_hot_encoded_labels")
predictions = tf.nn.softmax(out_acts, name = "output_softmax")
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=out_acts), name = "cross_entropy")
correct_prediction = tf.equal(tf.argmax(predictions,1), labels, name = "correct_predicted")
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name = "accuracy")
max_predictions = tf.argmax(predictions,1, name = "prediction")
tf.summary.scalar("accuracy", accuracy)


# Count the number of trainable parameters
total_parameters = 0
for variable in tf.trainable_variables():
	# shape is an array of tf.Dimension
	shape = variable.get_shape()
	variable_parameters = 1
	for dim in shape:
		variable_parameters *= dim.value
	total_parameters += variable_parameters

print("Built the network architecture with a total of " + str(total_parameters) + " parameters.\n")

# Set verbosity
tf.logging.set_verbosity(tf.logging.INFO)

# Do summary stuff
merged_summary_op = tf.summary.merge_all()

########################################################################
# Train the model

# Define loss and optimizer
train_step = tf.train.RMSPropOptimizer(LEARNING_RATE).minimize(cross_entropy)

# Define a saver
saver = tf.train.Saver()

with tf.Session() as sess:
	
	# Prepare summary writing
	summ_filename = model_path + "summ_" + model_name
	summary_writer = tf.summary.FileWriter(summ_filename, graph=tf.get_default_graph())
	
	sample_indx = 0
	if use_stored_model:
		saver.restore(sess, model_path + model_name + ".ckpt")
		with open(model_path + model_name + "_info.txt", "r") as f:
			sample_indx = int(f.read())
	else:
		sess.run(tf.global_variables_initializer())
	
	# Iterate over all epochs
	print("\nStarting training...")
	n_step_tot = num_quests_train / BATCHSIZE
	step = sample_indx / BATCHSIZE
	for k in range(N_EPOCHS):
		
		train_loss_epoch = 0.0
		print("Starting epoch " + str(k) + " of " + str(N_EPOCHS))
		# Iterate over all questions
		while True:
			# Prepare labels and input
			if verbose:
				print("Epoch: " + str(k + 1) + " of " + str(N_EPOCHS) + ", Step: " + str(step+1) + " of " + str(n_step_tot))
				print("Preparing input...")
			if sample_indx + BATCHSIZE < num_quests_train:
				[batch_labels, batch_img_feats, batch_quest_feats] = prepare_input(sample_indx, num_quests_train, 
								BATCHSIZE, answers_indxs_train, answer_vocab_size, n_img_features,
								quest_emb_dim, max_sequence_length, quest_to_img_map_train, data_train, data_val, name_train, name_val,
								train_quests, quest_vocab_embd, img_dict)
			else:
				break
			
			# Do one train step
			if verbose:
				print("Doing a training step...")
			# Do a training step and return the loss
			_, train_loss, summary = sess.run([train_step, cross_entropy,  merged_summary_op], 
							feed_dict={img_feats_inpt: batch_img_feats, 
									  quest_feats_inpt: batch_quest_feats, 
									  labels: batch_labels,
									  drop_prob_input: dropout_rate_input,
									  drop_prob_hidden: dropout_rate_hidden,
									  regularization_param: SCALE,
									  })
			if verbose:
				print("Loss: " + str(train_loss))
			train_loss_epoch += train_loss
			step += 1
			sample_indx += BATCHSIZE
			
			# Save model
			if not (step % n_steps_per_save):
				if verbose:
					print("Saving parameters...")
				with open(model_path + model_name + "_info.txt", "w") as f:
					f.write(str(sample_indx))
				save_path = saver.save(sess, model_path + model_name + ".ckpt")
				
			#~ # Early stopping:
			#~ if step == 5:
				#~ break
		with open(model_path + model_name + "_curr_loss.txt", "w") as f:
			f.write(str(train_loss_epoch))
		# Write accuracy to summary
		summary_writer.add_summary(summary, k)
		print("Training error of this epoch: " + str(train_loss_epoch))
		# Reset indices
		sample_indx = 0
		step = 0
		
	########################################################################
	# Evaluate the model on the validation data
	print("\nEvaluating model..")
	sample_indx = 0
	# Iterate over all questions
	n_batches = 0
	n_bat_tot = num_quests_val / BATCHSIZE
	accur = 0.0
	accur_human = 0.0
	while True:
		# Prepare labels and input
		if verbose:
			print("Step: " + str(n_batches) + " of " + str(n_bat_tot))
			print("Preparing input...")
		if sample_indx + BATCHSIZE < num_quests_val:
			[batch_labels, batch_img_feats, batch_quest_feats] = prepare_input(sample_indx, num_quests_val, 
							BATCHSIZE, answers_indxs_val, answer_vocab_size, n_img_features,
							quest_emb_dim, max_sequence_length, quest_to_img_map_val, data_train, data_val, name_train, name_val,
							val_quests, quest_vocab_embd, img_dict)
		else:
			break
		
		# Do one evaluation step
		if verbose:
			print("Doing an evaluation step...")
		accur_curr, max_p = sess.run([accuracy, max_predictions],
							feed_dict={img_feats_inpt: batch_img_feats, 
									  quest_feats_inpt: batch_quest_feats, 
									  labels: batch_labels,
									  drop_prob_input: dropout_rate_input,
									  drop_prob_hidden: dropout_rate_hidden,
									  regularization_param: SCALE,
									  })
		accur += accur_curr
		all_ans_batch = all_answers[sample_indx:sample_indx + BATCHSIZE,:]
		equal = (np.outer(max_p.transpose(), np.ones((num_ans,), dtype = int)) == all_ans_batch)
		accur_human += np.sum(np.clip(np.sum(equal, axis = 1), 0, 3) / 3.0) / BATCHSIZE
		
		#~ print(str(all_ans_batch) + "<-batch all labels")
		#~ print(str(answers_indxs_val[sample_indx:sample_indx + BATCHSIZE]) + "<-batch labels")
		#~ print(str(max_p) + "<-predicted")
		#~ print(accur_human / n_batches)
		
		n_batches += 1
		sample_indx += BATCHSIZE
		
		#~ # Early stopping since I have no patience
		#~ if n_batches == 3:
			#~ break
	
	print("The accuracy on the evaluation set is: " + str(accur / n_batches))
	print("The evaluation accuracy is: " + str(accur_human / n_batches))







