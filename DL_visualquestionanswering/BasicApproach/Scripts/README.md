# Scripts

The provided scripts in this folder serve to convert 
the data from torch (lua) to numpy.

## Automated Setup

`auto_setup.py` creates the needed folders and downloads the
required data automatically.

## Image Features

`pytorch_data_load.py` takes the GoogleNet features of the images
and saves them as a numpy array. It also converts the ImageList
file to .txt. It requires pytorch: http://pytorch.org/

## Language Processing

`process_text.lua` processes the questions and answers. 
It takes all answers and creates a vocabulary with all the words
that occur at least some specified number of times. Then it 
creates an index to word map and converts the questions to 
sequences of indices that refer to that map. The same is done 
for the answers. It obviously requires lua, and torch.

## Word Embedding

`add_embedding.py` takes a word embedding and creates a file with all
the words occurring in the question vocabulary file and adds the 
corresponding word embedding.
