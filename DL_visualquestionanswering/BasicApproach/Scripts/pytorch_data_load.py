import torch
import numpy as np
from torch.utils.serialization import load_lua

# Path into data folder
path_dataset = '../Data'
path_images = "../ImageData"

prefixes = ['test2015', 'val2014', 'train2014']

for prefix in prefixes:

	# Load image namelist and save to .txt
	data_imlst = load_lua(path_images + "/coco_" + prefix + "_googlenetFCdense_imglist.dat")
	n = len(data_imlst)
	print(n)
	f_str = ""
	for k in range(n):
		f_str = f_str + data_imlst[k] + "\n"

	with open(path_dataset + "/coco_" + prefix + "_googlenetFCdense_imglist.txt", 'w') as f:
		f.write(f_str)

	# Load image features and save as numpy array
	data_feats = load_lua(path_images + "/coco_" + prefix + "_googlenetFCdense_feat.dat")
	np.save(path_images + "/coco_" + prefix + "_googlenetFCdense_feat", data_feats.numpy())
