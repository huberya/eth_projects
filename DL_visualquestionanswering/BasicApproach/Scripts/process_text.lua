
-- Choose test and training set
local train_prefix = 'trainval2014_train'
local test_prefix = 'trainval2014_val'

local path_dataset = '../Data'
local prefix = train_prefix

-- Load libraries
require 'torch'
local file = require 'pl.file'
local stringx = require 'pl.stringx'

-- Check if file exists and return boolean
function existfile(filename)
    local f=io.open(filename,"r")
    if f~=nil then io.close(f) return true else return false end
end

-- Load imglist file
function load_filelist(fname)
    local data = file.read(fname)
    data = stringx.replace(data,'\n',' ')
    data = stringx.split(data)
    local imglist_ind = {}
    for i=1, #data do
        imglist_ind[i] = stringx.split(data[i],'.')[1]
    end
    return imglist_ind
end

-- Build the vocabulary dictionary and the inverse of it
function build_vocab(data, thresh, IDX_singleline, IDX_includeEnd)
    if IDX_singleline == 1 then
        data = stringx.split(data,'\n')
    else
        data = stringx.replace(data,'\n', ' ')
        data = stringx.split(data)
    end
    local countWord = {}
    for i=1, #data do
        if countWord[data[i]] == nil then
            countWord[data[i]] = 1
        else
            countWord[data[i]] = countWord[data[i]] + 1
        end
    end
    local vocab_map_ = {}
    local ivocab_map_ = {}
    local vocab_idx = 0
    if IDX_includeEnd==1 then
        vocab_idx = 1
        vocab_map_['NA'] = 1
        ivocab_map_[1] = 'NA'
    end

    for i=1, #data do
        if vocab_map_[data[i]]==nil then
            if countWord[data[i]]>=thresh then
                vocab_idx = vocab_idx+1
                vocab_map_[data[i]] = vocab_idx
                ivocab_map_[vocab_idx] = data[i]
                --print(vocab_idx..'-'.. data[i] ..'--'.. countWord[data[i]])
            else
                vocab_map_[data[i]] = vocab_map_['NA']
            end
        end 
    end
    vocab_map_['END'] = -1
    return vocab_map_, ivocab_map_, vocab_idx
end

-- Options
local opt = {
		thresh_answerword = 3,
		thresh_questionword = 6,
		seq_length = 50,
}


function load_visualqadataset(opt, dataType, manager_vocab)
    -- Change it to your path. 
    -- local path_imglist = 'datasets/coco_dataset/allimage2014'
    -- All COCO images.
   
    -- VQA question/answer txt files.
    -- Download data_vqa_feat.zip and data_vqa_txt.zip and decompress into this folder

    
    local prefix = 'coco_' .. dataType 
    local filename_question = paths.concat(path_dataset, prefix .. '_question.txt')
    local filename_answer = paths.concat(path_dataset, prefix .. '_answer.txt')
    local filename_imglist = paths.concat(path_dataset, prefix .. '_imglist.txt')
    local filename_allanswer = paths.concat(path_dataset, prefix .. '_allanswer.txt')
    local filename_choice = paths.concat(path_dataset, prefix .. '_choice.txt')
    local filename_question_type = paths.concat(path_dataset, prefix .. '_question_type.txt')
    local filename_answer_type = paths.concat(path_dataset, prefix .. '_answer_type.txt')
    local filename_questionID = paths.concat(path_dataset, prefix .. '_questionID.txt')

    if existfile(filename_allanswer) then
        data_allanswer = file.read(filename_allanswer)
        data_allanswer = stringx.split(data_allanswer,'\n')
    end
    if existfile(filename_choice) then
        data_choice = file.read(filename_choice)
        data_choice = stringx.split(data_choice, '\n')
    end
    if existfile(filename_question_type) then
        data_question_type = file.read(filename_question_type)
        data_question_type = stringx.split(data_question_type,'\n')
    end
    if existfile(filename_answer_type) then
        data_answer_type = file.read(filename_answer_type)
        data_answer_type = stringx.split(data_answer_type, '\n')
    end
    if  existfile(filename_questionID) then
        data_questionID = file.read(filename_questionID)
        data_questionID = stringx.split(data_questionID,'\n')
    end
    

    local data_answer
    local data_answer_split
    if existfile(filename_answer) then
        print("Load answer file = " .. filename_answer)
        data_answer = file.read(filename_answer)
        data_answer_split = stringx.split(data_answer,'\n')
    end

    print("Load question file = " .. filename_question)
    local data_question = file.read(filename_question)
    local data_question_split = stringx.split(data_question,'\n')
    local manager_vocab_ = {}

    if manager_vocab == nil then
        local vocab_map_answer, ivocab_map_answer, nvocab_answer = build_vocab(data_answer, opt.thresh_answerword, 1, 0)
        local vocab_map_question, ivocab_map_question, nvocab_question = build_vocab(data_question,opt.thresh_questionword, 0, 1)
        print(' no.vocab_question=' .. nvocab_question.. ', no.vocab_answer=' .. nvocab_answer)
        manager_vocab_ = {vocab_map_answer=vocab_map_answer, ivocab_map_answer=ivocab_map_answer, vocab_map_question=vocab_map_question, ivocab_map_question=ivocab_map_question, nvocab_answer=nvocab_answer, nvocab_question=nvocab_question}
    else
        manager_vocab_ = manager_vocab
    end
    
    local imglist = load_filelist(filename_imglist)
    local nSample = #imglist
    -- We can choose to run the first few answers.
    if nSample > #data_question_split then
        nSample = #data_question_split
    end

    -- Answers.
    local x_answer = torch.zeros(nSample):fill(-1)
    if opt.multipleanswer == 1 then
        x_answer = torch.zeros(nSample, 10)
    end
    local x_answer_num = torch.zeros(nSample)

    -- Convert words in answers and questions to indices into the dictionary.
    local x_question = torch.zeros(nSample, opt.seq_length)
    for i = 1, nSample do
        local words = stringx.split(data_question_split[i])
        -- Answers
        if existfile(filename_answer) then
            local answer = data_answer_split[i]
            if manager_vocab_.vocab_map_answer[answer] == nil then
                x_answer[i] = -1
            else
                x_answer[i] = manager_vocab_.vocab_map_answer[answer]
            end
        end
        -- Questions
        for j = 1, opt.seq_length do
            if j <= #words then
                if manager_vocab_.vocab_map_question[words[j]] == nil then
                    x_question[{i, j}] = 1 
                else
                    x_question[{i, j}] = manager_vocab_.vocab_map_question[words[j]]
                end
            else
                x_question[{i, j}] = manager_vocab_.vocab_map_question['END']
            end
        end
    end

    collectgarbage()
    -- Return the state.
    local _state = {
        x_question = x_question, 
        x_answer = x_answer, 
        x_answer_num = x_answer_num, 
        featureMap = featureMap, 
        data_question = data_question_split,
        data_answer = data_answer_split, 
        imglist = imglist, 
        path_imglist = path_imglist, 
        data_allanswer = data_allanswer, 
        data_choice = data_choice, 
        data_question_type = data_question_type, 
        data_answer_type = data_answer_type, 
        data_questionID = data_questionID

    }
    
    return _state, manager_vocab_, nSample
end


local _state_train, manager_vocab_, nSample_train = load_visualqadataset(opt, prefix, nil)
local _state_test, manager_vocab_, nSample_test = load_visualqadataset(opt, test_prefix, manager_vocab_)


function write_csv_vocab(manager_vocab_, prefix)
	-- Write vocabulary files for test set
	print("Writing vocabulary files for questions and answers for " .. prefix .. "...")
	local n_vocab_quest = manager_vocab_.nvocab_question
	local n_vocab_answe = manager_vocab_.nvocab_answer
	local question_vocab = assert(io.open(path_dataset .. "/question_vocab_" .. prefix .. ".csv", "w")) -- open a file for serialization
	local answer_vocab = assert(io.open(path_dataset .. "/answer_vocab_" .. prefix .. ".csv", "w")) -- open a file for serialization

	for i = 1, n_vocab_quest do
		question_vocab:write(i .. "," .. manager_vocab_.ivocab_map_question[i] .. "\n")
	end
	question_vocab:close()

	for i = 1, n_vocab_answe do
		answer_vocab:write(i .. "," .. manager_vocab_.ivocab_map_answer[i] .. "\n")
	end
	answer_vocab:close()
end

write_csv_vocab(manager_vocab_, train_prefix)

function write_csv_qanda(manager_vocab_, _state, prefix, nSample)
	-- Write question and answer data file
	print("Writing question and answer file for " .. prefix .. "...")
	local question_csv = assert(io.open(path_dataset .. "/questions_" .. prefix .. ".csv", "w")) -- open a file for serialization
	local answer_csv = assert(io.open(path_dataset .. "/answers_" .. prefix .. ".csv", "w")) -- open a file for serialization
	for i = 1,nSample do
		x_seq = _state.x_question[i]
		answer_csv:write(_state.x_answer[i] .. "\n")
		for k= 1, x_seq:size(1) do
			if x_seq[k+1] ~= manager_vocab_.vocab_map_question['END'] then    
				question_csv:write(x_seq[k] .. ",")
			else
				question_csv:write(x_seq[k] .. "\n")
				break
			end
		end
	end
	question_csv:close()
	answer_csv:close()
end

write_csv_qanda(manager_vocab_, _state_train, train_prefix, nSample_train)
write_csv_qanda(manager_vocab_, _state_test, test_prefix, nSample_test)

function write_allanswer_idxs(manager_vocab_, _state, prefix, nSample)
	-- Write question and answer data file
	print("Writing all answers to file for " .. prefix .. "...")
	local all_answer_csv = assert(io.open(path_dataset .. "/allanswers_" .. prefix .. ".csv", "w")) -- open a file for serialization
	for i = 1,nSample do
		x_seq = stringx.split(_state.data_allanswer[i], ",")
		for k= 1, 10 do
			indx_curr = manager_vocab_.vocab_map_answer[x_seq[k]]
			if indx_curr == nil then
				all_answer_csv:write("-1")
			else
				all_answer_csv:write(indx_curr)
			end
			if k == 10 then
				all_answer_csv:write("\n")
			else
				all_answer_csv:write(",")
			end
		end
	end
	all_answer_csv:close()
end

-- All answers only needed for evaluation
write_allanswer_idxs(manager_vocab_,  _state_test, test_prefix, nSample_test)





