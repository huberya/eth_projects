import os
path_dataset = '../Data'
path_images = "../ImageData"

# Create folders if not existing
#~ os.system("mkdir " + path_dataset)
os.system("mkdir " + path_images)
os.system("mkdir ../Embedding")
os.system("mkdir ../Model")

# Download needed data and unzip
# Image features
os.system("wget  -O " + path_images + "/data_vqa_feat.zip \"http://visualqa.csail.mit.edu/data_vqa_feat.zip\"")
os.system("unzip " + path_images + "/data_vqa_feat.zip -d " + path_dataset)
os.system("cp -a " + path_images + "/data_vqa_feat/. " + path_dataset)
os.system("rm -rf " + path_images + "/data_vqa_feat")
os.system("rm -r" + path_images + "/data_vqa_feat.zip")
# Language part
os.system("wget -O " + path_dataset + "/data_vqa_txt.zip \"http://visualqa.csail.mit.edu/data_vqa_txt.zip\"")
os.system("unzip " + path_dataset + "/data_vqa_txt.zip -d " + path_dataset)
os.system("cp -a " + path_dataset + "/vqa_data_share/. " + path_dataset)
os.system("rm -rf " + path_dataset + "/vqa_data_share")
os.system("rm -r" + path_dataset + "/data_vqa_txt.zip")
# Word embedding
os.system("wget -O ../Embedding/glove.6B.zip \"http://nlp.stanford.edu/data/glove.6B.zip\"")
os.system("unzip ../Embedding/glove.6B.zip -d ../Embedding/")
os.system("rm -r ../Embedding/glove.6B.zip")


