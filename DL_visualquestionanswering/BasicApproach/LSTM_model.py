import tensorflow as tf

def build_LSTM_model(n_img_features, quest_emb_dim, answer_vocab_size, max_sequence_length, batchsize = 32, seed = 42):
	
	# Model architecture params
	LEARNING_RATE = 0.01
	dropout_rate_input = 0.2
	dropout_rate_hidden = 0.5
	n_hidden_RNN = 1024
	hidden_fcl_sizes = [512, 512]
	activ = tf.nn.tanh
	#~ activ = tf.nn.relu
	regularization_param = 1.0
	
	print("Network Parameters:")
	print("\tLearning Rate: " + str(LEARNING_RATE))
	print("\tHidden layer sizes: " + str(hidden_fcl_sizes))
	print("\tDropout probability for input: " + str(dropout_rate_input))
	print("\tDropout probability for hidden layers: " + str(dropout_rate_hidden))
	print("")
	
	########################################################################
	# Define the Model

	# Inputs Paceholders
	img_feats_inpt = tf.placeholder(tf.float32, shape = [batchsize, n_img_features], name = "input_image_features")
	quest_feats_inpt = tf.placeholder(tf.float32, shape = [batchsize, quest_emb_dim * max_sequence_length], name = "input_questions_embedded")
	labels = tf.placeholder(tf.int64, shape = [batchsize], name = "input_labels_integer_encoded")
	all_answers_input = tf.placeholder(tf.int64, shape = [batchsize, 10], name = "input_all_answers")
	#~ drop_prob_input = tf.placeholder(tf.float32, name = "input_dropout_probability_for_input")
	#~ drop_prob_hidden = tf.placeholder(tf.float32, name = "input_dropout_probability_for_hidden_layers")
	#~ regularization_param = tf.placeholder(tf.float32, name = "input_regularization_parameter")
	drop_prob_input = dropout_rate_input
	drop_prob_hidden = dropout_rate_hidden

	# Refularization and Initialization
	tf.set_random_seed(seed)
	regularizer = tf.contrib.layers.l2_regularizer(scale = regularization_param)
	norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

	# Image features with dropout
	img_feats_inpt_drop = tf.nn.dropout(img_feats_inpt, drop_prob_input, name = "image_feats_dropout")

	# The LSTM with additional dropout
	quest_feats_inpt_list = tf.split(quest_feats_inpt, max_sequence_length, 1, name = "split_input")
	rnn_cell = tf.contrib.rnn.BasicLSTMCell(n_hidden_RNN)
	rnn_outputs, _ = tf.contrib.rnn.static_rnn(rnn_cell, quest_feats_inpt_list, dtype=tf.float32)
	rnn_outputs_drop = tf.nn.dropout(rnn_outputs[-1], drop_prob_input, name = "rnn_output_dropout")

	# The Fully connected hidden layers
	n_hidd_l = len(hidden_fcl_sizes)

	# Concatenate the features
	concated_feats = tf.concat([img_feats_inpt_drop, rnn_outputs_drop], 1, name = "concatenated_features")
	fcl_drop_nonl = concated_feats

	# Dense hidden layers
	for k in range(n_hidd_l):
		# Use dense layers
		layer_name = "dense_hidden_layer_" + str(k+1)
		dropout_name = "dense_hidden_layer_dropout" + str(k+1)
		with tf.name_scope(layer_name):
			fcl = tf.layers.dense(fcl_drop_nonl, hidden_fcl_sizes[k],
							name = layer_name,
							activation=activ,
							kernel_initializer = norm_initializer,
							kernel_regularizer = regularizer)
			fcl_drop_nonl = tf.nn.dropout(fcl, drop_prob_hidden, name = dropout_name)

	# The output, a dense layer without activation and a skip conection not using a bias for the second layer because of redundancy
	out_acts = tf.layers.dense(fcl_drop_nonl, answer_vocab_size, name = "output_activations_part1") + tf.layers.dense(concated_feats, answer_vocab_size, use_bias = False, name = "output_activations_part2")
	one_hot_labels = tf.one_hot(labels, answer_vocab_size, name = "one_hot_encoded_labels")
	predictions = tf.nn.softmax(out_acts, name = "output_softmax")
	# Use a namescope here to make the graph much much more nice to look at
	with tf.name_scope("cross_entropy_with_ligits"):
		cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=out_acts), name = "cross_entropy")
	correct_prediction = tf.equal(tf.argmax(predictions,1), labels, name = "correct_predicted")
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name = "accuracy")
	max_predictions = tf.argmax(predictions,1, name = "prediction")
	number_true_preds = tf.reduce_sum(tf.cast(tf.equal(all_answers_input, tf.reshape(max_predictions, [batchsize, 1])), tf.float32), 1, name = "number_of_equal_answers")
	clipped_number = tf.clip_by_value(number_true_preds, 0.0, 3.0, name = "clipped_n_of_eq_answers") / 3.0
	modif_accur = tf.reduce_mean(clipped_number, name = "evaluation_accuracy")
	
	
	tf.summary.scalar("accuracy", accuracy)
	
	# Define loss and optimizer
	train_step = tf.train.RMSPropOptimizer(LEARNING_RATE).minimize(cross_entropy)

	# Return training step, loss, predictions, accuracy and input placeholders
	return [train_step, cross_entropy, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, all_answers_input]












