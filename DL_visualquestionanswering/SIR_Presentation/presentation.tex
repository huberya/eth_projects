\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lipsum, lmodern}
\usepackage[scaled=.95]{helvet}% helvetica as the origin of arial
\usepackage[helvet]{sfmath}    % for the mathematical enviroments
\renewcommand{\familydefault}{\sfdefault}


%% ETH beamer theme
% Options: [default]
%   itemsblack/[itemsblue]: change color of bullets etc. to black/blue in itemize style environments
%   [titlesblack]/titlesblue: change color of frame titles/subtitles to black/blue
% \usetheme[itemsblack,titlesblack]{eth}
\usetheme{eth}

%% Theme uses ETH blue color by default. Can be changed to any color using this command: 
% \setbeamercolor{structure}{fg=blue}

% References
\usepackage[style=authoryear,citestyle=authortitle,backend=biber]{biblatex}
\addbibresource{./refs.bib}
% Approach from https://tex.stackexchange.com/questions/215512/only-authortitleyear-in-footnote
% to cite as Author+Title+Year
\usepackage{xpatch}
\xapptobibmacro{cite}{\setunit{\nametitledelim}\printfield{year}}{}{}

% Images from report and summaries
\graphicspath{{./images/}}
\newcommand{\spath}{./images/}

%% Mandatory variables
\author{Christian Baumann}
\title{Sensor Fusion in Deep Learning}
%~ \subtitle{Learning from Multimodal Data Using Deep Neural Networks}
\date{\today}

%% Optional variables
% \supervisor{Cesar Cadena} % for one supervisor
\supervisors{Cesar Cadena, Julia Nitsch} % for multiple supervisors
\projecttype{Seminar in Robotics for CSE}

\begin{document}
\frame{\maketitle}
\begin{frame}{Table of contents}%{Subtitle}
	\tableofcontents
\end{frame}

\section{Motivation}
\begin{frame}{Motivation}
\begin{itemize}
\item More sensors, more information, also more robust model?
\item Need efficient information extraction.
\item Availability of datasets.
\item Applications: 
\begin{itemize}
\item Autonomous driving
\item Medical diagnosis
\item Audio-visual speech recognition
\item Visual question answering
\item etc.
\end{itemize}
\end{itemize}
\end{frame}

\section{Fusion Methods in Deep Learning}
\subsection{Early Fusion}
\begin{frame}{Early Fusion}
Used also for classical methods e.g. by \cite{6907237}.
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth,natwidth=610,natheight=642]{EarlyFusionFeatPrec.pdf}
%\caption{Early Fusion Architecture}
\label{fig:efa}
\end{figure}
Applied e.g. by \cite{AdapNet}.
\end{frame}

\subsection{Late Fusion}
\begin{frame}{Late Fusion}
Used also for classical methods e.g. by \cite{Zhang_sensorfusion}.
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth,natwidth=610,natheight=642]{LateFusion.pdf}
%\caption{Late Fusion Architecture}
\label{fig:lfa}
\end{figure}
Applied e.g. by \cite{ssmdlRGBDor}.
%late: \cite{ssmdlRGBDor} (DL) object recognition, \cite{Zhang_sensorfusion} (classical) semantic segmentation
\end{frame}

\subsection{Intermediate Fusion}
\begin{frame}{Intermediate Fusion}
Allows fusion of different modalities at different stages.\\
Decide by choice of architecture which modalities to fuse first, e.g.:
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.6\textwidth,natwidth=610,natheight=642]{IntermFusion.pdf}
	%\caption{Intermediate Fusion Network}
	\label{fig:imftm}
\end{figure}
Terminology taken from \cite{8103116}.
\end{frame}

\subsection{Modout Fusion}
\begin{frame}{Modout Fusion \footcite{7961772}}
Fusion by stochastic regularization.\\
Remove connections between modalities with certain probabilities.\\
Let network optimize them to choose fusion strategy by itself.\\
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{Modout.pdf}
	%\caption{Modout Fusion Network}
	\label{fig:mftm}
\end{figure}
\end{frame}

\subsection{Deep Fusion}
\begin{frame}{Deep Fusion \footcite{MV3D}}
Add additional paths and share parameters during training.
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth,natwidth=610,natheight=642]{DeepFusion.pdf}
	%\caption{Deep Fusion Network}
\end{figure}
\end{frame}

\section{Fusion for Temporal Input Data}
\begin{frame}{Fusion for Temporal Input Data}
\begin{itemize}
\item Let feature extraction deal with temporal structure as e.g. \cite{2015arXiv151202167Z}.
\item Use RNN, multiple outputs possible, e.g.:
\begin{itemize}
\item One LSTM cell per
modality used by \cite{7487478}.
\item Multimodal GRUs designed by \cite{2017arXiv170403152Y}:
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.6\textwidth,natwidth=610,natheight=642]{FusionRNN.pdf}
	%\caption{Fusion RNN}
\end{figure}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
Unrolled version:
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth,natwidth=610,natheight=642]{FusionRNNunroll.pdf}
	%\caption{Unrolled Fusion RNN}
\end{figure}
\end{frame}

\section{Missing Input Data}
\begin{frame}{Missing Input Data from some Modalities}
Two methods encountered:
\begin{itemize}
\item One network per combination of missing input:\\
Exponentially many trained networks needed.
\item Set missing input to predefined value:\\
Neet to find appropriate value, NN additionally needs to learn
how to ignore this.
\end{itemize}
Both used e.g. by \cite{MultimDL}.
\end{frame}


% Not used because of the time constraint
%~ \section{Enhanced Training}
%~ \begin{frame}{Enhanced Training}
%~ Two main approaches:
%~ \begin{enumerate}
%~ \item Initialize (part of) NN with parameters pretrained on 
%~ related task.
%~ \item Train reconstruction network to extract meaningful features in an unsupervised way.
%~ \end{enumerate}
%~ Lable propagation as done by \cite{ssmdlRGBDor}. Model for each modality
%~ provides confidently classified samples.
%~ \end{frame}

\section{Conclusions}
\begin{frame}{Conclusions}
\begin{itemize}
\item Basic approaches: early, intermediate and late fusion.\\
\item Early fusion: usually fewest parameters to train.\\
\item RNNs slow down training a lot.\\
\item What performs best? Problem dependent!\\
\item Guideline: Fuse more correlated input earlier \footcite{8103116}.\\
\end{itemize}
\end{frame}

%~ \section{References}
\begin{frame}[shrink=42]{References}
\printbibliography
\end{frame}



\end{document}







