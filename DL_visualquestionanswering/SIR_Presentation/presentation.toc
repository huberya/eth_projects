\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Fusion Methods in Deep Learning}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Early Fusion}{4}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Late Fusion}{5}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{3}{Intermediate Fusion}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{4}{Modout Fusion}{7}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{5}{Deep Fusion}{8}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Fusion for Temporal Input Data}{9}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Missing Input Data}{11}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Conclusions}{12}{0}{5}
