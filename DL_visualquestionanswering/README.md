# Repository for the visual question answering task of the Deep Learning class

Todo: add some more description

* Baseline: https://arxiv.org/pdf/1512.02167.pdf
* Baseline Code: https://github.com/metalbubble/VQAbaseline
* Data: http://www.visualqa.org/
* Approach to try: https://www.uoguelph.ca/~gwtaylor/publications/modout.pdf

## Basic Implementation

In `BasicApproach` you can find a simple tensorflow implementation.
It uses the image features and the processed questions and answers 
from the Baseline Code mentioned above. For the questions a pretrained word 
embedding from https://github.com/3Top/word2vec-api#where-to-get-a-pretrained-models
was used. 
