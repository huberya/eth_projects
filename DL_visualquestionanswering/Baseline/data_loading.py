import numpy as np
import tensorflow as tf

# This file contains some not so important functions
# to make the other file easier to understand.


# This function loads the questions and returns a numpy integer array that
# contains the questions encoded into indices that refer to the vocabulary.
# It also does zero padding at the beginning of the question. If the question
# is too long the first part is discarded it first == True, else the last part.
def import_questions(num_quests, path_dataset, name, first = False, max_sequence_length = 10):
	quests = np.zeros((num_quests, max_sequence_length), dtype = int)
	quests.fill(-1)
	ct = 0
	with open(path_dataset + "/questions_" + name + ".csv") as f:
		content = [x.strip() for x in f.readlines()] 
		for line in content:
			idxs = np.fromstring(line, dtype = int, sep=',')
			idxs -= np.ones(idxs.shape, dtype = int) # Correct indices
			word_count = idxs.shape[0]
			if word_count < max_sequence_length:
				quests[ct, (max_sequence_length - word_count):] = idxs
			else:
				if first:
					quests[ct, :] = idxs[:max_sequence_length]
				else:
					quests[ct, :] = idxs[word_count - max_sequence_length:]
			ct += 1
	return quests

# This functions returns the number of questions (= number of training samples) and
# returns a list of strings, where the i'the entry is the name of the image 
# which the i'th question refers to.
def compute_quest_to_img_map(name, path_dataset):
	num_quests = sum(1 for line in open(path_dataset + "/coco_" + name + "_imglist.txt"))
	quest_to_img_map = []
	with open(path_dataset + "/coco_" + name + "_imglist.txt") as f:
		quest_to_img_map = f.read().split("\n")
	quest_to_img_map = [quest_to_img_map[i][:-4] for i in range(num_quests)] # Cut off the ".jpg"
	return [num_quests, quest_to_img_map]

# This function loads the answers and converts them to python indices.
def load_answer_indices(num_quests, name, path_dataset):
	answers_indxs = np.zeros((num_quests,), dtype = int)
	with open(path_dataset + "/answers_" + name + ".csv") as f:
		answers_indxs = np.fromstring(f.read(), dtype=int, sep="\n")
	return answers_indxs - np.ones((num_quests,), dtype = int) # decrement indices, lua uses 1-based ones

# Creates a dictionnary that returns an index when given an image
# name as a key. The index refers to the row index of the corresponding
# image feature array.
def load_img_dict(dataset_list, path_dataset):
	img_dict = {}
	n = len(dataset_list)
	for k in range(n):
		with open(path_dataset + "/coco_" + dataset_list[k] + "_googlenetFCdense_imglist.txt") as f:
			name_list = f.read().split("\n")
			n_train_imgs = len(name_list) - 1
			img_dict.update({name_list[i]:i for i in range(n_train_imgs)})
	return img_dict

# Here the input and the labels of the model is prepared. For the labels we 
# use one-hot-encoding, the image features have to be taken from the right 
# image dataset and for the words in the questions the embedding has to be 
# provided.
def prepare_input(sample_indx, num_quests, batchsize, answers_indxs, answer_vocab_size, n_img_features,
				max_sequence_length, quest_to_img_map, data_train, data_val, name_train, name_val,
				quests, img_dict, all_answers, quest_vocab_dim, training = True):
	batch_labels = np.zeros((batchsize,), dtype = np.int64)
	batch_img_feats = np.zeros((batchsize, n_img_features), dtype = np.float32)
	batch_quest_feats = np.zeros((batchsize, max_sequence_length), dtype = np.float32)
	batch_all_ans = np.zeros((batchsize, 10), dtype = np.int64)
	bat_count = 0
	while bat_count < batchsize:
		ans = answers_indxs[sample_indx]
		if ans != -1:
			
			# Choose the right answer
			batch_labels[bat_count] = ans
			
			# Only for evaluation
			if not training:
				# Choose right all answer row
				batch_all_ans[bat_count,:] = all_answers[sample_indx,:]
			
			# Choose the right image features
			img_name = quest_to_img_map[sample_indx]
			dataset_name = img_name.split("_")[1]
			img_ind = img_dict[img_name]
			if dataset_name == name_train:
				batch_img_feats[bat_count,:] = data_train[img_ind,:]
			elif dataset_name == name_val:
				batch_img_feats[bat_count,:] = data_val[img_ind,:]
			else:
				print("No such dataset available.")
			
			# get indices for one_hot
			batch_quest_feats[bat_count,:] = quests[sample_indx,:]
			
			bat_count += 1
		sample_indx += 1
	return [batch_labels, batch_img_feats, batch_quest_feats, batch_all_ans]


# Load all the data that is needed for the training and evaluation of the model.
def load_all_the_data(path_dataset, path_images, max_sequence_length, name_t, name_v, name_train, name_val, first = True):
	
	# Import Image Features
	data_train = np.load(path_images + "/coco_" + name_train + "_googlenetFCdense_feat.npy")
	data_val = np.load(path_images + "/coco_" + name_val + "_googlenetFCdense_feat.npy")
	n_img_features = data_train.shape[1]
	print("Image features with dimensions " + str(data_val.shape) + " and " + str(data_train.shape) + " imported.\n")

	# Create a file map
	img_dict = load_img_dict([name_train, name_val], path_images)

	# Import question and answer vocabulary, this is the same for training and validation
	#~ quest_vocab_embd = np.load(path_dataset + "/question_vocab_embedded_" + name_t + ".npy")
	#~ quest_emb_dim = quest_vocab_embd.shape[1]
	#~ quest_vocab_dim = quest_vocab_embd.shape[0]
	answer_vocab_size = sum(1 for line in open(path_dataset + "/answer_vocab_" + name_t + ".csv"))
	quest_vocab_dim = sum(1 for line in open(path_dataset + "/question_vocab_" + name_t + ".csv"))
	with open(path_dataset + "/answer_vocab_" + name_t + ".csv") as f:
		content = [x.strip() for x in f.readlines()] 
	print("Total: " + str(answer_vocab_size) + " possible answers.")
	#~ print("Question Vocabulary of size: " + str(quest_vocab_dim) + ".\n")

	# Import question to image map, questions, answers and all_answers
	[num_quests_train, quest_to_img_map_train] = compute_quest_to_img_map(name_t, path_dataset)
	[num_quests_val, quest_to_img_map_val] = compute_quest_to_img_map(name_v, path_dataset)
	train_quests = import_questions(num_quests_train, path_dataset, name_t, first, max_sequence_length)
	val_quests = import_questions(num_quests_val, path_dataset, name_v, first, max_sequence_length)
	answers_indxs_train = load_answer_indices(num_quests_train, name_t, path_dataset)
	answers_indxs_val = load_answer_indices(num_quests_val, name_v, path_dataset)
	all_answers = np.loadtxt(path_dataset + "/allanswers_" + name_v + ".csv" , delimiter=',', dtype = int)
	num_ans = all_answers.shape[1]
	all_answers -= np.ones(all_answers.shape, dtype = int)
	print("Imported questions and answers, total: " + str(num_quests_train) + ".\n")
	
	return [quest_vocab_dim, answer_vocab_size, 
					num_quests_train, quest_to_img_map_train, num_quests_val, 
					quest_to_img_map_val, train_quests, val_quests, answers_indxs_train,
					answers_indxs_val, all_answers, num_ans,
					n_img_features, data_train, data_val, img_dict] 
	

# Count the trainable parameters of the tensorflow model
def count_pars():
	# Count the number of trainable parameters
	total_parameters = 0
	for variable in tf.trainable_variables():
		# shape is an array of tf.Dimension
		shape = variable.get_shape()
		variable_parameters = 1
		for dim in shape:
			variable_parameters *= dim.value
		total_parameters += variable_parameters
		
	print("Built the network architecture with a total of " + str(total_parameters) + " parameters.\n")	
































