import tensorflow as tf


# To look at graph use:
# tensorboard --logdir ./Model/summ_MySecondModel/

def build_late_fusion_model(n_img_features, quest_vocab_dim, answer_vocab_size, max_sequence_length, batchsize=32,
                         seed=42):
    ########################################################################
    ETA = 0.01
    DECAY_POWER = 0.5

    # Model architecture params
    embedding_size = 512
    activ = tf.nn.tanh
    # ~ activ = tf.nn.relu

    print("Network Parameters:")
    print("\tInitial Learning Rate: " + str(ETA))
    print("\tRate of Polynomial Decay: " + str(DECAY_POWER))
    print("")

    ########################################################################
    # Define the Model

    global_step = tf.Variable(0, trainable=False)
    end_learning_rate = 0.000001
    decay_steps = 300000
    learning_rate = tf.train.polynomial_decay(ETA, global_step,
                                              decay_steps, end_learning_rate,
                                              power=DECAY_POWER)

    # Input Paceholders, do not change
    img_feats_inpt = tf.placeholder(tf.float32, shape=[batchsize, n_img_features], name="input_image_features")
    quest_feats_inpt = tf.placeholder(tf.int64, shape=[batchsize, max_sequence_length], name="input_questions_vocab")
    labels = tf.placeholder(tf.int64, shape=[batchsize], name="input_labels_integer_encoded")
    all_answers_input = tf.placeholder(tf.int64, shape=[batchsize, 10], name="input_all_answers")
    training = tf.placeholder(tf.bool, name="training")

    # Initialization
    tf.set_random_seed(seed)
    norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

    quest_one_hot = tf.reduce_sum(tf.one_hot(quest_feats_inpt, quest_vocab_dim), axis=1)
    # this looks correct if linearNB from paper corresponds to fully connected
    quest_feats_emb = tf.layers.dense(quest_one_hot, embedding_size, name="quest_embedding", activation=activ)

    # Fully connected layers for both modalities
    img_feats_inpt_act = tf.layers.dense(img_feats_inpt,n_img_features,activation=activ, name="img_feats_inpt_dense_layer")
    quest_feats_emb_act=tf.layers.dense(quest_feats_emb,embedding_size, activation=activ, name="quest_feats_emb_dense_layer")

    # Concatenate the features
    concated_feats = tf.concat([img_feats_inpt_act, quest_feats_emb_act], 1, name="concatenated_features")

    # The output, a dense logsoftmax layer without activation
    comb_out_acts = tf.layers.dense(concated_feats, answer_vocab_size, name="fully_connected_hidden")

    one_hot_labels = tf.one_hot(labels, answer_vocab_size, name="one_hot_encoded_labels")
    predictions = tf.nn.softmax(comb_out_acts, name="output_softmax")

    # Use a namescopes here to make the graph much much more nice to look at
    with tf.name_scope("combination_cross_entropy_with_ligits"):
        comb_cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=comb_out_acts,
                                                    name="combination_cross_entropy"),
            name="combination_cross_entropy_mean")
    loss = comb_cross_entropy

    correct_prediction = tf.equal(tf.argmax(predictions, 1), labels, name="correct_predicted")
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")
    max_predictions = tf.argmax(predictions, 1, name="prediction")
    number_true_preds = tf.reduce_sum(tf.cast(tf.equal(all_answers_input, tf.reshape(max_predictions, [batchsize, 1],
                                                                                     name="reshaped_max_preds_to_bs_1_matrix")),
                                              tf.float32), 1, name="number_of_equal_answers")
    clipped_number = tf.clip_by_value(number_true_preds, 0.0, 3.0, name="clipped_n_of_eq_answers") / 3.0
    modif_accur = tf.reduce_mean(clipped_number, name="evaluation_accuracy")

    tf.summary.scalar("accuracy", accuracy)

    # Define loss and optimizer
    train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(loss, global_step=global_step)

    # Return training step, loss, predictions, accuracy and input placeholders
    return [train_step, loss, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, training,
            all_answers_input]
