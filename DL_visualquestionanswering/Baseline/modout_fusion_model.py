import tensorflow as tf
import numpy as np


#this function builds the model
#it is called inside train_and_evaluate.py
def build_modout_fusion_model(n_img_features, quest_vocab_dim, answer_vocab_size, max_sequence_length, batchsize = 32, seed = 42):
	########################################################################
	ETA = 0.00001
	DECAY_POWER = 0.5
	# If question is longer use first part (True) or last part (False)
	first = True
	# Model architecture params
	activ = tf.nn.tanh
	#~ activ = tf.nn.relu
	SCALE = 1.0
	regularization_param = 0.1



	########################################################################
	# Define the Model
	global_step = tf.Variable(0, trainable=False)
	end_learning_rate = 0.0000001
	decay_steps = 300000
	learning_rate = tf.train.polynomial_decay(ETA, global_step,
											  decay_steps, end_learning_rate,
											  power = DECAY_POWER)
	dropout_rate_input = 0.2
	dropout_rate_hidden = 0.5
	#must all have same shape
	#must be even numbers (we need to split in to since we have two modalities)
	#layers are evenly split between modalities
	layer_shapes = [1024, 1024, 512]
	#initial fusion probabilities
	img_combine_prob = [0.5, 0.5, 0.5]
	quest_combine_prob = [0.5, 0.5, 0.5]
	n_fusion_layers = len(layer_shapes)
	#activ = tf.nn.tanh
	activ = tf.nn.relu
                               
	print("Network Parameters:")
	print("\tInitial Learning Rate: " + str(ETA))
	print("\tRate of Polynomial Decay: " + str(DECAY_POWER))
	print("\tDropout probability for input: " + str(dropout_rate_input))
	print("\tDropout probability for hidden layers: " + str(dropout_rate_hidden))
	print("\tRegularization parameter: " + str(regularization_param)) 
	print("")
                                           
	# Inputs Paceholders
	img_feats_inpt = tf.placeholder(tf.float32, shape=[batchsize, n_img_features], name = "input_image_features")
	quest_feats_inpt = tf.placeholder(tf.int64, shape=[batchsize, max_sequence_length], name = "input_questions_vocab")
	labels = tf.placeholder(tf.int64, shape=[batchsize], name = "input_labels_integer_encoded")
	all_answers_input = tf.placeholder(tf.int64, shape = [batchsize, 10], name = "input_all_answers")
	training = tf.placeholder(tf.bool, name = "training")

	# Refularization and Initialization
	tf.set_random_seed(seed)
	regularizer = tf.contrib.layers.l2_regularizer(scale = regularization_param)
	norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

	#one hot encoding of the question
	quest_one_hot = tf.reduce_sum(tf.one_hot(quest_feats_inpt, quest_vocab_dim), axis=1)
	#Linear word embedding
	quest_feats_emb = tf.layers.dense(quest_one_hot, n_img_features, name = "quest_embedding", activation = activ)

	# Input dropout
	if dropout_rate_input > 0:
		quest_feats_emb = tf.layers.dropout(quest_feats_emb, rate = dropout_rate_input, training = training, name = "quest_embedding_dropout")
		img_feats_inpt = tf.layers.dropout(img_feats_inpt, rate = dropout_rate_input, training = training, name = "img_feats_dropout")
	

	
	#initial size is img_features+question_features = 2*n_img_features
	last_layer_size = 2*n_img_features
	
	# Concatenate the features
	concated_feats = tf.concat([img_feats_inpt, quest_feats_emb], 1, name = "concatenated_features")
	#modout is simulated using a mask (see paper)
	modout_feats = concated_feats	
	# Modout Fusion layers
	for k in range(n_fusion_layers):
		curr_layer_size = layer_shapes[k]
		layer_name = "modout_fusion_layer_" + str(k+1)
		dropout_name = "modout_layer_dropout" + str(k+1)
		with tf.name_scope(layer_name):
			weights = tf.get_variable("question_weight_matrix_" + str(k), [last_layer_size, curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer, regularizer=regularizer)
			biases = tf.get_variable("question_bias_vector_" + str(k), [curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer)
			
			#compute mask matrix
			#this matrix basically says which features belong to which modality (in our case simply split in half)
			cinit=np.zeros((last_layer_size, 2))
			for i in range(last_layer_size//2):
				cinit[i,0]=1
				cinit[last_layer_size//2+i, 1]=1
			Cj = tf.constant(cinit, name="matrix_c"+str(k), dtype=tf.float32)#this is a fixed binary matrix
			cinit1=np.zeros((curr_layer_size, 2))
			for i in range(curr_layer_size//2):
				cinit1[i,0]=1
				cinit1[curr_layer_size//2+i, 1]=1
			Cj1 = tf.constant(cinit1, name="matrix_c"+str(k), dtype=tf.float32)#this is a fixed binary matrix
			
			#this is the fusion probability matrix
			pinit=tf.constant(np.array([[1,quest_combine_prob[k]],[img_combine_prob[k],1]], dtype=np.float32))
			Pj = tf.get_variable("modout_prob_matrix" + str(k), initializer=pinit, dtype=tf.float32) #this should get trained with the rest
			tfone = tf.constant(1.0, shape = (2,2), dtype=tf.float32)
			rtemp = np.random.uniform(size = (2,2))
			#this is how Uj should be computed according to the paper
			Uj = (tfone + tf.sign(tf.sigmoid(Pj) - rtemp)) / 2
			Mj = tf.matmul(tf.matmul(Cj,Uj), tf.transpose(Cj1))
			# Process the combined representations
			#tf.mul is elementwise multiplication
			modout_feats_tmp = activ(tf.matmul(modout_feats, tf.multiply(Mj, weights) + biases, name = "modout_processed_rep"))
			#additional dropout
			modout_feats = tf.layers.dropout(modout_feats_tmp, rate = dropout_rate_hidden, training = training, name = "dropout" + str(k))
		last_layer_size = curr_layer_size
			
			
	# The output, a dense layer without activation and a skip conection not using a bias for the second layer because of redundancy
	comb_out_acts = tf.layers.dense(modout_feats, answer_vocab_size, name = "combined_output_activations")

	
	one_hot_labels = tf.one_hot(labels, answer_vocab_size, name = "one_hot_encoded_labels")
	predictions = tf.nn.softmax(comb_out_acts, name = "output_softmax")
	
	# Use a namescopes here to make the graph much much more nice to look at
	with tf.name_scope("combination_cross_entropy_with_ligits"):
		comb_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=comb_out_acts, name = "combination_cross_entropy"), name = "combination_cross_entropy_mean")
	loss = comb_cross_entropy
	
	correct_prediction = tf.equal(tf.argmax(predictions,1), labels, name = "correct_predicted")
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name = "accuracy")
	max_predictions = tf.argmax(predictions,1, name = "prediction")
	number_true_preds = tf.reduce_sum(tf.cast(tf.equal(all_answers_input, tf.reshape(max_predictions, [batchsize, 1], name = "reshaped_max_preds_to_bs_1_matrix")), tf.float32), 1, name = "number_of_equal_answers")
	clipped_number = tf.clip_by_value(number_true_preds, 0.0, 3.0, name = "clipped_n_of_eq_answers") / 3.0
	modif_accur = tf.reduce_mean(clipped_number, name = "evaluation_accuracy")
	
	tf.summary.scalar("accuracy", accuracy)
	
	# Define loss and optimizer
	train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(loss, global_step=global_step)

	return [train_step, loss, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, training, all_answers_input]




