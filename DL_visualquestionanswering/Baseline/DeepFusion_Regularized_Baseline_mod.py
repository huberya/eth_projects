import tensorflow as tf
# To look at graph use:
# tensorboard --logdir ./Model/summ_MySecondModel/

def build_DF_Baseline_model(n_img_features, quest_vocab_dim, answer_vocab_size, max_sequence_length, batchsize = 32, seed = 42):
	########################################################################
	ETA = 0.0001
	DECAY_POWER = 0.5
	# Model architecture params
	#~ activ = tf.nn.tanh
	activ = tf.nn.relu
	
	loss_lambda = 0.5
	dropout_rate_input = 0.2
	dropout_rate_hidden = 0.5
	layer_shapes = [600]
	n_fusion_layers = len(layer_shapes)
	activ = tf.nn.tanh
	#~ activ = tf.nn.relu
	regularization_param = 0.1

	print("Network Parameters:")
	print("\tInitial Learning Rate: " + str(ETA))
	print("\tRate of Polynomial Decay: " + str(DECAY_POWER))
	print("\tFusion layer sizes: " + str(layer_shapes))
	print("\tDropout probability for input: " + str(dropout_rate_input))
	print("\tDropout probability for hidden layers: " + str(dropout_rate_hidden))
	print("\tRegularization parameter: " + str(regularization_param))
	print("\tWeighting parameter of additional loss: " + str(loss_lambda))
	print("")

	########################################################################
	# Define the Model
	
	# learning rate
	global_step = tf.Variable(0, trainable = False)
	end_learning_rate = 0.000001
	decay_steps = 300000
	learning_rate = tf.train.polynomial_decay(ETA, global_step,
											  decay_steps, end_learning_rate,
											  power = DECAY_POWER)
                                           
	# Input Paceholders, do not change
	img_feats_inpt = tf.placeholder(tf.float32, shape=[batchsize, n_img_features], name = "input_image_features")
	quest_feats_inpt = tf.placeholder(tf.int64, shape=[batchsize, max_sequence_length], name = "input_questions_vocab")
	labels = tf.placeholder(tf.int64, shape=[batchsize], name = "input_labels_integer_encoded")
	all_answers_input = tf.placeholder(tf.int64, shape = [batchsize, 10], name = "input_all_answers")
	training = tf.placeholder(tf.bool, name = "training")

	# Regularization and Initialization
	tf.set_random_seed(seed)
	regularizer = tf.contrib.layers.l2_regularizer(scale = regularization_param)
	norm_initializer = tf.contrib.layers.xavier_initializer(uniform=False, dtype=tf.float32)

	# One-hot encoding of the question
	quest_one_hot = tf.reduce_sum(tf.one_hot(quest_feats_inpt, quest_vocab_dim), axis=1, name = "one_hot_question")
	
	# Linear word embedding
	quest_feats_emb = tf.layers.dense(quest_one_hot, n_img_features, name = "quest_embedding", activation = activ)
	
	# Input dropout
	if dropout_rate_input > 0:
		quest_feats_emb = tf.layers.dropout(quest_feats_emb, rate = dropout_rate_input, training = training, name = "quest_embedding_dropout")
		img_feats_inpt = tf.layers.dropout(img_feats_inpt, rate = dropout_rate_input, training = training, name = "img_feats_dropout")
		
	# The Deep Fusion layers
	img_indep_proc = img_feats_inpt
	quest_indep_proc = quest_feats_emb
	
	# Define the combine function
	def combine(tensor1, tensor2):
		return (tensor1 + tensor2) / 2.0
		
	# Combine the features
	comb = combine(img_feats_inpt, quest_feats_emb)
	
	last_layer_size = n_img_features
	
	# Deep Fusion layers
	for k in range(n_fusion_layers):
		curr_layer_size = layer_shapes[k]
		# Use dense layers
		layer_name = "deep_fusion_layer_" + str(k+1)
		dropout_name = "fusion_layer_dropout" + str(k+1)
		with tf.name_scope(layer_name):
			# Define parameters, regularize only weight matrices
			quest_weights = tf.get_variable("question_weight_matrix_" + str(k), [last_layer_size, curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer, regularizer = regularizer)
			quest_biases = tf.get_variable("question_bias_vector_" + str(k), [curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer)
			img_weights = tf.get_variable("image_weight_matrix_" + str(k), [last_layer_size, curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer, regularizer = regularizer)
			img_biases = tf.get_variable("image_bias_vector_" + str(k), [curr_layer_size], 
							dtype=tf.float32, initializer=norm_initializer)
			
			# Process the combined representations
			img_proc = activ(tf.matmul(comb, img_weights) + img_biases, name = "img_processed_rep")
			quest_proc = activ(tf.matmul(comb, quest_weights) + quest_biases, name = "quest_processed_rep")
			with tf.name_scope("comb_processed_rep"):
				comb = combine(img_proc, quest_proc)
			
			# Process the individual representations
			img_indep_proc = activ(tf.matmul(img_indep_proc, img_weights) + img_biases, name = "img_indiv_processed_rep")
			quest_indep_proc = activ(tf.matmul(quest_indep_proc, quest_weights) + quest_biases, name = "quest_indiv_processed_rep")
		last_layer_size = curr_layer_size
		
		
	# The output, a dense layer without activation and a skip conection not using a bias for the second layer because of redundancy
	comb_out_acts = tf.layers.dense(comb, answer_vocab_size, name = "combined_output_activations")
	img_out_acts = tf.layers.dense(img_indep_proc, answer_vocab_size, name = "image_output_activations")
	quest_out_acts = tf.layers.dense(quest_indep_proc, answer_vocab_size, name = "question_output_activations")
	
	one_hot_labels = tf.one_hot(labels, answer_vocab_size, name = "one_hot_encoded_labels")
	predictions = tf.nn.softmax(comb_out_acts, name = "output_softmax")
	# Use a namescopes here to make the graph much much more nice to look at
	with tf.name_scope("combination_cross_entropy_with_ligits"):
		comb_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=comb_out_acts, name = "combination_cross_entropy"), name = "combination_cross_entropy_mean")
	with tf.name_scope("image_cross_entropy_with_ligits"):
		img_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=img_out_acts, name = "image_cross_entropy"), name = "image_cross_entropy_mean")
	with tf.name_scope("question_cross_entropy_with_ligits"):
		quest_cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=one_hot_labels, logits=quest_out_acts, name = "question_cross_entropy"), name = "question_cross_entropy_mean")
	loss = tf.add(comb_cross_entropy, loss_lambda * (img_cross_entropy + quest_cross_entropy), name = "final_loss")
	
	correct_prediction = tf.equal(tf.argmax(predictions,1), labels, name = "correct_predicted")
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name = "accuracy")
	max_predictions = tf.argmax(predictions,1, name = "prediction")
	number_true_preds = tf.reduce_sum(tf.cast(tf.equal(all_answers_input, tf.reshape(max_predictions, [batchsize, 1], name = "reshaped_max_preds_to_bs_1_matrix")), tf.float32), 1, name = "number_of_equal_answers")
	clipped_number = tf.clip_by_value(number_true_preds, 0.0, 3.0, name = "clipped_n_of_eq_answers") / 3.0
	modif_accur = tf.reduce_mean(clipped_number, name = "evaluation_accuracy")
	
	
	tf.summary.scalar("accuracy", accuracy)
	
	# Define loss and optimizer
	train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(loss, global_step=global_step)

	# Return training step, loss, predictions, accuracy and input placeholders
	return [train_step, loss, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, training, all_answers_input]
