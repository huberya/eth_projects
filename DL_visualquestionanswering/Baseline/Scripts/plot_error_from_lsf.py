import argparse

# This file creates a plot of the development of the training error during training
# of any model from the output of e.g. the baseline.py file.
# It can also take a ".txt" file containing only (ASCII) floats, then specify type = txt.

parser = argparse.ArgumentParser(description='Create a plot from the a "lsf." or a ".txt" file that shows the training error. If a .txt file is used, use type = txt.')
parser.add_argument('path', metavar='filename', type=str, nargs=1,
					help='path and name of the data file')
parser.add_argument('type', metavar='type', type=str, default="lsf",  nargs='?',
					help='type must either be .lsf or .txt')

args = parser.parse_args()

file_path = args.path[0]
input_type = args.type

data = []

if input_type == "lsf":
	# Read the file
	with open(file_path, 'r') as myfile:
		str_impt=myfile.read()
		
		# Edit the contents of the file
		lst = str_impt.split("Starting training...")[1].split("Starting epoch")[1:-1]
		data = [float(lst[i].split("\n")[1].split(" ")[-1]) for i in range(len(lst))]
elif input_type == "txt":
	data = []
	with open(file_path, 'r') as myfile:
		data=myfile.read().split("\n")[:-1]
		data = [float(data[i]) for i in range(len(data))]
else:
	print("\nInput type not supported!!!")

# Plot
import matplotlib.pyplot as plt
plt.plot(data)
plt.ylabel('Training Error')
plt.xlabel('Epoch')
plt.savefig("Plot.pdf")

















