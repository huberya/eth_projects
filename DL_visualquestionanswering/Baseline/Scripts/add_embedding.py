# Code used from: https://ireneli.eu/2017/01/17/tensorflow-07-word-embeddings-2-loading-pre-trained-vectors/
import numpy as np

prefix = "trainval2014_train"

path_dataset = '../Data'

filename = '../Embedding/glove.6B.50d.txt'
def loadGloVe(filename):
    vocab = []
    embd = []
    file = open(filename,'r')
    for line in file.readlines():
        row = line.strip().split(' ')
        vocab.append(row[0])
        embd.append(row[1:])
    print('Loaded GloVe!')
    file.close()
    return vocab,embd
vocab,embd = loadGloVe(filename)
vocab_size = len(vocab)
embedding_dim = len(embd[0])
embedding = np.asarray(embd)

# Count number of lines
myfile = open(path_dataset + "/question_vocab_" + prefix + ".csv")
num_lines = sum(1 for line in myfile)
embed = np.zeros((num_lines, embedding_dim))

# Fill Matrix, fill row with zero if word not in vocabulary
myfile = open(path_dataset + "/question_vocab_" + prefix + ".csv")
ct = 0
for line in myfile.readlines():
	word = line.strip().split(',')[1]
	try:
		idx = vocab.index(word)
		embed[ct,:] = embedding[idx,:]
	except ValueError:
		embed[ct,:] = np.zeros((embedding_dim,))
	ct += 1

np.save(path_dataset + "/question_vocab_embedded_" + prefix, embed)






