import tensorflow as tf
import numpy as np
from data_loading import *

import re
import os

def load_graph():
	"""Creates a graph from saved GraphDef file and returns a saver."""
	# Creates graph from saved graph_def.pb.
	model_filename =os.path.join('./Model/ImageNet', 'classify_image_graph_def.pb')
	with tf.gfile.GFile(model_filename, 'rb') as f:
		graph_def = tf.GraphDef()
		graph_def.ParseFromString(f.read())

	with tf.Graph().as_default() as graph:
		tf.import_graph_def(
			graph_def, 
			input_map=None, 
			return_elements=None, 
			name="", 
			op_dict=None, 
			producer_op_list=None
		)
	return graph

#this is function from tf tutorial
def create_graph():
  """Creates a graph from saved GraphDef file and returns a saver."""
  # Creates graph from saved graph_def.pb.
  with tf.gfile.FastGFile(os.path.join(
      "Model/ImageNet", 'classify_image_graph_def.pb'), 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

#helper function for classigying images makes strings humand readable
class NodeLookup(object):
  """Converts integer node ID's to human readable labels."""

  def __init__(self,
               label_lookup_path=None,
               uid_lookup_path=None):
    if not label_lookup_path:
      label_lookup_path = os.path.join(
          "Model/ImageNet/", 'imagenet_2012_challenge_label_map_proto.pbtxt')
    if not uid_lookup_path:
      uid_lookup_path = os.path.join(
          "Model/ImageNet/", 'imagenet_synset_to_human_label_map.txt')
    self.node_lookup = self.load(label_lookup_path, uid_lookup_path)

  def load(self, label_lookup_path, uid_lookup_path):
    """Loads a human readable English name for each softmax node.

    Args:
      label_lookup_path: string UID to integer node ID.
      uid_lookup_path: string UID to human-readable string.

    Returns:
      dict from integer node ID to human-readable string.
    """
    if not tf.gfile.Exists(uid_lookup_path):
      tf.logging.fatal('File does not exist %s', uid_lookup_path)
    if not tf.gfile.Exists(label_lookup_path):
      tf.logging.fatal('File does not exist %s', label_lookup_path)

    # Loads mapping from string UID to human-readable string
    proto_as_ascii_lines = tf.gfile.GFile(uid_lookup_path).readlines()
    uid_to_human = {}
    p = re.compile(r'[n\d]*[ \S,]*')
    for line in proto_as_ascii_lines:
      parsed_items = p.findall(line)
      uid = parsed_items[0]
      human_string = parsed_items[2]
      uid_to_human[uid] = human_string

    # Loads mapping from string UID to integer node ID.
    node_id_to_uid = {}
    proto_as_ascii = tf.gfile.GFile(label_lookup_path).readlines()
    for line in proto_as_ascii:
      if line.startswith('  target_class:'):
        target_class = int(line.split(': ')[1])
      if line.startswith('  target_class_string:'):
        target_class_string = line.split(': ')[1]
        node_id_to_uid[target_class] = target_class_string[1:-2]

    # Loads the final mapping of integer node ID to human-readable string
    node_id_to_name = {}
    for key, val in node_id_to_uid.items():
      if val not in uid_to_human:
        tf.logging.fatal('Failed to locate: %s', val)
      name = uid_to_human[val]
      node_id_to_name[key] = name

    return node_id_to_name

  def id_to_string(self, node_id):
    if node_id not in self.node_lookup:
      return ''
    return self.node_lookup[node_id]


# To look at graph use:
# tensorboard --logdir ./Model/summ_MySecondModel/

# Choose a model, replace the model_name and the model_constructor accordingly
# to train and evaluate your own model.
#~ model_name = "MySecondModel"
#~ from LSTM_model import *
#~ model_constructor = build_LSTM_model
model_name = "BaselineModel"
from baseline_model import *
model_constructor = build_Baseline_model

########################################################################
# Debugging? (Use True on local machine.)
DEBUG = True
# Where to store the model 
model_path = "./Model/"
# Whether to use the stored model
# Throws an error if required model does not exist
# CAUTION: you may overwrite parameters of another model.
use_stored_model = False
# Hyperparameters (global constants)
BATCHSIZE = 32
N_EPOCHS = 240
if DEBUG: 
	N_EPOCHS = 4
# Max. number of words to use per question
max_sequence_length = 10
# If question is longer use first part (True) or last part (False)
first = True
# Save options
n_steps_per_save = 500
if DEBUG: 
	n_steps_per_save = 3
# Generate output in DEBUG mode
verbose = DEBUG
SEED = 42
np.random.seed(SEED)

print("General parameters:")
print("\tNumber of epochs: " + str(N_EPOCHS))
print("\tBatch Size: " + str(BATCHSIZE))
print("\tMax. Input Question Length: " + str(max_sequence_length))
print("\tSave model parameters every: " + str(n_steps_per_save) + " steps.")
print("\tUsing saved model? " + str(use_stored_model))
print("")

########################################################################
# Setup Data
# Path into data folder
path_dataset = './Data'
path_images = "./ImageData"
	
# Choose Training and validation dataset
name_t = "trainval2014_train"
name_v = "trainval2014_val"

# Import Image Features
name_train = "train2014"
name_val = "val2014"

# Load all the needed data (need to load image data in addition)
[quest_vocab_embd, quest_emb_dim, quest_vocab_dim, answer_vocab_size, 
					num_quests_train, quest_to_img_map_train, num_quests_val, 
					quest_to_img_map_val, train_quests, val_quests, answers_indxs_train,
					answers_indxs_val, all_answers, num_ans,
					n_img_features, image_train, image_val,img_dict
					] = load_all_the_data_jpg(path_dataset, path_images, max_sequence_length, name_t, name_v, name_train, name_val, first)
########################################################################
# Build the model
[train_step, loss_to_minimize, modif_accur, accuracy, img_feats_inpt, quest_feats_inpt, labels, all_answers_input] = model_constructor(n_img_features, quest_emb_dim, quest_vocab_dim, answer_vocab_size, max_sequence_length, batchsize = 32, seed = 42)

# Count the number of trainable parameters
count_pars()

# Set verbosity
tf.logging.set_verbosity(tf.logging.INFO)

# Do summary stuff
merged_summary_op = tf.summary.merge_all()

########################################################################
# Train the model
print("Training the model: " + model_name + " using the following parameters:")

# Define a saver
saver = tf.train.Saver()

create_graph()

with tf.Session() as sess:
	
	# Prepare summary writing
	summ_filename = model_path + "summ_" + model_name
	summary_writer = tf.summary.FileWriter(summ_filename, graph=tf.get_default_graph())
	
	#preprocess image using ImageNet
	# Some useful tensors:
	# 'softmax:0': A tensor containing the normalized prediction across
	#   1000 labels.
	# 'pool_3:0': A tensor containing the next-to-last layer containing 2048
	#   float description of the image.
	# 'DecodeJpeg/contents:0': A tensor containing a string providing JPEG
	#   encoding of the image.
	# Runs the softmax tensor by feeding the image_data as input to the graph.
	#maybe by putting something different then softmax:0 (which is the last layer I think)
	#we could get some intermediate representation and use this as input
	img_feat_tensor = sess.graph.get_tensor_by_name('pool_3:0')
	data_train = sess.run(img_feat_tensor,
						   {'DecodeJpeg/contents:0': image_train})#this specifies the input layer I think
	data_val = sess.run(img_feat_tensor,
						   {'DecodeJpeg/contents:0': image_val})#this specifies the input layer I think
			
			
	# Clear training error file
	with open(model_path + model_name + "_curr_loss.txt", "w") as f:
		pass
	
	sample_indx = 0
	if use_stored_model:
		saver.restore(sess, model_path + model_name + ".ckpt")
		with open(model_path + model_name + "_info.txt", "r") as f:
			sample_indx = int(f.read())
	else:
		sess.run(tf.global_variables_initializer())
	
	# Iterate over all epochs
	print("\nStarting training...")
	n_step_tot = num_quests_train / BATCHSIZE
	step = sample_indx / BATCHSIZE
	for k in range(N_EPOCHS):
		
		train_loss_epoch = 0.0
		print("Starting epoch " + str(k) + " of " + str(N_EPOCHS))
		# Iterate over all questions
		while True:
			# Prepare labels and input
			if verbose:
				print("Epoch: " + str(k + 1) + " of " + str(N_EPOCHS) + ", Step: " + str(step+1) + " of " + str(n_step_tot))
				print("Preparing input...")
			if sample_indx + BATCHSIZE < num_quests_train:
				[batch_labels, batch_img_feats, batch_quest_feats, _] = prepare_input(sample_indx, num_quests_train, 
								BATCHSIZE, answers_indxs_train, answer_vocab_size, n_img_features,
								quest_emb_dim, max_sequence_length, quest_to_img_map_train, data_train, data_val, name_train, name_val,
								train_quests, img_dict, all_answers, quest_vocab_dim)
			else:
				break
			
			# Do one train step
			if verbose:
				print("Doing a training step...")
			# Do a training step and return the loss
			_, train_loss, summary = sess.run([train_step, loss_to_minimize,  merged_summary_op], 
							feed_dict={img_feats_inpt: batch_img_feats, 
									  quest_feats_inpt: batch_quest_feats, 
									  labels: batch_labels,
									  })
			if verbose:  
				print("Loss: " + str(train_loss))
			train_loss_epoch += train_loss
			step += 1
			sample_indx += BATCHSIZE
			
			# Save model
			if not (step % n_steps_per_save):
				if verbose:
					print("Saving parameters...")
				with open(model_path + model_name + "_info.txt", "w") as f:
					f.write(str(sample_indx))
				save_path = saver.save(sess, model_path + model_name + ".ckpt")
				
			if DEBUG:
				# Only do 5 steps:
				if step == 5:
					break
		with open(model_path + model_name + "_curr_loss.txt", "a") as f:
			f.write(str(train_loss_epoch) + "\n")
		# Write accuracy to summary
		summary_writer.add_summary(summary, k)
		print("Training error of this epoch: " + str(train_loss_epoch))
		# Reset indices
		sample_indx = 0
		step = 0
		
	########################################################################
	# Evaluate the model on the validation data
	print("\nEvaluating model..")
	sample_indx = 0
	# Iterate over all questions
	n_batches = 0
	n_bat_tot = num_quests_val / BATCHSIZE
	accur = 0.0
	accur_human = 0.0
	while True:
		# Prepare labels and input
		if verbose:
			print("Step: " + str(n_batches) + " of " + str(n_bat_tot))
			print("Preparing input...")
		if sample_indx + BATCHSIZE < num_quests_val:
			[batch_labels, batch_img_feats, batch_quest_feats, batch_all_ans] = prepare_input(sample_indx, num_quests_val, 
							BATCHSIZE, answers_indxs_val, answer_vocab_size, n_img_features,
							quest_emb_dim, max_sequence_length, quest_to_img_map_val, data_train, data_val, name_train, name_val,
							val_quests, img_dict, all_answers, quest_vocab_dim, training = False)
		else:
			break
		
		# Do one evaluation step
		if verbose:
			print("Doing an evaluation step...")
		accur_curr, modif_accur_curr = sess.run([accuracy, modif_accur],
							feed_dict={img_feats_inpt: batch_img_feats, 
									  quest_feats_inpt: batch_quest_feats, 
									  labels: batch_labels,
									  all_answers_input: batch_all_ans,
									  })
		accur += accur_curr
		accur_human += modif_accur_curr

		if verbose:
			print("The accuracy of this batch is: " + str(accur_curr))
			print("The evaluation accuracy of this batch is: " + str(modif_accur_curr))
		
		n_batches += 1
		sample_indx += BATCHSIZE
		
		if DEBUG:
			# Only evaluate on first three batches
			if n_batches == 3:
				break
	
	print("The accuracy on the evaluation set is: " + str(accur / n_batches))
	print("The evaluation accuracy is: " + str(accur_human / n_batches))







