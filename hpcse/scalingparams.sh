#!/bin/bash
N_SAMPLES=$1
N_U=$2
LAMBDA=$3
limit=10

#N_Samples
rm scaling.txt
echo "${N_SAMPLES}" >> scaling.txt
echo "${N_U}" >> scaling.txt
echo "${LAMBDA}" >> scaling.txt
for((i=1;i<=${limit};i+=2))
do
    echo "${i}/${limit}"
    var=$((${N_SAMPLES}*${i}))
    ./extended_cmaes ${var} ${N_U} ${LAMBDA} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt >> scaling.txt
done

echo "next type">>scaling.txt

#N_U
for((i=1;i<=${limit};i+=2))
do
    echo "${i}/${limit}"
    var=$((${N_U}*${i}))
    ./extended_cmaes ${N_SAMPLES} ${var} ${LAMBDA} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt >> scaling.txt
done

echo "next type">>scaling.txt

#LAMBDA
for((i=1;i<=${limit};i+=2))
do
    echo "${i}/${limit}"
    var=$((${LAMBDA}*${i}))
    ./extended_cmaes ${N_SAMPLES} ${N_U} ${var} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt >> scaling.txt
done
