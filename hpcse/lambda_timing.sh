#!/bin/bash
N_SAMPLES=$1
N_U=$2
limit=40

rm lambda_timing.txt
echo "${N_SAMPLES}" >> lambda_timing.txt
echo "${N_U}" >> lambda_timing.txt
echo "*" >> lambda_timing.txt
for((LAMBDA=20;LAMBDA<=${limit};LAMBDA+=5))
do
	echo "${LAMBDA}/${limit}"
	./extended_cmaes ${N_SAMPLES} ${N_U} ${LAMBDA} > tempoutput.txt
	echo "${LAMBDA}" >> lambda_timing.txt
	grep -E "Total elapsed time = " tempoutput.txt >> lambda_timing.txt
done

#having some problems with python3 and matplotlib
python2 plot.py lambda_timing.txt
