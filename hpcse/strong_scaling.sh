#!/bin/bash
N_SAMPLES=$1
N_U=$2
LAMBDA=$3
limit=4

rm strong_scaling.txt
echo "${N_SAMPLES}" >> strong_scaling.txt
echo "${N_U}" >> strong_scaling.txt
echo "${LAMBDA}" >> strong_scaling.txt
for((nthreads=1;nthreads<=${limit};nthreads*=2))
do
    echo "${nthreads}/${limit}"
    ./extended_cmaes ${N_SAMPLES} ${N_U} ${LAMBDA} ${nthreads} > tempoutput.txt
    echo "${nthreads}" >> strong_scaling.txt
    grep -E "Total elapsed time = " tempoutput.txt >> strong_scaling.txt
done

#having some problems with python3 and matplotlib
python2 plot.py strong_scaling.txt
