#!/bin/bash
# ARE WE ON DAINT? #
DAINT=1
####################
n_u=12
lambda=3
# simulation settings
N_Sensors=3
PERFORMANCE=1000 # longer than other runs!!!
MPI=0
nnodes=1
#other
CMAES_VERBOSE=0

#order of params in file is: runtime nsamples nsensors nu lambda nthreads nnodes


# baseline
TYPE="cuda"
DIR=./timings_comparison/$TYPE
SCALETYPE=comparison
SDIR=$DIR/$SCALETYPE
mkdir -p $SDIR
CUDA=1
nthreads=1
# scenario 1, <1min
N_Samples=500
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: omp 12"

bash do_job.sh $TYPE $SCALETYPE $SDIR $DAINT $MPI $nnodes $n_u $lambda $nthreads $N_Samples
# scenario 2, <1min
N_Samples=2000
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: omp 12"

bash do_job.sh $TYPE $SCALETYPE $SDIR $DAINT $MPI $nnodes $n_u $lambda $nthreads $N_Samples
