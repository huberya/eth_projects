#!/bin/bash
N_SAMPLES=$1
N_U=$2
LAMBDA=$3
limit=4

#N_Samples
rm weak_scalingns.txt
echo "*s" >> weak_scalingns.txt
echo "${N_U}" >> weak_scalingns.txt
echo "${LAMBDA}" >> weak_scalingns.txt
ns=${N_SAMPLES}
for((nthreads=1;nthreads<=${limit};nthreads*=2))
do
    echo "${nthreads}/${limit}"
    ./extended_cmaes ${ns} ${N_U} ${LAMBDA} ${nthreads} > tempoutput.txt
    echo "${nthreads}" >> weak_scalingns.txt
    grep -E "Total elapsed time = " tempoutput.txt >> weak_scalingns.txt
    ns*=2
done


#N_U
rm weak_scalingns.txt
echo "${N_SAMPLES}" >> weak_scalingnu.txt
echo "*s" >> weak_scalingnu.txt
echo "${LAMBDA}" >> weak_scalingnu.txt
nu=${N_U}
for((nthreads=1;nthreads<=${limit};nthreads*=2))
do
    echo "${nthreads}/${limit}"
    ./extended_cmaes ${N_SAMPLES} ${nu} ${LAMBDA} ${nthreads} > tempoutput.txt
    echo "${nthreads}" >> weak_scalingnu.txt
    grep -E "Total elapsed time = " tempoutput.txt >> weak_scalingnu.txt
    nu*=2
done


#LAMBDA
rm weak_scalingl.txt
echo "${N_SAMPLES}" >> weak_scalingl.txt
echo "${N_U}" >> weak_scalingl.txt
echo "*s" >> weak_scalingl.txt
l=${LAMBDA}
for((nthreads=1;nthreads<=${limit};nthreads*=2))
do
    echo "${nthreads}/${limit}"
    ./extended_cmaes ${N_SAMPLES} ${N_U} ${l} ${nthreads} > tempoutput.txt
    echo "${nthreads}" >> weak_scalingl.txt
    grep -E "Total elapsed time = " tempoutput.txt >> weak_scalingl.txt
    l*=2
done

#having some problems with python3 and matplotlib
python2 plot.py weak_scalingns.txt

#having some problems with python3 and matplotlib
python2 plot.py weak_scalingnu.txt

#having some problems with python3 and matplotlib
python2 plot.py weak_scalingl.txt
