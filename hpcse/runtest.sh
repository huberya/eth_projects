#!/bin/bash
n_u=8
lambda=16
nsensors=3
#maximal threads PER NODE
maxthreads=12
maxnodes=2
# simulation settings
N_Samples=200
N_Sensors=3

CMAES_VERBOSE=0
DIR=./timings

#order of params in file is: runtime nsamples nsensors nu lambda nthreads nnodes

#base code (no mpi no cuda) NOT BASELINE!!!!
rm $DIR/basecodes.txt
#strongscaling
MPI=0
CUDA=0
PERFORMANCE=0
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: base code (no mpi no cuda) NOT BASELINE!!!!"

for((nthreads=1;nthreads<${maxthreads};nthreads*=2))
do
    ./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/basecodes.txt
done
nthreads=${maxthreads}
./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/basecodes.txt
echo "strong scaling done"

#weakscaling
rm $DIR/basecodew.txt
lambdan=${lambda}
for((nthreads=1;nthreads<${maxthreads};nthreads*=2))
do
    ./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/basecodew.txt
    lambdan=$(($lambdan*2))
done
nthreads=${maxthreads}
lambdan=$((${lambda}*${maxthreads}))
./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/basecodew.txt
echo "weak scaling done"
#mpi code (no cuda)
rm mpicodes.txt
#strongscaling
MPI=1
CUDA=0
PERFORMANCE=0
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: mpi code (no cuda)"

nthreads=${maxthreads}
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/mpicodes.txt
done
nnodes=${maxnodes}
mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/mpicodes.txt
echo "strong scaling done"

#weakscaling
rm $DIR/mpicodew.txt
lambdan=${lambda}
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/mpicodew.txt
    lambdan=$(($lambdan*2))
done
nnodes=${maxnodes}
lambdan=$((${lambda}*${maxnodes}))
mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/mpicodew.txt
echo "weak scaling done"

#cuda+mpi code
rm $DIR/cudacodes.txt
#strongscaling
make clean
MPI=1
CUDA=1
PERFORMANCE=0
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: cuda+mpi code"

nthreads=${maxthreads}
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/cudacodes.txt
done
nnodes=${maxnodes}
mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambda} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/cudacodes.txt
echo "strong scaling done"

#weakscaling
rm $DIR/cudacodew.txt
lambdan=${lambda}
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
    grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/cudacodew.txt
    lambdan=$(($lambdan*2))
done
nnodes=${maxnodes}
lambdan=$((${lambda}*${maxnodes}))
mpirun -n ${nnodes} ./extended_cmaes ${n_u} ${lambdan} ${nthreads} > tempoutput.txt
grep -E "Total elapsed time = " tempoutput.txt | cut -c22- >> $DIR/cudacodew.txt
echo "weak scaling done"
