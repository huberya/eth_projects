#!/bin/bash
module unload PrgEnv-cray
module load PrgEnv-gnu
module load daint-gpu
module load cudatoolkit
module load GSL
