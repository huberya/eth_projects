#!/usr/bin/env python2
import sys
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.patches as mpatches
import numpy as np

#do plotting from timing files (as input)
ifile=sys.argv[1]

f=open(ifile, "r")
lines=list(f)
nsamples=lines[0]
nu=lines[1]
lambdas=lines[2]
time=np.zeros((len(lines)-5)//3)
size=np.zeros((len(lines)-5)//3)
currline=3


title="Scaling of Basecode"
fontsize=14
xlabel="Scaling Factor"
ylabel="Time"
plt.gcf().set_size_inches(10, 7)
plt.title(title, fontsize=fontsize)
plt.xlabel(xlabel, fontsize=fontsize)
plt.ylabel(ylabel, fontsize=fontsize, rotation='horizontal', ha='right')

linew=2
i=0
while(True):
    if(lines[currline]=="next type\n"):
        break
    size[i]=1+2*i
    tempstring=lines[currline].split(' = ')[1]
    time[i]=tempstring.split("  ")[0]
    i+=1
    currline+=1
currline+=1
time=time/time[0]
plt.loglog(size,time, linewidth=linew)
ns_patch=mpatches.Patch(color='blue', label='NSamples')
i=0
while(True):
    if(lines[currline]=="next type\n"):
        break
    size[i]=1+2*i
    tempstring=lines[currline].split(' = ')[1]
    time[i]=tempstring.split("  ")[0]
    i+=1
    currline+=1
currline+=1
time=time/time[0]
plt.loglog(size,time, linewidth=linew)
nu_patch=mpatches.Patch(color='red', label='N_u')
i=0
while(True):
    if (currline==len(lines)):
        break
    size[i]=1+2*i
    tempstring=lines[currline].split(' = ')[1]
    time[i]=tempstring.split("  ")[0]
    i+=1
    currline+=1
time=time/time[0]
plt.loglog(size,time, linewidth=linew)
l_patch=mpatches.Patch(color='green', label='Lambda')

plt.legend(handles=[ns_patch, nu_patch, l_patch])
plt.savefig("scaling_basecode" + '.pdf', bbox_inches='tight')
