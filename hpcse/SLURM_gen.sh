#!/bin/bash
nodes=$1
runfile=$2
n_u=$3
lambda=$4
nthreads=$5
savefile=$6
TYPE=$7


# TODO
# calculate time?! or upper bound
TIME="10" # in minutes
if [ $TYPE = "base" ]; then
    TIME="60"
elif [ $TYPE = "mpi" ]; then
    TIME="40"
elif [ $TYPE = "cuda" ]; then
    TIME="25"
else
    TIME="40"
fi

FILE=run_timing_daint.sh

rm $FILE
echo \
"#!/bin/bash -l

#SBATCH --job-name=project5
#SBATCH --time=00:$TIME:00
#SBATCH --nodes=$nodes
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=$nthreads
#SBATCH --partition=normal
#SBATCH --constraint=gpu

export OMP_NUM_THREADS=\$SLURM_CPUS_PER_TASK

srun ./$runfile $n_u $lambda $nthreads $savefile
"\
>>$FILE



# do not forget "\$" 
# export OMP_NUM_THREADS=\$SLURM_CPUS_PER_TASK
