#!/bin/bash
N_SAMPLES=$1
LAMBDA=$2
limit=10

rm nu_timing.txt
echo "${N_SAMPLES}" >> nu_timing.txt
echo "*" >> nu_timing.txt
echo "${LAMBDA}" >> nu_timing.txt
for((N_u=1;N_u<=${limit};N_u++))
do
	echo "${N_u}/${limit}"
	./extended_cmaes ${N_SAMPLES} ${N_u} ${LAMBDA} > tempoutput.txt
	echo "${N_u}" >> nu_timing.txt
	grep -E "Total elapsed time = " tempoutput.txt >> nu_timing.txt
done

#having some problems with python3 and matplotlib
python2 plot.py nu_timing.txt
