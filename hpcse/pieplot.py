import matplotlib.pyplot as plt

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'cblas_dsymv', 'gsl_dmvnorm_v.', 'intmalloc', 'intfree', 'malloc_consolidate', ' malloc', 'powfinite', 'other'
sizes = [12.93, 5.61, 19.49, 10.67, 6.82, 6.79, 4.15, 33.54]
explode = (0, 0, 0, 0, 0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=40, pctdistance=1.15, labeldistance=1.25)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.savefig("baseline_complete" + '.pdf', bbox_inches='tight')

plt.clf()

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'memory handling', 'cblas_dsymv', 'gsl_dmvnorm_v', 'cblas_ddot', 'other' , 'powfinite', 'gsl_vector_set'
sizes = [55.08, 12.93, 5.61, 2.38, 17.04, 4.15, 2.81]
explode = (0, 0, 0, 0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=40, pctdistance=1.15, labeldistance=1.25)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.savefig("baseline_reduced" + '.pdf', bbox_inches='tight')


plt.clf()

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'cblas_dsymv', 'gsl_dmvnorm_v_optimized', 'cblas_ddot', 'gsl_vecotr_sub', 'gsl_vector_memcpy', 'gsl_blas_dsymv', '__ieee754_exp', 'other'
sizes = [46.23, 6.94, 8.46, 8.15, 7.37, 6.58, 5.01, 11.25]
explode = (0, 0, 0, 0, 0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=40, pctdistance=1.15, labeldistance=1.25)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.savefig("improved_complete" + '.pdf', bbox_inches='tight')

plt.clf()

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'memory handling', 'cblas_dsymv', 'gsl_dmvnorm_v_optimized', 'cblas_ddot', 'other', 'gsl_vector_sub', 'gsl_blas_dsymv', '__ieee754_exp'
sizes = [7.96, 45.89, 6.94, 8.46, 11.12, 8.15, 6.58, 5.02]
explode = (0, 0, 0, 0, 0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=40, pctdistance=1.15, labeldistance=1.25)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.savefig("improved_reduced" + '.pdf', bbox_inches='tight')
