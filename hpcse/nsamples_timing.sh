#!/bin/bash
N_U=$1
LAMBDA=$2
limit=200

rm nsamples_timing.txt
echo "*" >> nsamples_timing.txt
echo "${N_U}" >> nsamples_timing.txt
echo "${LAMBDA}" >> nsamples_timing.txt
for((N_SAMPLES=50;N_SAMPLES<=${limit};N_SAMPLES+=50))
do
	echo "${N_SAMPLES}/${limit}"
	./extended_cmaes ${N_SAMPLES} ${N_U} ${LAMBDA} > tempoutput.txt
	echo "${N_SAMPLES}" >> nsamples_timing.txt
	grep -E "Total elapsed time = " tempoutput.txt >> nsamples_timing.txt
done

#having some problems with python3 and matplotlib
python2 plot.py nsamples_timing.txt
