#include <stdio.h>

// cublas library
#include <cublas_v2.h>
// profiling
#include <cuda_profiler_api.h>
// blocksize definition, determined with nvidia visual profiler
#define BLOCK_SIZE 128

// TODO: go to float numbers, massive (2-3x) speedup and the results are approximated anyway..., can I make it switchable --> introduce a layer of abstraction in the case of texture memory
typedef double value_type;	// TODO: check how much improvement with float
typedef std::size_t size_type;

// wrapper for all C functions to allow compiling of C and CUDA (C++)
extern "C" {
    void initialize_gpu();
    void set_sigma_inv(double * in, double ax);
    void set_g_eval(double * g_eval);
    void update_e(double * e);
    void set_theta(double * theta);
    void precompute();
    void mvnpdf_m_gpu(double *);
    void free_gpu();
    void model_gpu(value_type d0, value_type d1, value_type mygamma);
    void start_profiling();
    void stop_profiling();
}

// http://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

// https://stackoverflow.com/questions/35137213/texture-objects-for-doubles
static __inline__ __device__ double fetch_double(uint2 p){
    return __hiloint2double(p.y, p.x);
}

// https://stackoverflow.com/questions/10589925/initialize-device-array-in-cuda
template<typename T>
__global__ void initKernel(T * devPtr, const T val, const size_t nwords)
{
    int tidx = threadIdx.x + blockDim.x * blockIdx.x;
    int stride = blockDim.x * gridDim.x;

    for(; tidx < nwords; tidx += stride)
        devPtr[tidx] = val;
}

// channel description, save double in two "coordinates"
cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 32, 0, 0, cudaChannelFormatKindUnsigned);
// resource description
struct cudaResourceDesc resDesc_g_eval;
struct cudaResourceDesc resDesc_winv_g_eval;
struct cudaResourceDesc resDesc_y;
struct cudaResourceDesc resDesc_winv_y;
struct cudaResourceDesc resDesc_sigma_inv;
// texture description
struct cudaTextureDesc texDesc;
// texture objects
cudaTextureObject_t texObj_g_eval = 0;
cudaTextureObject_t texObj_winv_g_eval = 0;
cudaTextureObject_t texObj_y = 0;
cudaTextureObject_t texObj_winv_y = 0;
cudaTextureObject_t texObj_sigma_inv = 0;

// TODO: use compile time variables to allow for __constant__ data
// device memory
value_type * d_g_eval;
value_type * d_winv_g_eval;
value_type * d_y;	// changing, overwritten reused for summation
value_type * d_winv_y;
value_type * d_sigma_inv;
value_type * d_theta;
value_type * d_ones;
value_type * d_e;
value_type * d_evid_matrix;

double ax_gpu;

cublasStatus_t stat;
cublasHandle_t handle;

// TODO: cudeDeviceSync ?

void create_texture(int numel, value_type * d_array, cudaResourceDesc * resDesc, cudaTextureObject_t * texObj){
    // Specify texture
    memset(resDesc, 0, sizeof(*resDesc));
    resDesc->resType = cudaResourceTypeLinear;
    resDesc->res.linear.devPtr = d_array;
    resDesc->res.linear.sizeInBytes = numel*sizeof(value_type);
    resDesc->res.pitch2D.desc = channelDesc; // format definition
    // create texture
    gpuErrchk(cudaCreateTextureObject(texObj, resDesc, &texDesc, NULL));
}

void free_gpu(){
    gpuErrchk(cudaFree(d_g_eval));
    gpuErrchk(cudaFree(d_winv_g_eval));
    gpuErrchk(cudaFree(d_y));
    gpuErrchk(cudaFree(d_winv_y));
    gpuErrchk(cudaFree(d_sigma_inv));
    gpuErrchk(cudaFree(d_evid_matrix));
    gpuErrchk(cudaFree(d_theta));
    gpuErrchk(cudaFree(d_ones));
    gpuErrchk(cudaFree(d_e));
    // Destroy texture object
    gpuErrchk(cudaDestroyTextureObject(texObj_g_eval));
    gpuErrchk(cudaDestroyTextureObject(texObj_winv_g_eval));
    gpuErrchk(cudaDestroyTextureObject(texObj_y));
    gpuErrchk(cudaDestroyTextureObject(texObj_winv_y));
    //~ gpuErrchk(cudaDestroyTextureObject(texObj_sigma_inv));
}

void initialize_gpu(){
    // allocate memory
    gpuErrchk(cudaMalloc(&d_g_eval, _N_SAMPLES*_N_SENSORS*sizeof(value_type))); // multiple access on all data
    gpuErrchk(cudaMalloc(&d_winv_g_eval, _N_SAMPLES*_N_SENSORS*sizeof(value_type))); // multiple access on all data
    gpuErrchk(cudaMalloc(&d_y, _N_SAMPLES*_N_SENSORS*sizeof(value_type)));	// single access ona all data
    gpuErrchk(cudaMalloc(&d_winv_y, _N_SAMPLES*_N_SENSORS*sizeof(value_type)));	// single access ona all data
    gpuErrchk(cudaMalloc(&d_sigma_inv, _N_SENSORS*_N_SENSORS*sizeof(value_type)));	// single access ona all data
    gpuErrchk(cudaMalloc(&d_evid_matrix, _N_SAMPLES*_N_SAMPLES*sizeof(value_type))); // multiple access on all data
    gpuErrchk(cudaMalloc(&d_theta, 2*_N_SAMPLES*sizeof(value_type)));
    gpuErrchk(cudaMalloc(&d_ones, _N_SAMPLES*sizeof(value_type)));
    gpuErrchk(cudaMalloc(&d_e, _N_SAMPLES*_N_SENSORS*sizeof(value_type)));
    // set to one
    initKernel<<<ceil(_N_SAMPLES/(double)BLOCK_SIZE),BLOCK_SIZE>>>(d_ones, 1.0, _N_SAMPLES);
	
    // Specify texture object parameters
    memset(&texDesc, 0, sizeof(texDesc));
    texDesc.filterMode = cudaFilterModePoint; 
    texDesc.readMode = cudaReadModeElementType;
    texDesc.normalizedCoords = false;
	
    // create textures
    create_texture(_N_SAMPLES*_N_SENSORS, d_g_eval, &resDesc_g_eval, &texObj_g_eval);
    create_texture(_N_SAMPLES*_N_SENSORS, d_winv_g_eval, &resDesc_winv_g_eval, &texObj_winv_g_eval);
    create_texture(_N_SAMPLES*_N_SENSORS, d_y, &resDesc_y, &texObj_y);
    create_texture(_N_SAMPLES*_N_SENSORS, d_winv_y, &resDesc_winv_y, &texObj_winv_y);
    //~ create_texture(d_sigma_inv, &resDesc_sigma_inv, &texObj_sigma_inv);
	
    stat = cublasCreate(&handle);
    if (stat != CUBLAS_STATUS_SUCCESS) {
        printf ("CUBLAS initialization failed\n");
    }
	
    // TODO: do check later if everything is constructed
    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
}

// TODO: more work for one thread?
__global__ void update_y(value_type __restrict__ * d_e, value_type __restrict__ * d_g_eval, value_type __restrict__ *d_y){
    int idx = BLOCK_SIZE*blockIdx.x + threadIdx.x;
    if(idx < _N_SAMPLES*_N_SENSORS){
	d_y[idx] = d_g_eval[idx] + d_e[idx];
    }
}

// !!! we switch now to column major...
void set_sigma_inv(double * in, double ax){
    ax_gpu = ax;
    stat = cublasSetMatrix (_N_SENSORS, _N_SENSORS, sizeof(*in), in, _N_SENSORS, d_sigma_inv, _N_SENSORS);
    if (stat != CUBLAS_STATUS_SUCCESS) {
	printf ("data download failed");
	cudaFree (d_sigma_inv);
    }
}
void set_g_eval(double * g_eval){
    stat = cublasSetMatrix (_N_SENSORS, _N_SAMPLES, sizeof(value_type), g_eval, _N_SENSORS, d_g_eval, _N_SENSORS);
    if (stat != CUBLAS_STATUS_SUCCESS) {
	printf ("data download failed");
	cudaFree (d_g_eval);
    }
}
void update_e(double * e){
    stat = cublasSetMatrix (_N_SENSORS, _N_SAMPLES, sizeof(*e), e, _N_SENSORS, d_e, _N_SENSORS);
    if (stat != CUBLAS_STATUS_SUCCESS) {
	printf ("data download failed");
	cudaFree (d_e);
    }
    update_y<<<ceil(_N_SENSORS*_N_SAMPLES/(double)BLOCK_SIZE), BLOCK_SIZE>>>(d_e, d_g_eval, d_y);
    double alpha = 1.0;
    double beta = 0.0;
    cublasDsymm(handle, CUBLAS_SIDE_LEFT, CUBLAS_FILL_MODE_LOWER,
                           _N_SENSORS, _N_SAMPLES,
                           &alpha,
                           d_sigma_inv, _N_SENSORS,
                           d_y, _N_SENSORS,
                           &beta,
                           d_winv_y, _N_SENSORS);
}

void set_theta(double * theta){
    gpuErrchk(cudaMemcpy(d_theta, theta, 2*_N_SAMPLES*sizeof(value_type), cudaMemcpyHostToDevice));
}

// multiply two matrices by sigma_inv / winv
void precompute(){
    double alpha = 1.0;
    double beta = 0.0;
    cublasDsymm(handle, CUBLAS_SIDE_LEFT, CUBLAS_FILL_MODE_LOWER,
                           _N_SENSORS, _N_SAMPLES,
                           &alpha,
                           d_sigma_inv, _N_SENSORS,
                           d_g_eval, _N_SENSORS,
                           &beta,
                           d_winv_g_eval, _N_SENSORS);
}


// _N_SAMPLES * _N_SAMPLES sums are done at the same time
__global__ void d_mvnpdf_m_gpu(cudaTextureObject_t texObj_y, cudaTextureObject_t texObj_g_eval,
	cudaTextureObject_t texObj_winv_y, cudaTextureObject_t texObj_winv_g_eval,
	value_type * __restrict__ d_evid_matrix, double fac)
{
    value_type dot = 0;
    int i = blockIdx.y;
    int idx = BLOCK_SIZE*blockIdx.x + threadIdx.x;
    if(idx < _N_SAMPLES){
        // unroll directive
        for(int j = 0; j < _N_SENSORS; ++j){
            dot +=    (fetch_double(tex1Dfetch<uint2>(texObj_y, i*_N_SENSORS + j)) - 	fetch_double(tex1Dfetch<uint2>(texObj_g_eval,idx*_N_SENSORS + j)))
                * (fetch_double(tex1Dfetch<uint2>(texObj_winv_y, i*_N_SENSORS + j)) - 	fetch_double(tex1Dfetch<uint2>(texObj_winv_g_eval,idx*_N_SENSORS + j)));	// reduced complexity of the calulation O(Mn^3) --> O(Mn^2)
        }
        d_evid_matrix[i*_N_SAMPLES + idx] = exp(-0.5*dot)*fac;
    }
}

__global__ void log_subtraction(value_type * __restrict__ d_evid_matrix, value_type * __restrict__ d_y){
    int idx = BLOCK_SIZE*blockIdx.x + threadIdx.x;
    if(idx < _N_SAMPLES){
	d_y[idx] = log(d_evid_matrix[idx*_N_SAMPLES + idx]/d_y[idx]);
    }
}

void mvnpdf_m_gpu(double * result){
    dim3 Dg(ceil(_N_SAMPLES/(double)BLOCK_SIZE), _N_SAMPLES);
    // precompute value for GPU
    value_type fac = 1.0/sqrt( pow((2*M_PI),_N_SENSORS)*ax_gpu );
    d_mvnpdf_m_gpu<<<Dg,BLOCK_SIZE>>>(
            texObj_y, texObj_g_eval,
            texObj_winv_y, texObj_winv_g_eval,
            d_evid_matrix, fac);
    // cublas reduction, ddot with one vector
    double alpha = 1.0/_N_SAMPLES;
    double beta = 0.;
    stat = cublasDgemv(handle, CUBLAS_OP_T,
                           _N_SAMPLES, _N_SAMPLES,
                           &alpha,
                           d_evid_matrix, _N_SAMPLES,
                           d_ones, 1,
                           &beta,
                           d_y, 1);
    if (stat != CUBLAS_STATUS_SUCCESS) {
        printf ("dgemv failed");
    }
    log_subtraction<<<Dg.x, BLOCK_SIZE>>>(d_evid_matrix, d_y);
    // cublas reduction, ddot with one vector
    stat = cublasDdot (handle, _N_SAMPLES, d_y, 1, d_ones, 1, result);
    if (stat != CUBLAS_STATUS_SUCCESS) {
        printf ("ddot failed");
    }
    // TODO: isn't in the original code a 1/N_samples missing?!
    //*result /= _N_SAMPLES;
}

__global__ void model_gpu_d(value_type * __restrict__ d_g_eval, value_type * __restrict__ d_theta, value_type mygamma, value_type d0, value_type d1){
    int idx = blockIdx.x * BLOCK_SIZE + threadIdx.x;
    int j = idx/_N_SENSORS;
    int i = idx - j*_N_SENSORS;
    if( i > _N_SENSORS || j > _N_SAMPLES){
	return;
    }
    d_g_eval[j*_N_SENSORS + i] = mygamma*d_theta[j*2+1]/(2*M_PI*(pow((d0+i*d1)-d_theta[j*2+0],2.0) + pow(d_theta[j*2+1],2)));
}


void model_gpu(double d0, double d1, double mygamma){
    int Dg = ceil(_N_SAMPLES*_N_SENSORS/(double)BLOCK_SIZE);
    model_gpu_d<<<Dg,BLOCK_SIZE>>>(d_g_eval, d_theta, mygamma, d0, d1);
}

void start_profiling(){
    cudaProfilerStart();
}
void stop_profiling(){
    cudaProfilerStop();
}
