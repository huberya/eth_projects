/*
 *  extended_utility.c
 *
 *  Created by Panagiotis Hadjidoukas (CSE Lab, D-MAVT, ETH Zurich).
 *  Copyright 2017 ETH Zurich. All rights reserved.
 *
 */

#include <string.h>
#include <omp.h>
#include "auxil.c"

#ifdef CUDA
// definition of C++ function, separatly compiled with nvcc
extern void initialize_gpu();
extern void set_sigma_inv(double * in, double ax);
extern void set_g_eval(double * g_eval);
extern void update_e(double * e);
extern void set_theta(double * theta);
extern void precompute();
extern void mvnpdf_m_gpu(double *);
extern void free_gpu();
extern void model_gpu(double d0, double d1, double mygamma);
extern void start_profiling();
extern void stop_profiling();

// for cuda profiling
int step = 0;

double extended_utility(double *d)
{
    double Uo;  // output

    double *theta = utility_data.theta;
    double **z = utility_data.z;
    double *sigma = utility_data.sigma;
    double lamb = utility_data.lamb;
    int corr_error = utility_data.corr_error;
    int N_u = utility_data.N_u;

if(step < 1){
    start_profiling();
}
    //////////////////////////////////
    // simple_utility.m
    // This function provides the utility function for the vortex 
    // optimal sensor placement problem.
    // Input:
    // g         : function handle to model
    // d         : vector of design parameters, d = [x_s, h]
    // theta     : samples of model parameters
    // z         : error samples, normally distributed
    // sigma     : covariance matrix of error
    // lamb      : correlation length
    // corr_error: flag indicating whether spatial error correlation
    //             is assumed.
    // Author: Franziska Krummenacher, krummenf@student.ethz.ch
    // Spring/Summer 2016
    //////////////////////////////////

    //extract number of samples and sensors
    //N_samples = size(theta,1); // = n samples
    //N_sensors = size(sigma,1); // = M sensors


    set_theta(theta);

    //// evaluate model for given design parameters

    //g_eval = g(d);
    model_gpu(d[0], d[1], model_data.mygamma);

    //// build correlation matrix, second part (non-diagonal entries)
    // if correlated prediction error: fill other entries of sigma
    if(corr_error)
    {
        for (int i = 0; i < _N_SENSORS; i++)
        {
            for (int j = 0; j < _N_SENSORS; j++)
            {
                if(i != j && d[1] > 1e-8)
                {
                    //%if different sensors at different places
                    sigma[i*_N_SENSORS+j] = sigma[0*_N_SENSORS+0]*exp(-fabs((i-j)*d[1])/lamb);
                }
                else if (i != j)
                {
                    //%if sensors are too close:
                    //%avoid singular matrices: use small value for d(2)
                    sigma[i*_N_SENSORS+j] = sigma[0*_N_SENSORS+0]*exp(-fabs((i-j)*1e-8)/lamb);
                }
            }
        }
    } // TODO: perfect for cuda
    
    update_globals(sigma);

    set_sigma_inv(get_winv_pointer(), get_ax());   
    precompute();

    //cholesky decomposition of sigma
    //A = chol(sigma);
    double A[_N_SENSORS*_N_SENSORS];
    
    gsl_matrix *work = gsl_matrix_alloc(_N_SENSORS,_N_SENSORS);
    memcpy(work->data,sigma,_N_SENSORS*_N_SENSORS*sizeof(double));
    gsl_linalg_cholesky_decomp(work);
    memcpy(A, work->data,_N_SENSORS*_N_SENSORS*sizeof(double));
    gsl_matrix_free(work);

    //build error from covariance matrix and normally distributed error
    //e = z*A;

    double e[N_u][_N_SAMPLES*_N_SENSORS];

    for (int n=0; n<N_u; n++)
    {
        for (int i=0; i<_N_SAMPLES; i++)
        {
            for (int j=0; j<_N_SENSORS; j++)
            {
                e[n][i*_N_SENSORS+j] = 0.0;
                for (int k=0; k<=j; k++)    // peh: note this, TODO: What is this for?
                {
                    e[n][i*_N_SENSORS+j] += z[n][i*_N_SENSORS+k]*A[k*_N_SENSORS+j];
                }
            }
        }
    }// TODO: port to cuda if it is clear what kind of multiplication it is., z stays the same for the whole simulation!

    //// approximate utility function
    double U[N_u];
    for (int i = 0; i < N_u; i++) U[i] = 0.0;

    for (int n = 0; n < N_u; n++)
    {
        // use other errors and update y
        update_e(e[n]);
        //compute likelihood
        mvnpdf_m_gpu(&U[n]);
        
    }   // for n
    // free memory
    free_globals();
if(step < 1){
    stop_profiling();
}
step++;
    Uo = 0.0;
    for (int n = 0; n < N_u; n++) Uo += U[n];
    Uo /= N_u;

    return Uo;
}
#else
double extended_utility(double *d)
{
    //double gt0, gt1, gt2, gt3;
    //gt0=torc_gettime();
    double Uo;  // output

    double *theta = utility_data.theta;
    double **z = utility_data.z;
    double *sigma = utility_data.sigma;
    double lamb = utility_data.lamb;
    int corr_error = utility_data.corr_error;
    int N_u = utility_data.N_u;


    //////////////////////////////////
    // simple_utility.m
    // This function provides the utility function for the vortex 
    // optimal sensor placement problem.
    // Input:
    // g         : function handle to model
    // d         : vector of design parameters, d = [x_s, h]
    // theta     : samples of model parameters
    // z         : error samples, normally distributed
    // sigma     : covariance matrix of error
    // lamb      : correlation length
    // corr_error: flag indicating whether spatial error correlation
    //             is assumed.
    // Author: Franziska Krummenacher, krummenf@student.ethz.ch
    // Spring/Summer 2016
    //////////////////////////////////

    //extract number of samples and sensors
    //N_samples = size(theta,1);
    //N_sensors = size(sigma,1);


    //// evaluate model for given design parameters
    double g_eval[_N_SAMPLES*_N_SENSORS];

    //g_eval = g(d);
    model(g_eval, d);
    
    //// build correlation matrix, second part (non-diagonal entries)
    // if correlated prediction error: fill other entries of sigma
    if(corr_error)
    {
        for (int i = 0; i < _N_SENSORS; i++)
        {
            for (int j = 0; j < _N_SENSORS; j++)
            {
                if(i != j && d[1] > 1e-8)
                {
                    //%if different sensors at different places
                    sigma[i*_N_SENSORS+j] = sigma[0]*exp(-fabs((i-j)*d[1])/lamb);
                }
                else if (i != j)
                {
                    //%if sensors are too close:
                    //%avoid singular matrices: use small value for d(2)
                    sigma[i*_N_SENSORS+j] = sigma[0]*exp(-fabs((i-j)*1e-8)/lamb);
                }
            }
        }
    }


    //cholesky decomposition of sigma
    //A = chol(sigma);
    double A[_N_SENSORS*_N_SENSORS];
    
    gsl_matrix *work = gsl_matrix_alloc(_N_SENSORS, _N_SENSORS);
    memcpy(work->data,sigma,_N_SENSORS*_N_SENSORS*sizeof(double));
    gsl_linalg_cholesky_decomp(work);
    memcpy(A, work->data,_N_SENSORS*_N_SENSORS*sizeof(double));
    gsl_matrix_free(work);

    //build error from covariance matrix and normally distributed error
    //e = z*A;

    double e[N_u][_N_SAMPLES*_N_SENSORS];
    
    Uo = 0.0;
    //// approximate utility function
    double U[N_u];
    
 //#pragma omp for schedule(static)
        for (int i = 0; i < N_u; i++) U[i] = 0.0;
        #pragma omp parallel
    {        
        #pragma omp for //probably pretty irrelevant
        for (int n=0; n<N_u; n++)
        {

            for (int i=0; i<_N_SAMPLES; i++)
            {
                for (int j=0; j<_N_SENSORS; j++)
                {
                    e[n][i*_N_SENSORS+j] = 0.0;
                    for (int k=0; k<=j; k++)    // peh: note this
                    {
                        e[n][i*_N_SENSORS+j] += z[n][i*_N_SENSORS+k]*A[k*_N_SENSORS+j];
                    }
                }
            }
        }

        update_globals(&g_eval[0],  sigma);
        
        #pragma omp for //doesn't look terrible
        for (int n = 0; n < N_u; n++)
        {

            //construct data from evaluated model and error
            double y[_N_SAMPLES*_N_SENSORS];

            //y = g_eval + e;
            for (int i = 0; i < _N_SAMPLES; i++)
                for (int j = 0; j < _N_SENSORS; j++)
                    y[i*_N_SENSORS+j] = g_eval[i*_N_SENSORS+j] + e[n][i*_N_SENSORS+j];


            //compute likelihood
            //double likelihood = mvnpdf(y,g_eval,sigma); // built in - multivariate pdf
            double likelihood[_N_SAMPLES];
            
            //#pragma omp parallel for
            for (int i = 0; i < _N_SAMPLES; i++) //iterate through samples of \theta
            {
                //compute evidence
                //double evid_array = mvnpdf(y_i,g_eval, sigma);    // for each 
                double evid_array[_N_SAMPLES];
                
                //this takes like 90% of time according to profiler
                mvnpdf_v_optimized(evid_array, &y[i*_N_SENSORS]); 

                //inner loop: take mean of all terms in the evidence
                //double evid = sum(evid_array);
                //evid = evid/N_samples;
                double evid = 0;
                for (int k = 0; k < _N_SAMPLES; k++)
                {
                    evid += evid_array[k];
                    if(i == k){
                        likelihood[i] = evid_array[k];
                    }
                }
                evid /= _N_SAMPLES;
            
                //outer loop: add logarithms

                U[n] = U[n] + log(likelihood[i])-log(evid); // division to save one log

            }
            

        }   // for n
    
    //gt2=torc_gettime();
}
        //#pragma omp for reduction(+:Uo)
        for (int n = 0; n < N_u; n++) Uo += U[n];
    Uo /= N_u;
    //gt3=torc_gettime();
    /*printf("(inner)Total elapsed time = %.3lf  seconds\n", gt3-gt0);
            printf("(inner)Initialization time = %.3lf  seconds\n", gt1-gt0);
            printf("(inner)Processing time = %.3lf  seconds\n", gt2-gt1);
            printf("(inner)Finalization time = %.3lf  seconds\n", gt3-gt2);*/
    return Uo;
}
#endif
