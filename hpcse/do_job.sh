#!/bin/bash
TYPE=$1
SCALETYPE=$2
DIR=$3
DAINT=$4
MPI=$5
nodes=$6
n_u=$7
lambda=$8
nthreads=$9
suffix=_${10}

savefile=$DIR/nodes_"$nodes"_nthreads_"$nthreads"$suffix.txt
echo $savefile
# make a function for reoccuring part
if [ $DAINT = 1 ]; then
    runfile=extended_cmaes_"$TYPE"_"$nodes"_"$nthreads"_"$SCALETYPE"$suffix
    cp extended_cmaes $runfile
    bash SLURM_gen.sh $nodes $runfile $n_u $lambda $nthreads $savefile $TYPE
    sbatch run_timing_daint.sh
else
    if [ $MPI = 1 ]; then
	mpirun -n ${nodes} ./extended_cmaes ${n_u} ${lambda} ${nthreads} $savefile
    else
	./extended_cmaes ${n_u} ${lambda} ${nthreads} $savefile
    fi
fi
