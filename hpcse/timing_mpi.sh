#!/bin/bash
# ARE WE ON DAINT? #
DAINT=1
####################
n_u=12
lambda=128
#maximal threads PER NODE
maxthreads=12
maxnodes=64
# simulation settings
N_Samples=500
N_Sensors=3
MPI=1
CUDA=0
PERFORMANCE=500

#other
TYPE="mpi"
DIR=./timings/$TYPE
CMAES_VERBOSE=0

#order of params in file is: runtime nsamples nsensors nu lambda nthreads nnodes

#mpi code (no cuda)
nthreads=${maxthreads}
#strongscaling, 45min
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: mpi code (no cuda)"
SCALETYPE=strong
SUBDIR=$DIR/$SCALETYPE
mkdir -p $SUBDIR
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nnodes $n_u $lambda $nthreads
done
nnodes=${maxnodes}
bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nnodes $n_u $lambda $nthreads
echo "strong scaling done"

#weakscaling, 45min
SCALETYPE=weak
SUBDIR=$DIR/$SCALETYPE
mkdir -p $SUBDIR
lambdan=${lambda}
for((nnodes=1;nnodes<${maxnodes};nnodes*=2))
do
    bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nnodes $n_u $lambdan $nthreads
    lambdan=$(($lambdan*2))
done
nnodes=${maxnodes}
lambdan=$((${lambda}*${maxnodes}))
bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nnodes $n_u $lambdan $nthreads
echo "weak scaling done"

