import numpy as np
import pylab

folders=["original", "base", "omp", "cuda", "cuda12"]
# cuda runs are 10x longer
invscaling=[1,1,1,10,10]
labels=["Original", "Optimized", "OpenMP", "CUDA", "CUDA+OpenMP"]
threads=[1,1,12,1,12]
Ns=[500,2000]

timings=np.zeros((5,2))

counter = 0
for folder in folders:
    for i in [0,1]:
	filename = folder +  "/" + "comparison/nodes_1_nthreads_" + str(threads[counter]) + "_" + str(Ns[i]) + ".txt"
	timings[counter,i] = pylab.loadtxt(filename)[0]/invscaling[counter]
    counter = counter + 1

speedup=timings[0,:]/timings
print(timings)
print(speedup)

out_array = np.empty([5, 4], dtype=object)
out_array.fill('')
out_array[:,0] = labels
for i in range(out_array.shape[0]):
    out_array[i,1]=threads[i]
    out_array[i,2]="{0:.1f}".format(speedup[i,0])
    out_array[i,3]="{0:.1f}".format(speedup[i,1])

# https://tex.stackexchange.com/questions/54990/convert-numpy-array-into-tabular
out_string = " \\\\\n".join([" & ".join(map(str,line)) for line in out_array])
# latex compatibility
out_string=out_string.replace("%", "\%")
print(out_string)
tex_file = open("comparison.tex", "w")
tex_file.write(out_string)
tex_file.close()
