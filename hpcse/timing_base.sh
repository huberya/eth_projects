#!/bin/bash
# ARE WE ON DAINT? #
DAINT=1
####################
n_u=12
lambda=4
#maximal threads PER NODE
maxthreads=12
maxnodes=2
# simulation settings
N_Samples=500
N_Sensors=3
MPI=0
CUDA=0
PERFORMANCE=500

#other
TYPE="base"
DIR=./timings/$TYPE
CMAES_VERBOSE=0

#order of params in file is: runtime nsamples nsensors nu lambda nthreads nnodes

#base code (no mpi no cuda) NOT BASELINE!!!!
nodes=1
#strongscaling
make clean
OPT="-D_N_SAMPLES=$N_Samples -D_N_SENSORS=$N_Sensors -DCMAES_VERBOSE=$CMAES_VERBOSE" \
MPI=$MPI CUDA=$CUDA PERFORMANCE=$PERFORMANCE make
echo "Compilation done: base code (no mpi no cuda) NOT BASELINE!!!!"
SCALETYPE=strong
SUBDIR=$DIR/$SCALETYPE
mkdir -p $SUBDIR
for((nthreads=1;nthreads<${maxthreads};nthreads*=2))
do
    bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nodes $n_u $lambda $nthreads
done
nthreads=${maxthreads}
bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nodes $n_u $lambda $nthreads

echo "strong scaling done"

#weakscaling
SCALETYPE=weak
SUBDIR=$DIR/$SCALETYPE
mkdir -p $SUBDIR
n_un=${n_u}
for((nthreads=1;nthreads<${maxthreads};nthreads*=2))
do
    bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nodes $n_un $lambda $nthreads
    n_un=$(($n_un*2))
done
nthreads=${maxthreads}
n_un=$((${n_u}*${maxthreads}))
bash do_job.sh $TYPE $SCALETYPE $SUBDIR $DAINT $MPI $nodes $n_un $lambda $nthreads
echo "weak scaling done"
