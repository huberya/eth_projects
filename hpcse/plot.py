#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import numpy as np

#do plotting from timing files (as input)
ifile=sys.argv[1]

f=open(ifile, "r")
lines=list(f)
nsamples=lines[0]
nu=lines[1]
lambdas=lines[2]
time=np.zeros((len(lines)-3)//2)
size=np.zeros((len(lines)-3)//2)
for i in range(3,len(lines),2):
    size[(i-3)//2]=lines[i]
    tempstring=lines[i+1].split(' = ')[1]
    time[(i-3)//2]=tempstring.split("  ")[0]

title="NSamples="+nsamples+" N_u="+nu+" Lambda="+lambdas
fontsize=14
xlabel=""
#i did not manage to split the \n at the end of line
if nsamples=="*\n":
    xlabel="NSamples"
elif nu=="*\n":
    xlabel="N_u"
elif lambdas=="*\n":
    xlabel="Lambda"
else:
    xlabel="NThreads"
ylabel="Time"
plt.gcf().set_size_inches(10, 7)
plt.title(title, fontsize=fontsize)
plt.xlabel(xlabel, fontsize=fontsize)
plt.ylabel(ylabel, fontsize=fontsize, rotation='horizontal', ha='right')
plt.plot(size,time)
plt.savefig(title + '.pdf', bbox_inches='tight')
