#!/usr/bin/env python2
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.mlab as mlab
import numpy as np

#How to get all of the immediate subdirectories in Python
#https://stackoverflow.com/questions/800197/how-to-get-all-of-the-immediate-subdirectories-in-python
def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]
            
def get_immediate_files(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isfile(os.path.join(a_dir, name))]
      
#do plotting from timing files (as input)
ifolder=sys.argv[1]
code=get_immediate_subdirectories(ifolder)
for coden in code:
    scaling=get_immediate_subdirectories(os.path.join(ifolder,coden))
    #print(scaling)
    for scalingn in scaling:
        print(coden + " - " + scalingn + " scaling")
        files=get_immediate_files(os.path.join(os.path.join(ifolder,coden),scalingn))
        #print (files)
        nfiles=len(files)
        times=np.zeros(nfiles)
        nsamples=np.zeros(nfiles)
        nsensors=np.zeros(nfiles)
        n_u=np.zeros(nfiles)
        lambda_=np.zeros(nfiles)
        nthreads=np.zeros(nfiles)
        nnodes=np.zeros(nfiles)
        for i in range(nfiles):
            f=open(os.path.join(os.path.join(os.path.join(ifolder,coden),scalingn),files[i]), "r")
            lines=list(f)
            if(not len(lines)==1):
                print("unexpected number of lines in file")
                print (files[i])
                sys.exit();
            tempstring=lines[0].split("\t")
            times[i]=tempstring[0]
            nsamples[i]=tempstring[1]
            nsensors[i]=tempstring[2]
            n_u[i]=tempstring[3]
            lambda_[i]=tempstring[4]
            nthreads[i]=tempstring[5]
            nnodes[i]=tempstring[6]
        title=coden+"code "+scalingn+"scaling\n"
        xlabel="";
        xval=np.zeros(nfiles)
        fontsize=14
        linew=3.0
        if(np.max(nsamples)==np.min(nsamples)):
            title+="nsamples "+str(nsamples[0])+", "
        else:
            title+="nsamples "+str(np.min(nsamples))+"-"+str(np.max(nsamples))+", "
        if(np.max(nsensors)==np.min(nsensors)):
            title+="nsensors "+str(nsensors[0])+", "
        else:
            title+="nsensors "+str(np.min(nsensors))+"-"+str(np.max(nsensors))+", "
        if(np.max(n_u)==np.min(n_u)):
            title+="N_u "+str(n_u[0])+", "
        else:
            title+="N_u "+str(np.min(n_u))+"-"+str(np.max(n_u))+", "
        if(np.max(lambda_)==np.min(lambda_)):
            title+="Lambda "+str(lambda_[0])+", "
        else:
            title+="Lambda "+str(np.min(lambda_))+"-"+str(np.max(lambda_))+", "
        if(np.max(nthreads)==np.min(nthreads)):
            title+="threads per node "+str(nthreads[0])
        else:
            xlabel="Number of Threads"
            xval=nthreads
        if(np.max(nnodes)==np.min(nnodes)):
            title+="NNodes "+str(nnodes[0])
        else:
            xlabel="Number of Nodes"
            xval=nnodes
        
        #Is it possible to sort two lists(which reference each other) in the exact same way?
        #https://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w
        xval, times = (list(t) for t in zip(*sorted(zip(xval, times))))
        plt.clf()
        plt.gcf().set_size_inches(10, 7)
        plt.title(title, fontsize=fontsize)
        yval=np.zeros(nfiles)
        if(scalingn=="strong"):
            plt.xlabel(xlabel, fontsize=fontsize)
            plt.ylabel("Speedup", fontsize=fontsize, rotation='horizontal', ha='right')
            for i in range(nfiles):
                yval[i]=times[0]/times[i]
        else:
            plt.xlabel(xlabel, fontsize=fontsize)
            plt.ylabel("Efficiency", fontsize=fontsize, rotation='horizontal', ha='right')
            for i in range(nfiles):
                yval[i]=times[0]/times[i]
        if(coden=="base"):
            plt.plot(xval, yval, linewidth=linew)
            plt.xlim(1.0, plt.xlim()[1])
            if(scalingn=="weak"):
                plt.ylim(0.0, plt.ylim()[1])
        else:
            if(scalingn=="strong"):
                plt.loglog(xval,yval, linewidth=linew)
            else:
                plt.semilogx(xval,yval, linewidth=linew)
                plt.ylim(0.0, plt.ylim()[1])
        #plt.xlim(0.0, plt.xlim()[1])
        if(scalingn=="strong"):
            xopt=[1,np.max(xval)]
            yopt=[1,np.max(xval)]
        else:
            xopt=[1,np.max(xval)]
            yopt=[1,1]
        plt.plot(xopt,yopt, "r--", linewidth=linew)
        measured_patch = mpatches.Patch(color='blue', label='measured')
        optimal_patch=mpatches.Patch(color='red', linestyle='--', label='optimal')
        plt.legend(handles=[measured_patch, optimal_patch])
        plt.savefig(coden+"code "+scalingn+"scaling" + ".pdf", bbox_inches='tight')
        
        #need to plot efficiency for strongscaling
        if(scalingn=="strong"):
            plt.clf()
            plt.gcf().set_size_inches(20, 14)
            plt.title(title, fontsize=fontsize)
            plt.xlabel(xlabel, fontsize=fontsize)
            plt.ylabel("Efficiency", fontsize=fontsize, rotation='horizontal', ha='right')
            yvale=np.zeros(nfiles)
            for i in range(nfiles):
                yvale[i]=yval[i]/xval[i]
            if(coden=="base"):
                plt.plot(xval, yvale, linewidth=linew)
            else:
                plt.semilogx(xval,yvale, linewidth=linew)
            plt.ylim(0.0, plt.ylim()[1]+0.1)
            xopt=[1,np.max(xval)]
            yopt=[1,1]
            plt.plot(xopt,yopt, "r--", linewidth=linew)
            measured_patch = mpatches.Patch(color='blue', label='measured')
            plt.legend(handles=[measured_patch])
            plt.savefig(coden+"code "+scalingn+"scaling_E" + ".pdf", bbox_inches='tight') 
            
        # efficiency tables
        out=[]
        efficiency=[]
        speedup=[]
        if(scalingn=="strong"):
            speedup = times[0]/times
            efficiency = speedup/xval
            out=np.zeros((len(xval),3))
            out[:,0]=xval
            out[:,1]=speedup
            out[:,2]=efficiency*100
        else:
            efficiency = times[0]/times
            out=np.zeros((len(xval),2))
            out[:,0]=xval
            out[:,1]=efficiency*100
        out_array = np.empty(out.shape, dtype=object)
        out_array.fill('')
        for i in range(out_array.shape[0]):
            out_array[i,0]=str(int(out[i,0]))
            out_array[i,1]="{0:.1f}".format(out[i,1])
            out_array[i,-1]="{0:.1f}".format(out[i,-1])+"%"
            
        # https://tex.stackexchange.com/questions/54990/convert-numpy-array-into-tabular
        out_string = " \\\\\n".join([" & ".join(map(str,line)) for line in out_array])
        # latex compatibility
        out_string=out_string.replace("%", "\%")
        print(out_string)
        filename = ifolder + "/" + coden + "_" + scalingn + ".tex"
        tex_file = open(filename, "w")
        tex_file.write(out_string)
        tex_file.close()
        
