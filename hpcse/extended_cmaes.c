/*
 *  extended_cmaes.c
 *
 *  Created by Panagiotis Hadjidoukas (CSE Lab, D-MAVT, ETH Zurich).
 *  Copyright 2017 ETH Zurich. All rights reserved.
 *
 */


#define _XOPEN_SOURCE 500
#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h> /* free() */
#include <assert.h>
#include <omp.h>
#ifdef USEMPI
#include <mpi.h>
#endif
#include "cmaes_interface.h"
#if defined(_USE_TORC_)
#include <torc.h>
#else
#include <sys/time.h>
double torc_gettime()
{
        struct timeval t;
        gettimeofday(&t, NULL);
        return (double)t.tv_sec + (double)t.tv_usec*1.0E-6;
}

#define torc_register_task(x)
#define torc_init(a,b,c)
#define torc_finalize()
#define torc_enable_stealing()
#define torc_disable_stealing()
#define torc_waitall()
#endif
#include <unistd.h>
#include <math.h>

#ifndef CMAES_VERBOSE
#define CMAES_VERBOSE 1
#endif
#define _STEALING_
/*#define _RESTART*/
#define IODUMP 0
#include "utility/fitfun_extended.c" 

/* the objective (fitness) function to be minimized */
void taskfun(double *x, int *pn, double *res, int *info)
{
    int n = *pn;
    int gen, chain, step, task;
    gen = info[0]; chain = info[1]; step = info[2]; task = info[3];

    double t0 = torc_gettime();
    double f = +fitfun(x, n, (void *)NULL, info);   /* CMA-ES needs this minus sign */
    double t1 = torc_gettime();

#if CMAES_VERBOSE
#ifdef USEMPI
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank==0)
    {
        printf("task (%d,%d,%d,%d): %f %f = %f in %.3f secs\n", gen, chain, step, task, x[0], x[1], f, t1-t0);
    }
#else
    printf("task (%d,%d,%d,%d): %f %f = %f in %.3f secs\n", gen, chain, step, task, x[0], x[1], f, t1-t0);
#endif
#endif
    *res = f;
    return;
}

double lower_bound[] = {-5, 0};
double upper_bound[] = {10, 5};

int is_feasible0(double *pop, int dim)
{
    int i, good;
    for (i = 0; i < dim; i++) {
        good = (lower_bound[i] <= pop[i]) && (pop[i] <= upper_bound[i]);
        if (!good) {
            return 0;
        }
    }

    return 1;
}



#ifdef USEMPI
int  cma_worker(int myrank, int nranks) {

    int lambda, dim, N;
    MPI_Bcast(&lambda, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dim, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD); //need to additionally bcast N

    int lambda_local = lambda/nranks;
    double local_pop[lambda_local*dim];
    double local_arFunvals[lambda_local];
    int step;
    while(1)
    {
        MPI_Bcast(&step, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (step == -1) break;

        MPI_Scatter(NULL, lambda_local*dim, MPI_DOUBLE, local_pop, lambda_local*dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);     

        for (int i = 0; i < lambda_local; i++)
        {
            int info[4];
            info[0] = info[1] = 0; info[2] = step; info[3] = i+lambda_local*myrank;
            taskfun(&local_pop[i*dim], &N, &local_arFunvals[i], info);
        }

        MPI_Gather(local_arFunvals, lambda_local, MPI_DOUBLE, NULL, lambda_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
    return 0;
}
#endif


/* the optimization loop */
int main(int argn, char **args)
{
    //first 3 arguments changed to be problemsize: N_u (fitfun_extended.c), lambda*/
    if(argn<3)
    {
        printf("usage: ./extended_cmaes N_u, lambda\n");
        return 0;
    }
#ifdef USEMPI
    int provided;
    MPI_Init_thread(&argn,&args,MPI_THREAD_FUNNELED,&provided);
    // we need to be able to communicate at least from the main thread
    assert(provided >= MPI_THREAD_FUNNELED);
    int res;
    
    int rank, size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    

    
    const int N_cmaes = 1;  //%number of CMA-ES runs with different samples
    
    /*some argumentsplitting so that argn2, args2 fit argn and args from the original version->can be passed to torc*/
    /*int argn2=argn-3;
    char* args2[argn2];
    args2[0]=args[0];
    for (int i=1;i<argn2;++i)
    {
        args2[i]=args[i+3];
    }*/
    int ompthreadspernode;
    ompthreadspernode=1;
    if(argn>3)
    {
        omp_set_num_threads(atoi(args[3]));
        ompthreadspernode=atoi(args[3]);
    }
    
    //const int N_samples=atoi(args[1]);
    const int N_u=atoi(args[1]);
    /*these arguments are passed to fitfun_extended.c*/
    
    ///part for rank 0
    if(rank==0)
    {
        
        //const int N_samples=100;
        //const int N_u=8;
        
        int i; 
        /* peh - start */
        double gt0, gt1, gt2, gt3;
        /* peh - end */

        torc_register_task(taskfun);

        /* Initialize everything into the struct evo, 0 means default */
        torc_init(argn, args, MODE_MS);

        fitfun_init(N_u);
        gsl_rand_init(1); //used to be 1


        double bestever_x[N_cmaes][2];
        double bestever_f[N_cmaes];

        //need to care about who does what and how to collect
        //MPI_Barrier(MPI_COMM_WORLD);
        for (int c = 0; c < N_cmaes; c++)
        {

            gt0 = torc_gettime();
            fitfun_init_zetas();

            int N = 2;
            double startp[2];
            double xll[2];
            double xrl[2];
            int lambda = atoi(args[2]);//32
            //int lambda=16;
            double inrgstddev[2];
            int maxfuncalls = 3200;
            int fevals;
            double optp[2], optv;
            int iter;

            for (int i = 0; i < N; i++) xll[i] = lower_bound[i];
            for (int i = 0; i < N; i++) xrl[i] = upper_bound[i];

            for (int i = 0; i < N; i++) {
                startp[i] = xll[i] + 0.5*(xrl[i]-xll[i]);
                inrgstddev[i] = 0.25*(xrl[i]-xll[i]); 
            }

            long iseed = time(0);
            gt1 = torc_gettime();
            int ret = cma_optimize(N, startp, xll, xrl, lambda, inrgstddev, iseed, maxfuncalls, optp, &fevals, &optv, &iter);
            gt2 = torc_gettime();


            gt3 = torc_gettime();
            printf("Total elapsed time = %.3lf\t%.4d\t%.3d\t%.3d\t%.6d\t%.3d\t%.4d\n", gt3-gt0, _N_SAMPLES, _N_SENSORS, N_u, lambda, ompthreadspernode, size);
            printf("Initialization time = %.3lf  seconds\n", gt1-gt0);
            printf("Processing time = %.3lf  seconds\n", gt2-gt1);
            printf("Finalization time = %.3lf  seconds\n", gt3-gt2);
#ifdef _PERFORMANCE_
            if(argn > 4){
                FILE *f = fopen(args[4], "w");
                if (f == NULL){
                    printf("Error opening file!\n");
                    exit(1);
                }
                fprintf(f,"%.3lf\t%.4d\t%.3d\t%.3d\t%.6d\t%.3d\t%.4d\n", gt3-gt0, _N_SAMPLES, _N_SENSORS, N_u, lambda, ompthreadspernode, size);
                fclose(f);
            }
#endif
            bestever_x[c][0] = optp[0];
            bestever_x[c][1] = optp[1];
            bestever_f[c] = optv;

        }

        int i_best = 0;
        double f_best = bestever_f[0];

        for (int c = 0; c < N_cmaes; c++)
        {
            if (bestever_f[c] < f_best) 
            {
                f_best = bestever_f[c];
                i_best = c;
            }
            printf("CMAES %d:  %f %f -> %f\n", c, bestever_x[c][0], bestever_x[c][1], bestever_f[c]);
        }


        //%% plotting
        {
        double optp[2];
        optp[0] = bestever_x[i_best][0];
        optp[1] = bestever_x[i_best][1];

        double x[_N_SENSORS];
        //%translate min into normal sensor coordinates
        x[0] = optp[0]; //minVal(1);
        for (int i = 1; i < _N_SENSORS; i++)
            x[i] = optp[0] + i*optp[1]; 

        //%all sensors are placed on the x-axis
        double y[_N_SENSORS];
        memset(y, 0, _N_SENSORS*sizeof(double));
        //%plot sampling area, sensors at optimal locations and samples

        //rectangle('Position',[x0_l y0_l,x0_u-x0_l, y0_u-y0_l]);
        //hold on;
        //scatter(x,y);
        FILE *fp;
        fp = fopen("extendedVortex500Samples_sensors.txt", "w");
        for (i = 0; i < _N_SENSORS; i++)
        {
            fprintf(fp, "%f %f\n", x[i], y[i]);
        }
        fclose(fp);

        //scatter(theta(:,1),theta(:,2));
        //legend('sensors', 'samples');
        //savefig('simpleVortex500Samples')
        double *theta = utility_data.theta;
        fp = fopen("extendedVortex500Samples_theta.txt", "w");
        for (i = 0; i < _N_SAMPLES; i++)
        {
            fprintf(fp, "%f %f\n", theta[i*2+0], theta[i*2+1]);
        }
        fclose(fp);

        }

        torc_finalize();

        fitfun_finalize();
    }
    
    ///part orther ranks
    else
    {
        torc_register_task(taskfun);

        /* Initialize everything into the struct evo, 0 means default */
        torc_init(argn, args, MODE_MS);

        fitfun_init(N_u);
        gsl_rand_init(1); //used to be 1
        //MPI_Barrier(MPI_COMM_WORLD);
        for (int c = 0; c < N_cmaes; c++)
        {
            fitfun_init_zetas();
            int res=cma_worker(rank, size);
        }
        torc_finalize();

        fitfun_finalize();
    }
#else
   
    /*some argumentsplitting so that argn2, args2 fit argn and args from the original version->can be passed to torc*/
    /*int argn2=argn-3;
    char* args2[argn2];
    args2[0]=args[0];
    for (int i=1;i<argn2;++i)
    {
        args2[i]=args[i+3];
    }*/
    int ompthreadspernode;
    ompthreadspernode=1;
    if(argn>3)
    {
        omp_set_num_threads(atoi(args[3]));
        ompthreadspernode=atoi(args[3]);
    }
    /*these arguments are passed to fitfun_extended.c*/
    const int N_u=atoi(args[1]);
    
    //const int N_samples=100;
    //const int N_u=8;
    
    int i; 
    /* peh - start */
    double gt0, gt1, gt2, gt3;
    /* peh - end */

    torc_register_task(taskfun);

    /* Initialize everything into the struct evo, 0 means default */
    torc_init(argn, args, MODE_MS);

    fitfun_init(N_u);
    gsl_rand_init(1);

    const int N_cmaes = 1;  //%number of CMA-ES runs with different samples

    double bestever_x[N_cmaes][2];
    double bestever_f[N_cmaes];

    //can probably splitup using mpi since they are independent
    for (int c = 0; c < N_cmaes; c++)
    {

        gt0 = torc_gettime();
        fitfun_init_zetas();

        int N = 2;
        double startp[2];
        double xll[2];
        double xrl[2];
        int lambda = atoi(args[2]);//32
        //int lambda=16;
        double inrgstddev[2];
        int maxfuncalls = 3200;
        int fevals;
        double optp[2], optv;
        int iter;

        for (int i = 0; i < N; i++) xll[i] = lower_bound[i];
        for (int i = 0; i < N; i++) xrl[i] = upper_bound[i];

        for (int i = 0; i < N; i++) {
            startp[i] = xll[i] + 0.5*(xrl[i]-xll[i]);
            inrgstddev[i] = 0.25*(xrl[i]-xll[i]); 
        }

        long iseed = time(0);
        gt1 = torc_gettime();
        int ret = cma_optimize(N, startp, xll, xrl, lambda, inrgstddev, iseed, maxfuncalls, optp, &fevals, &optv, &iter);
        gt2 = torc_gettime();


        gt3 = torc_gettime();
        printf("Total elapsed time = %.3lf\t%.4d\t%.3d\t%.3d\t%.6d\t%.3d\t%.4d\n", gt3-gt0, _N_SAMPLES, _N_SENSORS, N_u, lambda, ompthreadspernode, 1);
        printf("Initialization time = %.3lf  seconds\n", gt1-gt0);
        printf("Processing time = %.3lf  seconds\n", gt2-gt1);
        printf("Finalization time = %.3lf  seconds\n", gt3-gt2);
#ifdef _PERFORMANCE_
            if(argn > 4){
                FILE *f = fopen(args[4], "w");
                if (f == NULL){
                    printf("Error opening file!\n");
                    exit(1);
                }
                fprintf(f,"%.3lf\t%.4d\t%.3d\t%.3d\t%.6d\t%.3d\t%.4d\n", gt3-gt0, _N_SAMPLES, _N_SENSORS, N_u, lambda, ompthreadspernode, 1);
                fclose(f);
            }
#endif
        bestever_x[c][0] = optp[0];
        bestever_x[c][1] = optp[1];
        bestever_f[c] = optv;

    }

    int i_best = 0;
    double f_best = bestever_f[0];

    for (int c = 0; c < N_cmaes; c++)
    {
        if (bestever_f[c] < f_best) 
        {
            f_best = bestever_f[c];
            i_best = c;
        }
        printf("CMAES %d:  %f %f -> %f\n", c, bestever_x[c][0], bestever_x[c][1], bestever_f[c]);
    }


    //%% plotting
    {
    double optp[2];
    optp[0] = bestever_x[i_best][0];
    optp[1] = bestever_x[i_best][1];

    double x[_N_SENSORS];
    //%translate min into normal sensor coordinates
    x[0] = optp[0]; //minVal(1);
    for (int i = 1; i < _N_SENSORS; i++)
        x[i] = optp[0] + i*optp[1]; 

    //%all sensors are placed on the x-axis
    double y[_N_SENSORS];
    memset(y, 0, _N_SENSORS*sizeof(double));
    //%plot sampling area, sensors at optimal locations and samples

    //rectangle('Position',[x0_l y0_l,x0_u-x0_l, y0_u-y0_l]);
    //hold on;
    //scatter(x,y);
    FILE *fp;
    fp = fopen("extendedVortex500Samples_sensors.txt", "w");
    for (i = 0; i < _N_SENSORS; i++)
    {
        fprintf(fp, "%f %f\n", x[i], y[i]);
    }
    fclose(fp);

    //scatter(theta(:,1),theta(:,2));
    //legend('sensors', 'samples');
    //savefig('simpleVortex500Samples')
    double *theta = utility_data.theta;
    fp = fopen("extendedVortex500Samples_theta.txt", "w");
    for (i = 0; i < _N_SAMPLES; i++)
    {
        fprintf(fp, "%f %f\n", theta[i*2+0], theta[i*2+1]);
    }
    fclose(fp);

    }

    torc_finalize();

    fitfun_finalize();
#endif

#ifdef USEMPI
    MPI_Finalize();
#endif
    return 0;
}


static int is_feasible(double *x, double *xll, double *xrl, int n){
    int i, good;
    for (i=0; i<n; i++){
        good = (xll[i] <= x[i]) && (x[i] <= xrl[i]);
        if (!good) {
            return 0;
        }
    }
    return 1;
}

#define TERM_CONVERGE   0
#define TERM_MAXFEVALS  1
#define TERM_MAXITERS   2
#define TERM_OTHER  -1

int  cma_optimize(int N, double startp[], double *xll, double *xrl, int lambda, double *inrgstddev, long iseed, int maxfuncalls, double optp[], int *fevals, double *optv, int *iter) {
    //double gt0, gt1, gt2, gt3;
    cmaes_t evo; /* an CMA-ES type struct or "object" */
    double *arFunvals, *const*pop, *xfinal;
    int i, term = TERM_OTHER;
    int dim, step;

    *fevals = 0;

    /* Initialize everything into the struct evo, 0 means default */

    arFunvals = cmaes_init(&evo, N, startp, inrgstddev, iseed, lambda, "none");
    evo.sp.stopMaxFunEvals = maxfuncalls;
    dim = cmaes_Get(&evo, "dim");
    lambda = cmaes_Get(&evo, "lambda");

#ifdef USEMPI
    int rank, nranks;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nranks);
    

    MPI_Bcast(&lambda, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dim, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
#endif

    /* Iterate until stop criterion holds */
    step = 0;

#if _PERFORMANCE_
    for(int iter=0;iter<_LIMIT_;++iter)
    {
        //cmaes_TestForTermination(&evo);
#else
    while(!cmaes_TestForTermination(&evo))
    {
#endif

            //gt0=torc_gettime();

        /* generate lambda new search points, sample population */
                pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */

        /*  Here you may resample each solution point pop[i] until it
            becomes feasible, e.g. for box constraints (variable
            boundaries). function is_feasible(...) needs to be
            user-defined.
            Assumptions: the feasible domain is convex, the optimum is
            not on (or very close to) the domain boundary, initialX is
            feasible and initialStandardDeviations are sufficiently small
            to prevent quasi-infinite looping.
        */

        for (i = 0; i < cmaes_Get(&evo, "popsize"); ++i)
        {
            while (!is_feasible(pop[i], xll, xrl, dim)){
                cmaes_ReSampleSingle(&evo, i);
            }
        }
        //gt1=torc_gettime();
        /* evaluate the new search points using fitfun from above */
        //#pragma omp parallel for shared(pop, arFunvals, fevals) firstprivate(N)//(for some reason there are more iterations with this parallel for...idk why)
        //problem is related to cmaes_TestForTermination and probably some racecondition?
        //is order relevant? (what is lambda exactly)
#ifdef USEMPI
        MPI_Bcast(&step, 1, MPI_INT, 0, MPI_COMM_WORLD);

        int lambda_local = lambda/nranks;
        double pop_buf[lambda*dim];
        double local_pop[lambda_local*dim];
        double local_arFunvals[lambda_local];

        for (i = 0; i < lambda; ++i) {
            int d;
            for (d = 0; d < dim; ++d) {
                pop_buf[(i)*dim+d] = pop[i][d];
            }
        }

        MPI_Scatter(pop_buf, lambda_local*dim, MPI_DOUBLE, local_pop, lambda_local*dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);  

        for (i = 0; i < lambda_local; i++)
        {
            int info[4];
            info[0] = info[1] = 0; info[2] = step; info[3] = i;
            taskfun(&local_pop[i*dim], &N, &local_arFunvals[i], info);
        }
        (*fevals)+=lambda;

        MPI_Gather(local_arFunvals, lambda_local, MPI_DOUBLE, arFunvals, lambda_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
#else
        for (i = 0; i < lambda; ++i) {
                //arFunvals[i] = fitfun(pop[i], (int) dim);
            int info[4];
            info[0] = info[1] = 0; info[2] = step; info[3] = i;
            taskfun(pop[i], &N, &arFunvals[i], info);
            (*fevals)++; 
        }
#endif
        
        //gt2=torc_gettime();
        /* update the search distribution used for cmaes_SampleDistribution() */
        cmaes_UpdateDistribution(&evo, arFunvals);

#if CMAES_VERBOSE
                {
                const double *xbever = cmaes_GetPtr(&evo, "xbestever");
                double fbever = cmaes_Get(&evo, "fbestever");

                printf("BEST @ %5d: ", step);
                for (i = 0; i < dim; i++)
                        printf("%25.16lf ", xbever[i]);
                printf("%25.16lf\n", fbever);
                }
#endif

        step++;
        /*gt3=torc_gettime();
        printf("(inner)Total elapsed time = %.3lf  seconds\n", gt3-gt0);
    printf("(inner)Initialization time = %.3lf  seconds\n", gt1-gt0);
    printf("(inner)Processing time = %.3lf  seconds\n", gt2-gt1);
    printf("(inner)Finalization time = %.3lf  seconds\n", gt3-gt2);*/
    }
#ifdef USEMPI
    step = -1;  MPI_Bcast(&step, 1, MPI_INT, 0, MPI_COMM_WORLD);
#endif
#if _PERFORMANCE_
    term=TERM_MAXITERS;
#else
    if (strstr(cmaes_TestForTermination(&evo), "MaxIter") != NULL) {
        term = TERM_MAXITERS;
    } else if (strstr(cmaes_TestForTermination(&evo), "MaxFunEvals") != NULL) {
        term = TERM_MAXFEVALS;
    } else if (strstr(cmaes_TestForTermination(&evo), "TolFun") != NULL) {
                term = TERM_CONVERGE;
        } else if (strstr(cmaes_TestForTermination(&evo), "TolX") != NULL) {
        term = TERM_CONVERGE;
    }
#endif

    /* get best estimator for the optimum, xmean */
    xfinal = cmaes_GetNew(&evo, "xbestever"); /* "xbestever" might be used as well */

    *optv = cmaes_Get(&evo, "fbestever");

    *iter = (int)cmaes_Get(&evo, "iteration");

    for(i=0; i<dim; i++){
        optp[i] = xfinal[i];
    }

    cmaes_exit(&evo); /* release memory */

    /* do something with final solution and finally release memory */
    free(xfinal);
    return term;
}
