import numpy as np
import math

def poly_ker(x, y):
    return np.power(np.dot(x,y) + 1, 2)
   
ker_var = 4.5
fac = -0.5 * ker_var * ker_var
def RBF_ker(x, y):
    return np.exp(fac * np.linalg.norm(x-y))

np.random.seed(42)

# This does definitely not work like this
def Nystroem(X, m = 500, ker = RBF_ker):
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    
    # Nystroem Features
    S = X[np.random.choice(n_data_p, m, replace=False), :]
    KSS = np.zeros((m,m))
    for k in range(m):
        for j in range(m):
            KSS[k,j] = ker(S[k,:], S[j,:])
    D, V = np.linalg.eig(KSS)
    Z = np.zeros((n_data_p, m))
    for k in range(m):
        for j in range(n_data_p):
            Z[j,k] = ker(X[j,:], S[k,:])
    Z = np.matmul(Z, V) / np.sqrt(D)
    return np.sqrt(6000 / 2) * Z

def RFF(X, n_rand_feat = 400, ker_var = 4.5):
    np.random.seed(42)
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    # Random Fourier Features
    from math import pi
    bs = np.random.uniform(0, 2*pi,(1,n_rand_feat))
    ws = np.random.normal(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.logistic(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.gumbel(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.rayleigh(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.standard_t(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.chisquare(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.exponential(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.triangular(-ker_var, 0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.zipf(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.weibull(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.vonmises(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.pareto(ker_var, (n_feat, n_rand_feat))
    #ws = np.random.beta(ker_var, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.uniform(-ker_var, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.standard_cauchy((n_feat, n_rand_feat)) * ker_var
    #ws = np.random.laplace(0, ker_var, (n_feat, n_rand_feat))
    F = np.cos(np.matmul(X, ws) + np.matmul(np.ones((n_data_p, 1)), bs))
    return F

def RFF_TEFM_G(X, n_rand_feat = 400, ker_var = 1, q = 1, l = 500):
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    # Random Fourier Features
    from math import pi
    bs = np.random.uniform(0, 2*pi,(1,n_rand_feat))
    ws = np.random.normal(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.standard_cauchy((n_feat, n_rand_feat))
    #ws = np.random.laplace(0, ker_var, (n_feat, n_rand_feat))
    F = np.cos(np.matmul(X, ws) + np.matmul(np.ones((n_data_p, 1)), bs))
    # TEFM-G algorithm
    theta = np.random.standard_normal((n_data_p, l))
    #Y = np.matmul(np.linalg.matrix_power(np.matmul(F.transpose(), F), q),np.matmul(F.transpose(), theta))
    Y = np.matmul(F.transpose(), theta)
    Q,R = np.linalg.qr(Y)
    return np.matmul(F, Q)


def RMFM(X, n_rand_feat=5000, p=2, degree = 2):    
    a=np.array([math.factorial(degree) / math.factorial(degree - i) / math.factorial(i) for i in range(degree+1)])
    n=np.array(range(degree+1), dtype=int)
    parr = np.array([1.0/np.power(p,i+1) for i in range(degree+1)])
    parr=parr/np.sum(parr)
    np.random.seed(42)
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    Z=np.zeros((n_data_p, n_rand_feat))
    for i in range(n_rand_feat):
        N=np.random.choice(n,p=parr) 
        t=np.ones((n_data_p,))*np.sqrt(a[N]*np.power(p,(N+1))) #make array const with value
        for j in range(N):
            w=2*np.random.randint(0,2,n_feat)-1 #need {1,1}^d
            t = t * np.matmul(X,w)
        Z[:,i]=t[:]
    return 1.0/np.sqrt(n_rand_feat)*Z
    
def data_augment(X, Y, n_factor=1, mean=0, var=1, disturbed=0.01):
    n_data_p=X.shape[0]
    n_feat=X.shape[1]
    Xout=np.zeros((n_factor*n_data_p, n_feat))
    Yout=np.zeros((n_factor*n_data_p))
    np.random.seed(42)
    for i in range(n_factor):
        for j in range(n_data_p):
            for k in range(n_feat):
                if np.random.uniform(0,1)<disturbed:
                    t=np.random.normal(mean, var)
                    Xout[i*n_data_p+j,k]=X[j,k]+t
                else:
                    Xout[i*n_data_p+j,k]=X[j,k]
                Yout[i*n_data_p+j]=Y[j]
    return [Xout, Yout]


def RMFM_ArbDegree(X, n_rand_feat=5000, p=2, degree = 2):    
    a=np.array([math.factorial(degree) / math.factorial(degree - i) / math.factorial(i) for i in range(degree+1)])
    n=np.array(range(degree+1), dtype=int)
    parr = np.array([1.0/np.power(p,i+1) for i in range(degree+1)])
    parr=parr/np.sum(parr)
    np.random.seed(42)
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    Z=np.zeros((n_data_p, n_rand_feat))
    for i in range(n_rand_feat):
        N=np.random.choice(n,p=parr) 
        t=np.ones((n_data_p,))*np.sqrt(a[N]*np.power(p,(N+1))) #make array const with value
        for j in range(N):
            w=2*np.random.randint(0,2,n_feat)-1 #need {1,1}^d
            t = t * np.matmul(X,w)
        Z[:,i]=t[:]
    return 1.0/np.sqrt(n_rand_feat)*Z 
