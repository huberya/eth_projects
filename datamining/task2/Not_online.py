import numpy as np
import math

# python runner.py ./data/handout_train.txt ./data/handout_test.txt Not_online.py

def transform(X):
	from all_transfs import Nystroem
	return X


def mapper(key, value):
    # key: None
    # value: 2D array where each row corresponds to one "image"
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])    
    targets = trnsf[:,0]
    features = trnsf[:,1:]
    n_feat = features.shape[1]    
    yield 0, np.concatenate((np.array([n_feat, n_data_p]), trnsf.flatten()))  # This is how you yield a key, value pair


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here. 
    
    n_vals = len(values)
    n_feat_list = [int(values[i][0]) for i in range(n_vals)]
    n_data_p_list = [int(values[i][1]) for i in range(n_vals)]
    tot_dp = sum(n_data_p_list)
    all_targets = np.zeros((tot_dp,))
    all_data = np.zeros((tot_dp, n_feat_list[0]))
    
    values = [np.reshape(values[i][2:], (n_data_p_list[i], n_feat_list[i] + 1)) for i in range(n_vals)]
    indx = 0
    for k in range(n_vals):
		ind2 = n_data_p_list[k]
		all_targets[indx:indx+ind2] = values[k][:,0]
		all_data[indx:indx+ind2,:] = values[k][:,1:]
		indx = indx + ind2
		       
    n_data_p = tot_dp
    targets = all_targets
    features = transform(all_data)
    n_feat = features.shape[1]
    
    
    w_t = basic_proj_SGD(features, targets)
    
    # Error computation
    err = np.maximum(np.ones(targets.shape) - targets * np.matmul(features, w_t), np.zeros(targets.shape)).sum()
    acc = 1-np.absolute(((np.matmul(features, w_t) > 0) - (0.5 * targets + 0.5 * np.ones((n_data_p,))))).sum() / n_data_p
    print("Empirical Risk: " + str(err))
    print("Accuracy: " + str(acc))
    yield w_t
