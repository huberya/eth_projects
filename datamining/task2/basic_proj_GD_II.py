# python runner.py --log ./data/handout_train.txt ./data/handout_test.txt basic_proj_GD_II.py

import numpy as np

def eta_t(t):
    return 0.5 / np.power(t + 1, 0.3)

def ADAM(features, targets, lam = 100, n_iter_max = 10000, n_avg_wv = 100, beta1 = 0.9,
         beta2 = 0.9, eta_t = eta_t):
    
    n_data_p = features.shape[0]
    n_feat = features.shape[1]
    
    w_t = np.zeros(n_feat)
    
    # Parameters
    eps = 0.00000001
    m_t = 0
    v_t = 0
    
    # Convergence criteria
    tol = 10e-4
    convg_ct = 0
    max_convg_ct = n_data_p / 4
    indx = 0
    ct = 1
    
    # Weight normalization
    weightsnormalization = False # The weight normalization is False
    
    batchsize = 20
    indxs = np.zeros(batchsize)
    
    # Take average over last 'n_avg_wv' weight vectors
    w_array = np.zeros((n_avg_wv, n_feat))
    indx_wv = 0
    
    # while not converged
    while True:
        eta = eta_t(ct)
        indxs = np.random.randint(0, n_data_p, batchsize)
        tempsum = np.zeros(n_feat)
        for i in range(batchsize):
            if targets[indxs[i]] * np.dot(features[indxs[i]], w_t) < 1: # wrongly classified
                tempsum=tempsum+ targets[indxs[i]] * features[indxs[i]]
        grad = -1 * tempsum / batchsize
        m_t = beta1 * m_t + (1.0 - beta1) * grad
        v_t = beta2 * v_t + (1.0 - beta2) * grad * grad
        mhat = m_t / (1.0 - np.power(beta1, ct))
        vhat = v_t / (1.0 - np.power(beta2, ct))
        w_t = w_t - eta * mhat / (np.sqrt(vhat) + eps)
        if weightsnormalization:
            w_t = min(1, lam / np.linalg.norm(w_t)) * w_t
        # Storing the last few w_t
        w_array[indx_wv,:] = w_t
        indx_wv = (indx_wv + 1) % n_avg_wv
        indx = (indx + 1) % n_data_p
        if np.linalg.norm(w_t - w_array[1,:]) < tol:
            convg_ct = convg_ct + 1
        else:
            convg_ct = 0
        if convg_ct >= max_convg_ct:
            print("Converged!")
            break
        ct = ct + 1
        if ct > n_iter_max:
            print("Max number of iterations reached.")
            break
    return np.mean(w_array, axis=0)

def RFF(X, n_rand_feat = 400, ker_var = 1):
    np.random.seed(42)
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    # Random Fourier Features
    from math import pi
    bs = np.random.uniform(0, 2*pi,(1,n_rand_feat))
    ws = np.random.normal(0, ker_var, (n_feat, n_rand_feat))
    F = np.cos(np.matmul(X, ws) + np.matmul(np.ones((n_data_p, 1)), bs))
    return F


np.random.seed(42)

def transform(X):
    return RFF(X, n_rand_feat = 20000, ker_var = 6) # ker_var = 6

def mapper(key, value):
    # key: None
    # value: one line of input file
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])
    targets = trnsf[:,0]
    features = trnsf[:,1:]
    features = transform(features)
    n_feat = features.shape[1]
    
    # beta1 and beta2 are equal to 0.9
    w_t = ADAM(features, targets, lam = 100, n_iter_max = 1000, n_avg_wv = 100, beta1 = 0.9, beta2 = 0.9)
    
    #~ # Error computation
    #~ err = np.maximum(np.ones(targets.shape) - targets * np.matmul(features, w_t), np.zeros(targets.shape)).sum()
    #~ acc = 1-np.absolute(((np.matmul(features, w_t) > 0) - (0.5 * targets + 0.5 * np.ones((n_data_p,))))).sum() / n_data_p
    #~ print("Empirical Risk: " + str(err))
    #~ print("Accuracy: " + str(acc))
    yield 0, w_t  # This is how you yield a key, value pair


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # Simple averaging of the received weight vectors
    yield sum(values) / len(values)

