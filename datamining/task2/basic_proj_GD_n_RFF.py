import numpy as np

## transformation
def transform(X):
    # corresponds to input of RFF in all_transfs.py
    n_rand_feat = 6000
    ker_var = 5
    
    # set a seed (optional)
    np.random.seed(42)
    
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    # Random Fourier Features
    from math import pi
    bs = np.random.uniform(0, 2*pi,(1,n_rand_feat))
    ws = np.random.normal(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.standard_cauchy((n_feat, n_rand_feat))
    #ws = np.random.laplace(0, ker_var, (n_feat, n_rand_feat))
    F = np.cos(np.matmul(X, ws) + np.matmul(np.ones((n_data_p, 1)), bs))
    return F

## optimizer
def basic_proj_SGD(features, targets, lam = 100, n_iter_max = 100000, n_avg_wv = 100):
    n_data_p = features.shape[0]
    n_feat = features.shape[1]
    
    w_t = np.zeros(n_feat)
    
    # learning rate
    def eta_t(t):
        return 0.5 / np.power(t + 1, 0.3)
    
    # Convergence criteria
    tol = 10e-4
    convg_ct = 0
    max_convg_ct = n_data_p / 4
    indx = 0
    ct = 0
    
    # Take average over last 'n_avg_wv' weight vectors
    w_array = np.zeros((n_avg_wv, n_feat))
    indx_wv = 0
    
    # while not converged
    while True:
        if targets[indx] * np.dot(features[indx,:], w_t) < 1: # wrongly classified
            # Parameter update
            w_t = w_t + eta_t(indx) * targets[indx] * features[indx,:]
            # Projection
            sz = np.linalg.norm(w_t)
            if sz > lam:
                w_t = lam / sz * w_t
            # Storing the last few w_t
            w_array[indx_wv,:] = w_t
            indx_wv = (indx_wv + 1) % n_avg_wv
        indx = (indx + 1) % n_data_p
        #print(np.linalg.norm(w_t - w_array[(indx_wv + 1) % n_avg_wv,:]))
        if np.linalg.norm(w_t - w_array[1,:]) < tol:
            convg_ct = convg_ct + 1
        else:
            convg_ct = 0
        if convg_ct >= max_convg_ct:
            print("Converged!")
            break
        ct = ct + 1
        if ct > n_iter_max:
            print("Max number of iterations reached.")
            break
    
    return np.mean(w_array, axis=0)

## mapper
def mapper(key, value):
    # key: None
    # value: one line of input file
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])
    targets = trnsf[:,0]
    features = transform(trnsf[:,1:])
    
    w_t = basic_proj_SGD(features, targets)

    yield 0, w_t # This is how you yield a key, value pair

## reducer
def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # Simple averaging of the received weight vectors
    yield sum(values) / len(values) # sum(values[1]) / len(values[1])
