import numpy as np
from all_transfs import RFF, poly_ker, Nystroem, RFF_TEFM_G, data_augment, RMFM
from all_optimizers import basic_proj_SGD, ADAM


# python runner.py ./data/handout_train.txt ./data/handout_test.txt 

np.random.seed(42)

def transform(X):
    #return RFF_TEFM_G(X, n_rand_feat = 20000, ker_var = 5, q = 1, l = 2000)

    return RMFM(X)
    return RFF(X, n_rand_feat = 20000, ker_var = 5)


def mapper(key, value):
    # key: None
    # value: one line of input file
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])
    targets = trnsf[:,0]
    features = trnsf[:,1:]
    #[features, targets] = data_augment(features, targets, 2, 0, 0.1, 0.05)
    features = transform(features)
    n_data_p=features.shape[0]
    n_feat = features.shape[1]
    
    #w_t = basic_proj_SGD(features, targets, lam = 100, n_iter_max = 100000, n_avg_wv = 100)

    w_t = ADAM(features, targets, lam = 1000, n_iter_max = 4000, n_avg_wv = 200)

    
    # Error computation
    err = np.maximum(np.ones(targets.shape) - targets * np.matmul(features, w_t), np.zeros(targets.shape)).sum()
    acc = 1-np.absolute(((np.matmul(features, w_t) > 0) - (0.5 * targets + 0.5 * np.ones((n_data_p,))))).sum() / n_data_p
    print("Empirical Risk: " + str(err))
    print("Accuracy: " + str(acc))
    yield 0, w_t  # This is how you yield a key, value pair


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # Simple averaging of the received weight vectors
    yield sum(values) / len(values)
