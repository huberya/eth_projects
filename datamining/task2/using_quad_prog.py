import numpy as np
from random import randint
from all_optimizers import QP_cvxopt
from all_transfs import RFF, Nystroem

# The data is divided into 8 subsets, each is given to the
# mapper seperately.

def transform(X):
    # corresponds to input of RFF in all_transfs.py
    n_rand_feat = 6000
    ker_var = 5
    
    # set a seed (optional)
    np.random.seed(42)
    
    n_data_p = X.shape[0]
    n_feat = X.shape[1]
    # Random Fourier Features
    from math import pi
    bs = np.random.uniform(0, 2*pi,(1,n_rand_feat))
    ws = np.random.normal(0, ker_var, (n_feat, n_rand_feat))
    #ws = np.random.standard_cauchy((n_feat, n_rand_feat))
    #ws = np.random.laplace(0, ker_var, (n_feat, n_rand_feat))
    F = np.cos(np.matmul(X, ws) + np.matmul(np.ones((n_data_p, 1)), bs))
    return F

def mapper(key, value):
    # key: None
    # value: one line of input file
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])
    targets = trnsf[:,0]
    features = transform(trnsf[:,1:])
    
    w_t = QP_cvxopt(features, targets, C = 10000)
    
    # Error computation
    err = np.maximum(np.ones(targets.shape) - targets * np.matmul(features, w_t), np.zeros(targets.shape)).sum()
    acc = 1-np.absolute(((np.matmul(features, w_t) > 0) - (0.5 * targets + 0.5 * np.ones((n_data_p,))))).sum() / n_data_p
    print("Empirical Risk: " + str(err))
    print("Accuracy: " + str(acc))
    
    yield 0, w_t  # This is how you yield a key, value pair


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # Simple averaging of the received weight vectors
    yield sum(values) / len(values)
