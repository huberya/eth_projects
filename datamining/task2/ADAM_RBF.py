import numpy as np

def transform(X):
    # Make sure this function works for both 1D and 2D NumPy arrays.
    sigma=0.1
    #in this case X is a vector corresponding to the features of a single image
    X_out=0
    if(len(X.shape)==1):
        #im to stupid to get this correct analytically
        dimout=(X.shape[0]-1)+(X.shape[0]-2)+(X.shape[0]-3)+(X.shape[0]-4)+(X.shape[0]-5)+(X.shape[0]-6)+(X.shape[0]-7)+(X.shape[0]-8)+(X.shape[0]-9)+(X.shape[0]-10)
        #dimout=10*(X.shape[0]-1)
        #X_out=np.zeros(X.shape[0]*(X.shape[0]-1)/2)
        X_out=np.zeros(dimout)
        counter=0
        #for i in range(X.shape[0]):
        for i in range(10):
            for j in range(i+1, X.shape[0]):
                X_out[counter]=np.exp(-1*abs(X[i]-X[j])*abs(X[i]-X[j])/(2*sigma*sigma))
                counter+=1
    #in this case X is a matrix corresponding to the features of multiple images
    else:
        #loop over all images
        #im to stupid to get this analytically
        dimout=(X.shape[1]-1)+(X.shape[1]-2)+(X.shape[1]-3)+(X.shape[1]-4)+(X.shape[1]-5)+(X.shape[1]-6)+(X.shape[1]-7)+(X.shape[1]-8)+(X.shape[1]-9)+(X.shape[1]-10)
        #dimout=5*(X.shape[1]-1)
        #X_out=np.zeros((X.shape[0],X.shape[1]*(X.shape[1]-1)/2))
        X_out=np.zeros((X.shape[0],dimout))
        #print(dimout)
        for k in range(X.shape[0]):
            counter=0
            #for i in range(X.shape[1]):
            for i in range(10):
                for j in range(i+1, X.shape[1]):
                    X_out[k][counter]=np.exp(-1*abs(X[k][i]-X[k][j])*abs(X[k][i]-X[k][j])/(2*sigma*sigma))
                    counter+=1
    #print(X_out)
    return X_out


def mapper(key, value):
    # key: None
    # value: 2D array where each row corresponds to one "image"
    n_data_p = len(value)
    trnsf = np.array([np.fromstring(value[i], dtype=float, sep=' ') for i in range(n_data_p)])
    targets = trnsf[:,0]
    features = transform(trnsf[:,1:])
    n_feat = features.shape[1]
    w_t = np.zeros(n_feat)
    lam = 100
    tol = 10e-5
    eta = 0.001
    beta1=0.9
    beta2=0.999
    eps=0.00000001
    m_t=0
    v_t=0
    ct = 1
    n_iter = 10000

    weightsnormalization=True
    

    batchsize=20
    indxs=np.zeros(batchsize)


    # while not converged
    while True:
        indxs=np.random.randint(0,n_data_p,batchsize)
        tempsum=np.zeros(n_feat)
        for i in range(batchsize):
            if targets[indxs[i]] * np.dot(features[indxs[i]], w_t) < 1: # wrongly classified
                tempsum=tempsum+ targets[indxs[i]] * features[indxs[i]]
        grad=-1*tempsum/batchsize
        m_t=beta1*m_t+(1.0-beta1)*grad
        v_t=beta2*v_t+(1.0-beta2)*grad*grad
        mhat=m_t/(1.0-np.power(beta1,ct))
        #print(1.0-np.power(beta1,ct))
        vhat=v_t/(1.0-np.power(beta2,ct))
        w_t = w_t - eta * mhat / (np.sqrt(vhat)+eps)
        if weightsnormalization:
            w_t=min(1,1/np.sqrt(lam)/np.linalg.norm(w_t))*w_t
        ct = ct + 1
        if ct > n_iter:
            break
    
    yield 0, w_t  # This is how you yield a key, value pair


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # Simple averaging of the received weight vectors
    yield sum(values) / len(values)
