import numpy as np

def ADAM(features, targets, lam = 200, n_iter_max = 10000, n_avg_wv = 100, beta1=0.5,
				beta2=0.99, eta_const = 0.5, eta_rate = 0.5):

    def eta_t(t):
        return eta_const / np.power(t + 1, eta_rate)
    
    n_data_p = features.shape[0]
    n_feat = features.shape[1]  
    
    w_t = np.zeros(n_feat)
    
    # Parameters
    eps=0.00000001
    m_t=0
    v_t=0
    
    # Convergence criteria
    tol = 10e-4
    convg_ct = 0
    max_convg_ct = n_data_p / 4
    indx = 0
    ct = 1
    
    # Weight normalization
    weightsnormalization=True    
    batchsize=20
    indxs=np.zeros(batchsize)
    
    # Take average over last 'n_avg_wv' weight vectors    
    w_array = np.zeros((n_avg_wv, n_feat))
    indx_wv = 0
    
    # while not converged
    while True:
        eta = eta_t(ct)
        indxs=np.random.randint(0,n_data_p,batchsize)
        tempsum=np.zeros(n_feat)
        for i in range(batchsize):
            if targets[indxs[i]] * np.dot(features[indxs[i]], w_t) < 1: # wrongly classified
                tempsum=tempsum+ targets[indxs[i]] * features[indxs[i]]
        grad=-1*tempsum/batchsize
        m_t=beta1*m_t+(1.0-beta1)*grad
        v_t=beta2*v_t+(1.0-beta2)*grad*grad
        mhat=m_t/(1.0-np.power(beta1,ct))
        vhat=v_t/(1.0-np.power(beta2,ct))
        w_t = w_t - eta * mhat / (np.sqrt(vhat)+eps)
        if weightsnormalization:
            w_t=min(1,lam/np.linalg.norm(w_t))*w_t
        # Storing the last few w_t
        w_array[indx_wv,:] = w_t
        indx_wv = (indx_wv + 1) % n_avg_wv
        indx = (indx + 1) % n_data_p
        if np.linalg.norm(w_t - w_array[1,:]) < tol:
			convg_ct = convg_ct + 1
        else:
			convg_ct = 0
        if convg_ct >= max_convg_ct:
            print("Converged at: " + str(ct) + " iterations.")
            break
        ct = ct + 1
        if ct > n_iter_max:
            print("Max number of iterations reached.")
            break
    return np.mean(w_array, axis=0) 


def basic_proj_SGD(features, targets, lam = 100, n_iter_max = 100000, n_avg_wv = 100):
    n_data_p = features.shape[0]
    n_feat = features.shape[1]  
    
    w_t = np.zeros(n_feat)
    
    # learning rate
    def eta_t(t):
        return 0.5 / np.power(t + 1, 0.3)
    
    # Convergence criteria
    tol = 10e-4
    convg_ct = 0
    max_convg_ct = n_data_p / 4
    indx = 0
    ct = 0
    
    # Take average over last 'n_avg_wv' weight vectors    
    w_array = np.zeros((n_avg_wv, n_feat))
    indx_wv = 0
    
    # while not converged
    while True:
        if targets[indx] * np.dot(features[indx,:], w_t) < 1: # wrongly classified
			# Parameter update
            w_t = w_t + eta_t(indx) * targets[indx] * features[indx,:]
            # Projection
            sz = np.linalg.norm(w_t)
            if sz > lam:
				w_t = lam / sz * w_t
            # Storing the last few w_t
            w_array[indx_wv,:] = w_t
            indx_wv = (indx_wv + 1) % n_avg_wv
        indx = (indx + 1) % n_data_p
        if np.linalg.norm(w_t - w_array[1,:]) < tol:
			convg_ct = convg_ct + 1
        else:
			convg_ct = 0
        if convg_ct >= max_convg_ct:
            print("Converged at: " + str(ct) + " iterations.")
            break
        ct = ct + 1
        if ct > n_iter_max:
            print("Max number of iterations reached.")
            break
    
    return np.mean(w_array, axis=0) 
    
# Does not work for submissions
def QP_cvxopt(features, targets, C = 100):
    import cvxopt as co
    n_data_p = features.shape[0]
    n_feat = features.shape[1]    
    n_tot = n_feat + n_data_p
    qn = np.zeros(n_tot)
    Gn = np.zeros((n_data_p, n_tot))
    h = co.matrix(-np.ones(n_data_p))
    for k in range(n_data_p):
        qn[n_feat + k] = C
        Gn[k,:n_feat] = -targets[k] * features[k,:]
        Gn[k,n_feat + k] = -1
	
    P = co.spmatrix(1.0, range(n_feat), range(n_feat), (n_tot, n_tot))
    q = co.matrix(qn)
    G = co.matrix(Gn)
    sol = co.solvers.qp(P,q,G,h)
    w = np.array(sol['x'])[:n_feat][:,0]
    
    return w  # This is how you yield a key, value pair
  
    
    
