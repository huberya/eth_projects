
import numpy as np
import time
from all_optimizers import basic_proj_SGD, ADAM
from all_transfs import RFF, poly_ker, Nystroem, RFF_TEFM_G, RMFM

np.random.seed(42)

def transform(X):
    #return RFF_TEFM_G(X, n_rand_feat = 20000, ker_var = 5, q = 1, l = 2000)
    #return RFF(X, n_rand_feat = 2000, ker_var = 4.3)
    return RMFM(X)
    return RFF(X, n_rand_feat = 10000, ker_var = 4)

path = "./data/handout_"

n_max = 2000
n_data_p = n_max
test_data = np.loadtxt(path + "test.txt")
train_data = np.loadtxt(path + "train.txt")
train_targets = np.array(train_data[:n_max,0], dtype = int)

start = time.time()
trf_train_data = transform(train_data[:n_max,1:])
end = time.time()
print("Time for transformation: " + str(end - start))

test_targets = np.array(test_data[:n_max,0], dtype = int)
trf_test_data = transform(test_data[:n_max,1:])

opt_dicts_AD = [
        #["beta1", [0.5,0.7, 0.9, 0.99]], 
        #["beta2", [0.5,0.7,0.9, 0.99]],
        #["eta_rate", [0.2, 0.4, 0.5,0.7,0.9]],
        #["eta_const", [0.2, 0.4, 0.5,0.7,0.9]],
        #["lam", [10, 40, 100, 200, 300]],
        #["lam", [0.01, 0.1, 1, 10, 100, 1000, 5000]],
        ["lam", [1, 10, 100, 1000, 5000, 10000, 100000]],
        ]
n = len(opt_dicts_AD)

time_list = []

for k in range(n):
    for i in range(len(opt_dicts_AD[k][1])):
        print("\n" + opt_dicts_AD[k][0] + ": " + str(opt_dicts_AD[k][1][i]))
        start = time.time()
        w = ADAM(trf_train_data, train_targets, n_iter_max = 10000, **{opt_dicts_AD[k][0]: opt_dicts_AD[k][1][i]})
        end = time.time()
        t_el = end - start
        acc = 1.0 * (np.matmul(trf_train_data, w) * train_targets > 0).sum() / n_data_p
        print("Training Accuracy: " + str(acc))
        acc = 1.0 * (np.matmul(trf_test_data, w) * test_targets > 0).sum() / n_data_p
        print("Testing Accuracy: " + str(acc))
        print("Time for training: " + str(t_el))
        time_list += [(t_el, acc)]


print("Testing transformations...\n\n")
trafo_dicts = [
        #["ker_var", [2, 2.5, 3, 3.5,4, 4.2, 4.4, 4.5, 4.6]],
        #["ker_var", [0.7, 1, 2, 3]],
        ["ker_var", np.logspace(-4,3, 10)],
        #["ker_var", [2,5,10,100]],
        #["ker_var", [1,2,3,5,10,100]],
        ]
for k in range(n):
    for i in range(len(trafo_dicts[k][1])):
        print("\n" + trafo_dicts[k][0] + ": " + str(trafo_dicts[k][1][i]))
        start = time.time()
        trf_train_data = RFF(train_data[:n_max,1:], n_rand_feat = 10000, **{trafo_dicts[k][0]: trafo_dicts[k][1][i]})
        w = ADAM(trf_train_data, train_targets, n_iter_max = 10000, lam = 5000)
        end = time.time()
        t_el = end - start
        acc = 1.0 * (np.matmul(trf_train_data, w) * train_targets > 0).sum() / n_data_p
        print("Training Accuracy: " + str(acc))
        trf_test_data = RFF(test_data[:n_max,1:], n_rand_feat = 10000,**{trafo_dicts[k][0]: trafo_dicts[k][1][i]})
        acc = 1.0 * (np.matmul(trf_test_data, w) * test_targets > 0).sum() / n_data_p
        print("Testing Accuracy: " + str(acc))
        print("Time for training: " + str(t_el))
        time_list += [(t_el, acc)]

print(time_list)
