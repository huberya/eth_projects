
import numpy as np
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn import svm

path = "./data/handout_"

n_max = 10000
test_data = np.loadtxt(path + "test.txt")
train_data = np.loadtxt(path + "train.txt")
train_targets = np.array(train_data[:n_max,0], dtype = int)
train_data = train_data[:n_max,1:]
test_targets = np.array(test_data[:,0], dtype = int)
test_data = test_data[:,1:]


from sklearn.kernel_approximation import RBFSampler, Nystroem
from sklearn.linear_model import SGDClassifier
Ny_feature = Nystroem(random_state=42, n_components=4000)
X_features = Ny_feature.fit_transform(train_data)
clf = svm.SVC(kernel = "linear")   
#clf.fit(X_features, train_targets)
print(cross_val_score(clf, X_features, train_targets, cv = 5))

gamma_list = np.logspace(1,2,10)
gamma_list_ny = np.logspace(-5,5,11)
alpha_range = np.logspace(-5,3,9)

# [error, "Method", alpha, gamma]
best_params_and_error = [0.0, "RFF", 1, 1] 

for k in range(len(gamma_list)):
    rbf_feature = RBFSampler(gamma=gamma_list[k], random_state=42, n_components=6000)
    X_features = rbf_feature.fit_transform(train_data)
    print("RFF: Gamma = " + str(gamma_list[k]))
    for i in range(len(alpha_range)):        
        clf = SGDClassifier(max_iter=10, alpha = alpha_range[i])   
        c_list = cross_val_score(clf, X_features, train_targets, cv = 5)
        if c_list.mean() > best_params_and_error[0]:
            best_params_and_error[0] = c_list.mean()
            best_params_and_error[1] = "RFF"
            best_params_and_error[2] = alpha_range[i]
            best_params_and_error[3] = gamma_list[k]           
        print("Alpha = " + str(alpha_range[i]) + ": mean cv score: " + str(c_list.mean())
                    + " +- " + str(np.std(c_list)))
    
for k in range(len(gamma_list_ny)):
    Ny_feature = Nystroem(gamma=gamma_list_ny[k], random_state=42, n_components=500)
    X_features = Ny_feature.fit_transform(train_data)
    print("Nystroem: Gamma = " + str(gamma_list[k]))
    for i in range(len(alpha_range)):  
        clf = SGDClassifier(max_iter=10, alpha = 0.0001)   
        c_list = cross_val_score(clf, X_features, train_targets, cv = 5)
        if c_list.mean() > best_params_and_error[0]:
            best_params_and_error[0] = c_list.mean()
            best_params_and_error[1] = "NY"
            best_params_and_error[2] = alpha_range[i]
            best_params_and_error[3] = gamma_list[k]   
        print("Alpha = " + str(alpha_range[i]) + ": mean cv score: " + str(c_list.mean())
                    + " +- " + str(np.std(c_list)))

print(best_params_and_error)

for k in range(1):
    parameters = {'alpha': np.linspace(0.0000001,0.001,5)}
    clf = GridSearchCV(clf, parameters, cv = 5)

    grid_result = clf.fit(X_features, train_targets)
    print("...found optimal parameters.")
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
