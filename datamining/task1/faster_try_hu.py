import numpy as np

# Parameters
numHF = 8
N = 8192
p = 48611
np.random.seed(42)
ab = np.random.randint(1, N, (numHF,2))
b = 4
r = numHF / b
ar = np.random.randint(1, N, (r, b))
br = np.random.randint(1, N, (r, b))

# Hash function
def h(i):
    return np.remainder(np.remainder(ab[:,0] * i + ab[:,1], p), N)

def mapper(key, value):
    #doc_id = int(value[5:14])
    doc_id = value
    val = value.split(" ")[1:]
    n = len(val)
    val = np.array([int(val[i]) for i in range(n)], dtype = int)
    sigMatCol = 10000 * np.ones((numHF), dtype = int)
    for i in range(n):
        sigMatCol = np.minimum(sigMatCol, h(val[i]))
    sigMatHashed = np.remainder(np.sum(np.remainder(ar * sigMatCol.reshape((r,b)) + br, p), axis = 0),N)
    for k in range(b):
        yield sigMatHashed[k] + N * k , doc_id


def reducer(key, values):
    n = len(values)
    val_int_list = [int(values[i][5:14]) for i in range(n)]
    formatted = [values[i].split(" ")[1:] for i in range(n)]
    formatted = [np.array([int(formatted[i][j]) for j in range(len(formatted[i]))], dtype = int)
                     for i in range(n)]
    if n > 1:
        for i in range(n):
            for j in range(i+1, n):
                v_min = min(val_int_list[i], val_int_list[j])
                v_max = max(val_int_list[i], val_int_list[j])
                if np.intersect1d(formatted[i], formatted[j]).size >= 93:# Something is strange here :/
                    yield v_min, v_max  # this is how you yield a key, value pair






