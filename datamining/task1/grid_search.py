import numpy as np
import os
import subprocess
from datetime import datetime

mintime=180000000
besthf=0
bestb=0

for hf in range(1,100,1):
    for b in range(1,hf):
        if (hf%b==0):
            f=open("params.txt",'w')
            text=str(hf)+" "+str(b)
            f.write(text)
            f.close()
            startt=datetime.now()
            process=subprocess.Popen("python runner.py lsh_gs.py", shell=True, stdout=subprocess.PIPE)
            #print(process.communicate()[0])
            score=float((process.communicate()[0]).split("\n")[0])
            process.wait()
            endt=datetime.now()
            diff=endt-startt
            timing=diff.seconds*1000000+diff.microseconds
            if(score==1.0):
                #print("candidate")
                if(timing<6000000):
                    print(timing)
                    print(hf)
                    print(b)
                if(timing<mintime):
                    mintime=timing
                    besthf=hf
                    bestb=b

print("finalresult")
print(mintime)
print(besthf)
print(bestb)
