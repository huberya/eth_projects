# Code taken from:
# https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

# Idea how to later improve the starting values: 
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/

# Run with
# python runner.py ./data/handout_train.npy ./data/handout_test.npy KMplusplus.py

import numpy as np
import random

np.random.seed(42)

def cluster_points(X, mu):
    clusters  = {}
    #print(X.shape)
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                         for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters


def reevaluate_centers(mu, clusters):
    keys = sorted(clusters.keys())
    #print(clusters[1])
    #print(clusters[1][1].shape)
    newmu = list()
    #print(newmu.shape)
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
        # we could try to take the median instead => minimize the L1 loss instead of L2 
    return newmu


def has_converged(mu, oldmu):
    #print(oldmu)
    #print("comparison")
    #return set(mu)==set(oldmu)
    return set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu])

# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)
  
def initialize_centers_plus_plus(X, K):
    # Initialize to K random centers using Kmeans++   
    n = len(X)
    mu = random.sample(X, 1)
    dists = np.array([np.linalg.norm(x-mu)**2 for x in X])
    while len(mu) < K:
        # next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0]]
        next_mu = X[np.where((dists/sum(dists)).cumsum() < random.random())[0][-1]]
        # next_mu = X[np.argmax(dists)]
        mu.append(next_mu)
        for i in range(n):
            dists[i] = min(dists[i], np.linalg.norm(x-next_mu)**2)
    return mu
            
def find_centers(X, K = 200):
	
    mu = initialize_centers_plus_plus(X, K)
    oldmu = mu
    print(len(mu))
    # Same as before
    print(type(mu))
    maxiter=20
    for i in range(maxiter):
            print("loop")
            if not (i % 4):
                print("Iteration: " + str(i))
            oldmu = mu
            # Assign all points in X to clusters
            clusters = cluster_points(X, mu)
            # Reevaluate centers
            mu = reevaluate_centers(oldmu, clusters)
            if(has_converged(mu, oldmu)):
			    print("converged")
			    return mu
            #print(type(mu))
            #print(mu)
    print("maxiter reached")
    return mu # , clusters


def mapper(key, value):
    # key: None
    # value: one line of input file
    
    print(value.shape)
    # cluster the values
    #mu = initialize_centers_plus_plus(value, 300)
    mu = find_centers(X = value, K = 100)

    yield 1, mu  # this is how you yield a key, value pair
    


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    print("Reducing...")
    # To find a one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    muAll = find_centers(X = values, K = 200)
    yield muAll
    



