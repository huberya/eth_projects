from current_best import find_centers_multiple
import numpy as np

# Simple model problem that requires two iterations
data = np.array([7.0, 3.0, 0.0, 12.0, 18.0])
n = len(data)
X = np.reshape(data, (n,1))

mu = find_centers_multiple(X, 2, n_runs = 1)
print(mu)
