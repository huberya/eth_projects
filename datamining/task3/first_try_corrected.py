# Code taken from:
# https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

# Idea how to later improve the starting values: 
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/

import numpy as np
import random

def cluster_points(X, mu):
    clusters  = {}
    #print(X.shape)
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                         for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters


def reevaluate_centers(mu, clusters):
    keys = sorted(clusters.keys())
    #print(clusters[1])
    #print(clusters[1][1].shape)
    newmu = list()
    #print(newmu.shape)
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
        # we could try to take the median instead => minimize the L1 loss instead of L2 
    return newmu


def has_converged(mu, oldmu):
    #print(oldmu)
    #print("comparison")
    #return set(mu)==set(oldmu)
    return set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu])

            
def find_centers(X, K = 200):
    # Initialize to K random centers
    oldmu = random.sample(X, K)
    #print(oldmu[1].shape)
    mu = random.sample(X, K)
    #print(type(oldmu))
    #print(type(mu))
    maxiter=10
    for i in range(maxiter):
            #print("loop")
            print("new iter")
            oldmu = mu
            # Assign all points in X to clusters
            clusters = cluster_points(X, mu)
            # Reevaluate centers
            mu = reevaluate_centers(oldmu, clusters)
            if(has_converged(mu, oldmu)):
			    print("converged")
			    return mu
            #print(type(mu))
            #print(mu)
    print("maxiter reached")
    return mu # , clusters


# # Small example of the function find_centers
# # generate some data 
#def init_board(N):
#    X = np.array([(random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1)) for i in range(N)])
#    return X
#
#X = init_board(300)
#
#print(X)
#
# Test run
#mu = find_centers(X, 10)
#print(mu)



def mapper(key, value):
    # key: None
    # value: one line of input file
    
    # cluster the values
    mu = find_centers(X = value, K = 100)

    yield 1, mu  # this is how you yield a key, value pair
    


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # To find a one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    muAll = find_centers(X = values, K = 200)
    yield muAll
    



