# Idea taken from:
# https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

# Run with
# python runner.py ./data/handout_train.npy ./data/handout_test.npy KMplusplus_optimized.py

# Replacing lists with numpy arrays
# mu: K * #features 
# X: #samples * #features
# clusters: #samples

import numpy as np
import random

np.random.seed(42)

# Assign samples to nearest cluster and return the distance for each sample
def cluster_points_and_dist(X, mu):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1)
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

# Restart = True if no data point gets assigned to a certain cluster?
def reevaluate_centers(mu, X, clusters):
    K = mu.shape[0]
    for k in range(K):
        indxs = clusters == k
        n_ass = np.sum(indxs)
        if n_ass > 0:
            mu[k,:] = np.sum(X[indxs], axis = 0) / n_ass
        else:
            return [mu, True]
    return [mu, False]


# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)
def initialize_centers_plus_plus(X, K, p = 2):
    # Initialize to K random centers using Kmeans++   
    n_samples = X.shape[0]
    n_features = X.shape[1]
    mu = np.zeros((K, n_features))
    mu[0,:] = X[np.random.randint(n_samples),:]
    # mu[0,:] = X[0,:]
    dists = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[0,:]), axis = 1)**p
    for k in range(K-1):
        next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0],:]
        # Alternative possibility:
        #next_mu = X[np.argmax(dists),:]
        mu[k+1,:] = next_mu
        dists = np.minimum(dists, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k+1,:]), axis = 1)**p)
    return mu
    

# Do Kmeans++
def find_centers_and_min_dists(X, K = 200, maxiter = 10):	
    n_samples = X.shape[0]
    tol = 1e-8
    min_dists = np.ones(n_samples)
    clusters = np.ones(n_samples, dtype = int)
    restart = True
    while restart:
        mu = initialize_centers_plus_plus(X, K)
        oldclusters = np.copy(clusters)
        restart = False
        print("Iterating...")
        for i in range(maxiter):
            print("Iteration " + str(i))
            # Assign all points in X to clusters
            [clusters, min_dists] = cluster_points_and_dist(X, mu)
            # Reevaluate centers
            [mu, restart] = reevaluate_centers(mu, X, clusters)
            if restart:
                print("Restarting...")
                break
            print("Loss: " + str(np.sum(min_dists)))
            # Check if the cluster assignments have changed
            if not np.sum(clusters != oldclusters) > 0:
                print("converged")
                return [mu, min_dists]
            oldclusters = clusters
            if i == maxiter - 1:
                print("maxiter reached")
    return [mu, min_dists]
    
def find_centers_multiple(X, K = 200, n_runs = 10, maxiter = 10):	
    [mu, dists] = find_centers_and_min_dists(X, K, maxiter = maxiter)
    cost = np.sum(dists)
    for n in range(n_runs-1):
        [mu_temp, dists_temp] = find_centers_and_min_dists(X, K, maxiter = maxiter)
        cost_temp = np.sum(dists_temp)
        if cost_temp < cost:
            print("Found better clusters.")
            [mu, dists] = [mu_temp, dists_temp]
            cost = cost_temp
    return mu


def mapper(key, value):
    # key: None
    # value: one line of input file
    print("hoi")
    # cluster the values
    #mu = initialize_centers_plus_plus(value, 800)
    mu = find_centers_multiple(value, K = 500, n_runs = 1, maxiter = 4)

    yield 1, mu  # this is how you yield a key, value pair
    


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    print("Reducing...")
    # To find a one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    muAll = find_centers_multiple(X = values, K = 200, n_runs = 1, maxiter = 10)
    yield muAll
    


