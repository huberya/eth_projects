# Mapper: pick good inital values and cluster
# Reducer: cluster (kmeans) the centers / mu's which are the output of the mapper

# Result: ca. 135 (noRand = 150)
# Result:  (noRand = 1)
# Time: very slow (slowest)
# Maybe we should take a different method or subsample

import numpy as np
import random

from kmeans import *


def mapper(key, value):
    # key: None
    # value: one line of input file
    
    # print(value.shape) # (3000, 250) # maybe we should subsample
    
    # cluster the values
    mu = find_centers(X = value, K = 200, noRand = 1)

    yield 1, mu  
    

def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # To find one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    muAll = find_centers(X = values, K = 200)
    
    yield muAll
    

