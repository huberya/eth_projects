# Code taken from:
# https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/

import numpy as np
import random

def dist_from_centers(mu, X):
    cent = mu
    D2 = np.array([min([np.linalg.norm(x-c)**2 for c in cent]) for x in X])
    return D2
 
def choose_next_center(D2, X):
    probs = D2/sum(D2)
    cumprobs = np.cumsum(probs)
    r = random.random()
    ind = np.where(cumprobs >= r)[0][0]
    return(X[ind])
 
# X data
# K number of centers
# noRand number of centers which are randomly initialized
def init_centers(X, K, noRand):
    mu = random.sample(X, noRand)
    while len(mu) < K:
        print("find " + str(len(mu) + 1) + "th center")
        D2 = dist_from_centers(mu, X)
        mu.append(choose_next_center(D2, X))
    return mu
        
        

def cluster_points(X, mu):
    clusters  = {}
    #print(X.shape)
    for x in X:
        bestmukey = min([(i[0], np.linalg.norm(x-mu[i[0]])) \
                         for i in enumerate(mu)], key=lambda t:t[1])[0]
        try:
            clusters[bestmukey].append(x)
        except KeyError:
            clusters[bestmukey] = [x]
    return clusters


def reevaluate_centers(mu, clusters):
    keys = sorted(clusters.keys())
    #print(clusters[1])
    #print(clusters[1][1].shape)
    newmu = list()
    #print(newmu.shape)
    for k in keys:
        newmu.append(np.mean(clusters[k], axis = 0))
        # we could try to take the median instead => minimize the L1 loss instead of L2 
    return newmu


def has_converged(mu, oldmu):
    return set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu])

# X data
# K number of centers
# noRand number of centers which are randomly initialized
def find_centers(X, K = 200, noRand = 1):
    # Initialize to K random centers
    oldmu = random.sample(X, K)
    mu = init_centers(X, K, noRand)
    #print(type(oldmu))
    #print(type(mu))
    maxiter=10
    for i in range(maxiter):
            print("new iter")
            oldmu = mu
            # Assign all points in X to clusters
            clusters = cluster_points(X, mu)
            # Reevaluate centers
            mu = reevaluate_centers(oldmu, clusters)
            if(has_converged(mu, oldmu)):
			    print("converged")
			    return mu

    print("maxiter reached")
    return mu 


# X data
# K number of centers
# noRand number of centers which are randomly initialized
def find_centers_given_init(X, mu_init, K = 200):
    # Initialize to K random centers
    oldmu = random.sample(X, K)
    mu = mu_init
    #print(type(oldmu))
    #print(type(mu))
    maxiter=10
    for i in range(maxiter):
            print("new iter")
            oldmu = mu
            # Assign all points in X to clusters
            clusters = cluster_points(X, mu)
            # Reevaluate centers
            mu = reevaluate_centers(oldmu, clusters)
            if(has_converged(mu, oldmu)):
			    print("converged")
			    return mu

    print("maxiter reached")
    return mu 