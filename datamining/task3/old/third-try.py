# Mapper: only pick good starting values
# Reducer: cluster (kmeans) those starting values

# Result: 27.8
# Time: very slow (faster than first and second try)
# Maybe we should take a different method or subsample

import numpy as np
import random

from kmeans import *

def mapper(key, value):
    # key: None
    # value: one line of input file
    
    # print(value.shape) # (3000, 250) # maybe we should subsample
    
    # pick initival values from a subsample, say, 1000
    mu_init = init_centers(X = random.sample(value, 1000), K = 200, noRand = 1)
    
    # Cluster
    mu = find_centers_given_init(X = value, mu_init = mu_init, K = 200)

    yield 1, mu  
    


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    # To find one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    muAll = find_centers(X = values, K = 200)
    
    yield muAll
    