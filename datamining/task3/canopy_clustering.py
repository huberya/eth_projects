import numpy as np
#X is the data, T1 > T2 are the thresholds (for distances)
#returns a (ncanopies, dim+1) np array
def compute_canopy_cluster(X, T1, T2):
    terminated=False
    k=X.shape[0]
    dim=X.shape[1]
    canopies=np.zeros((1,dim+1))
    while not terminated:
        k=X.shape[0]
        dim=X.shape[1]
        canopy=X[np.random.randint(0, k),:]
        [X, weight, terminated]=update_canopy(X, canopy, T1, T2)
        canopyw=np.zeros((1,dim+1))
        canopyw[0,0]=weight
        canopyw[0,1:]=canopy
        canopies=np.concatenate((canopies,canopyw),axis=0)
    return canopies[1:,:]
    
def update_canopy(X, canopy, T1, T2):
    
    terminated=True
    k=X.shape[0]
    dim=X.shape[1]
    canopy=np.reshape(canopy,(1,dim))
    Xnew=np.zeros((1,dim))
    weight=1
    for i in range(k): #somehow exclude canopy from X (would be more clean but probably little impact)
        #precompute distance for efficiency
        dist=np.linalg.norm(canopy-X[i,:], axis=1)
        if dist < T2:
            weight+=1
        elif dist<T1:
            weight+=1
            newX=np.reshape(X[i,:], (1,dim))
            Xnew=np.concatenate((Xnew,newX), axis=0)
            terminated=False
        else:
            newX=np.reshape(X[i,:], (1,dim))
            Xnew=np.concatenate((Xnew,newX), axis=0)
            terminated=False
    return Xnew[1:,:], weight, terminated
