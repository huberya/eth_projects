import numpy as np
import math
import random

# Run with
# python runner.py ./data/handout_train.npy ./data/handout_test.npy coresets.py


#implements Algorithm 2 from paper 1703.06476
#X is Nxdim values
#k is ?
#B is ?xdim matrix of centroids (x,initialized with KMeans++ i think)
#m is number of coreset points
#for efficiency returns the indexs of the points of the coreset and their corresponding weight (NOT THE POINT ITSELF)
#if points are already weighted dist is scaled by this
def construct_coreset(X, k, B, m, weights, norm_ord=2):
    alpha=400*(math.log(k)+2)
    #clusters saves the index of the corresponding cluster foreach data
    #mindist is the distance foreach point to its assigned cluster
    ndata=X.shape[0]
    s=np.zeros(ndata)
    p=np.zeros(ndata)
    [clusters, min_dist]=cluster_points_and_dist_weighted(X, B, weights, norm_ord=norm_ord)
    c_phi=np.mean(min_dist)#somehting like this probably choosen cluster (maybe in other function)
    for i in range(B.shape[0]):
        indxs = clusters == i
        #foreach point assigned to cluster B_i
        s[indxs]=alpha*min_dist[indxs]/c_phi+2*alpha*np.mean(min_dist[indxs])/(np.sum(indxs)*c_phi)+4*ndata/np.sum(indxs) #i dont think this works
    means=np.sum(s)
    for i in range(ndata):
        p[i]=s[i]/means
    indx = np.random.choice(ndata, size=m, replace=True, p=p)
    weights=np.zeros(m)
    for i in range(m):
        weights[i] = 1/(m*p[indx[i]])
    return [indx, weights]

def combine_two_coresets_return_one(valout1, valout2, K, m, kk):
    valueC = np.concatenate((valout1[:, 1:], valout2[:, 1:]), axis = 0)
    print("clustering for initial")
    weights = np.concatenate((valout1[:, 0], valout2[:, 0]), axis = 0)
    #only need low accuracy
    [mu, weights_new] = find_centers_multiple(valueC, weights, K = K, n_runs = 1, tol=1e-8)
    mu = initialize_centers_plus_plus(valueC, K)
    print("Using K = " + str(K) + " and m = " + str(m) + " for Coresets in Mapper")
    #mu = initialize_centers_plus_plus(value, K)
    [indx, weight] = construct_coreset(valueC, kk, mu, m, weights)
    valoutC = np.zeros((m, valueC.shape[1] + 1))
    for i in range(m):
        valoutC[i,0] = weight[i]
        valoutC[i,1:] = valueC[indx[i],:]

    if False:
        # normalize weights
        valoutC[:, 0] = valoutC[:, 0] / sum(valoutC[:, 0])

    return valoutC
    
#returns the closest cluster index and the distance to it for each sample 
def cluster_points_and_dist_weighted(X, mu, weights, norm_ord = 2):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.multiply(weights, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord))
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

def cluster_points_and_dist(X, mu, norm_ord = 1):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]


# Restart = True if no data point gets assigned to a certain cluster
#now also returns n_ass since this should be the weight a cluster gets in the reduction step
def reevaluate_centers(mu, X, clusters, weights):
    K = mu.shape[0]
    ass_weight=np.ones(K)
    newmu=np.zeros((K,X.shape[1]))
    for k in range(K):
        indxs = clusters == k
        
        n_ass = np.sum(indxs)
        #print(n_ass)
        n_weighted=np.sum(weights[indxs])
        ass_weight[k]=n_ass
        if n_ass > 0:
            #newmu[k,:]=np.matmul(np.transpose(weights[indxs]),X[indxs])/n_weighted
            newmu[k,:]=np.mean(X[indxs], axis=0)
            #mu[k,:] = np.sum(weights[indxs]*X[indxs], axis = 0) / n_weighted
        else:
            return [newmu, True, ass_weight]
    #print(np.sum(ass_weight))
    return [newmu, False, ass_weight]


def has_converged(mu, oldmu, tol = 1e-8):
    return np.linalg.norm(mu-oldmu) < tol
    #return False

# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)
  
def initialize_centers_plus_plus(X, K, p = 3):
    # Initialize to K random centers using Kmeans++   
    n_samples = X.shape[0]
    n_features = X.shape[1]
    mu = np.zeros((K, n_features))
    mu[0,:] = X[np.random.randint(n_samples),:]
    dists = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[0,:]), axis = 1)**p
    for k in range(K-1):
        next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0],:]
        # Alternative possibility:
        #next_mu = X[np.argmax(dists),:]
        mu[k+1,:] = next_mu
        dists = np.minimum(dists, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k+1,:]), axis = 1)**p)
    return mu
            
    
def find_centers_and_min_dists(X, weights, K = 200, tol = 1e-8):    
    n_samples = X.shape[0]
    if weights.shape[0]!=n_samples:
        print("missmatch in dimension between X and weights, supposed to have same first dimensiont")
        return
    min_dists = np.ones(n_samples)
    clusters = np.ones(n_samples, dtype = int)
    restart = True
    maxiter = 11
    while restart:
        mu = initialize_centers_plus_plus(X, K)
        oldclusters = np.copy(clusters)
        restart = False
        print("Iterating...")
        for i in range(maxiter):
            # print("Iteration " + str(i))
            # Assign all points in X to clusters
            [clusters, min_dists] = cluster_points_and_dist(X, mu)
            # Reevaluate centers
            [mu, restart, weights_new] = reevaluate_centers(mu, X, clusters, weights)
            if restart:
                print("Restarting...")
                break
            # Check if the cluster assignments have changed
            if not np.sum(clusters != oldclusters) > 0:
                print("converged")
                return [mu, min_dists, weights_new]
            oldclusters = clusters
            if i == maxiter - 1:
                print("maxiter reached")
    return [mu, min_dists, weights_new]

def find_centers_multiple(X, weights, K = 200, n_runs = 10, tol=1e-8):    
    [mu, dists, weights_new] = find_centers_and_min_dists(X, weights, K, tol=tol)
    #for i in range(1000):
    #  print(dists[i])
    indxs=dists==0.0
    print(np.sum(indxs))
    #print(np.min(dists))
    cost = np.sum(dists)
    for n in range(n_runs-1):
        [mu_temp, dists_temp, weights_new_temp] = find_centers_and_min_dists(X, weights, K, tol=tol)
        cost_temp = np.sum(dists_temp)
        if cost_temp < cost:
            print("Found better clusters.")
            [mu, dists, weights_new] = [mu_temp, dists_temp, weights_new_temp]
            cost = cost_temp
    #weights_new should be the number of points assigned to the cluster s.t a cluster with a lot of points
    #becomes more weight in the reduce step
    #try to pass it as key
    return [mu, weights_new]




def mapper(key, value):
    # key: None
    # value: one line of input file
    K = 160 # number of clusters
    kk = 160 # we do not really need this one... because we change alpha directly in the function construct_coreset
   
    value1 = value #[0:1500, :]
    # value2 = value[1500:3000, :]
    
    m = 2000
    
    # # coreset 1 (pick 1000 samples)
    # print("clustering for initial")
    weights = np.ones(value1.shape[0])
    # [mu, weights_new] = find_centers_multiple(value1, weights, K = K, n_runs = 1, tol=1e-3)
    # mu = initialize_centers_plus_plus(value1, K)
    print("Using K = " + str(K) + " and m = " + str(m) + " for Coresets in Mapper")
    mu = initialize_centers_plus_plus(value, K)
    [indx, weight] = construct_coreset(value1, kk, mu, m, weights)
    valout1 = np.zeros((m, value1.shape[1]+1))
    for i in range(m):
        valout1[i,0] = weight[i]
        valout1[i,1:] = value1[indx[i],:]
    valout = valout1

    # # coreset 2 (pick 1000 samples)
    # print("clustering for initial")
    # weights = np.ones(value2.shape[0])
    # [mu, weights_new] = find_centers_multiple(value2, weights, K = K, n_runs = 1, tol=1e-8)
    # mu = initialize_centers_plus_plus(value2, K)
    # print("Using K = " + str(K) + " and m = " + str(m) + " for Coresets in Mapper")
    # #mu = initialize_centers_plus_plus(value, K)
    # [indx, weight] = construct_coreset(value2, kk, mu, m, weights)
    # valout2 = np.zeros((m, value2.shape[1]+1))
    # for i in range(m):
    #     valout2[i,0] = weight[i]
    #     valout2[i,1:] = value2[indx[i],:]
    #
    # if False:
    #     # normailze weights
    #     valout1[:, 0] = valout1[:, 0] / sum(valout1[:, 0])
    #     valout2[:, 0] = valout2[:, 0] / sum(valout2[:, 0])
    #
    # # combine the two coresets (pick 1500 samples)
    # m = 1500
    # valout = combine_two_coresets_return_one(valout1 = valout1, valout2 = valout2, K = K, m = m, kk = kk)


    the_group = np.ones((valout.shape[0], 1)) * np.random.randint(1000, size = 1)
    valout = np.hstack((the_group, valout))

    yield 1, valout  # this is how you yield a key, value pair

def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    print("Reducing...")
    #print("initializing for coresets")
    
    K = 160 # number of clusters
    kk = 160 # we do not really need this one... because we change alpha directly in the function construct_coreset
    m = 2000 # number of samples in coresets # 1500
    
    groups = np.unique(values[:, 0])
    no_groups = len(groups)
    
    while len(np.unique(values[:, 0])) > 1:
        take_two = no_groups[0:2]
        # take two coresets and make a new one
        valout_take_two = combine_two_coresets_return_one(valout1 = values[values[:, 0] == take_two[0], 1:], valout2 = values[values[:, 0] == take_two[1], 1:], K = K, m = m, kk = kk)
        new_group = np.random.randint(1000, size = 1)
        no_groups = np.concatenate((no_groups[2:], new_group), axis = 0)
        print(no_groups.shape)
        
        values = values[values[:, 0] == take_two[0], :]
        values[values[:, 0] == take_two[1], :] = np.concatenate((np.ones((sum(values[:, 0] == take_two[1]), 1)) * new_group, valout_take_two), axis = 1)

    print("clustering")
    values = values[:, 1:]
    
    K = 200
    [muAll, weights] = find_centers_multiple(X = values[:,1:], weights=values[:,0], K = 200, n_runs = 1, tol = 1e-3)
    print("done")
    yield muAll
