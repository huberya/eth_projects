# Code taken from:
# https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

# Idea how to later improve the starting values: 
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/

# Run with
# python runner.py ./data/handout_train.npy ./data/handout_test.npy KMplusplus_optimized.py

# Replacing lists with numpy arrays
# mu: K * #features 
# X: #samples * #features
# clusters: #samples

import numpy as np
import random
#from canopy_clustering import compute_canopy_cluster

np.random.seed(42)

#returns the closest cluster index and the distance to it for each sample 
def cluster_points_and_dist(X, mu, norm_ord = 4):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

# Restart = True if no data point gets assigned to a certain cluster
#now also returns n_ass since this should be the weight a cluster gets in the reduction step
def reevaluate_centers(mu, X, clusters, weights):
    K = mu.shape[0]
    ass_weight=np.ones(mu.shape[0])
    for k in range(K):
        indxs = clusters == k
        
        n_ass = np.sum(indxs)
        n_weighted=np.sum(weights[indxs])
        ass_weight[k]=n_ass
        if n_ass > 0:
            mu[k,:]=np.matmul(np.transpose(weights[indxs]),X[indxs])/n_weighted
            #mu[k,:] = np.sum(weights[indxs]*X[indxs], axis = 0) / n_weighted
        else:
            return [mu, True, ass_weight]
    return [mu, False, ass_weight]


def has_converged(mu, oldmu, tol = 1e-8):
    return np.linalg.norm(mu-oldmu) < tol

# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)
  
def initialize_centers_plus_plus(X, K, p = 3):
    # Initialize to K random centers using Kmeans++   
    n_samples = X.shape[0]
    n_features = X.shape[1]
    mu = np.zeros((K, n_features))
    mu[0,:] = X[np.random.randint(n_samples),:]
    dists = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[0,:]), axis = 1)**p
    for k in range(K-1):
        # next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0],:]
        # Alternative possibility:
        next_mu = X[np.argmax(dists),:]
        mu[k+1,:] = next_mu
        dists = np.minimum(dists, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k+1,:]), axis = 1)**p)
    return mu
            
    
def find_centers_and_min_dists(X, weights, K = 200):    
    n_samples = X.shape[0]
    #print(weights)
    if weights.shape[0]!=n_samples:
        print("missmatch in dimension between X and weights, supposed to have same first dimensiont")
        return
    min_dists = np.ones(n_samples)
    restart = True
    maxiter=20
    while restart:
        mu = initialize_centers_plus_plus(X, K)
        oldmu = mu
        restart = False
        print("Iterating...")
        for i in range(maxiter):
            oldmu = mu
            # Assign all points in X to clusters
            [clusters, min_dists] = cluster_points_and_dist(X, mu)
            # Reevaluate centers
            [mu, restart, weights_new] = reevaluate_centers(oldmu, X, clusters, weights)
            if restart:
                print("Restarting...")
                break
            if has_converged(mu, oldmu):
                print("converged")
                return [mu, min_dists, weights_new]
        print("maxiter reached")
    return [mu, min_dists, weights_new]
    
def find_centers_multiple(X, weights, K = 200, n_runs = 10):    
    [mu, dists, weights_new] = find_centers_and_min_dists(X, weights, K)
    cost = np.sum(dists)
    for n in range(n_runs-1):
        [mu_temp, dists_temp, weights_new_temp] = find_centers_and_min_dists(X, weights, K)
        cost_temp = np.sum(dists_temp)
        if cost_temp < cost:
            print("Found better clusters.")
            [mu, dists, weights_new] = [mu_temp, dists_temp, weights_new_temp]
            cost = cost_temp
    #weights_new should be the number of points assigned to the cluster s.t a cluster with a lot of points
    #becomes more weight in the reduce step
    #try to pass it as key
    return [mu, weights_new]


def mapper(key, value):
    # key: None
    # value: one line of input file

    # cluster the values
    weights=np.ones(value.shape[0])
    

    #simply pass everything to reducer
    weights=np.ones((value.shape[0],1))
    valout=np.concatenate((weights,value), axis=1)
    yield 1, valout  # this is how you yield a key, value pair
    


def reducer(key, values):
    # key: key from mapper used to aggregate
    # values: list of all value for that key
    # Note that we do *not* output a (key, value) pair here.
    
    print("Reducing...")
    print(values.shape)
    #causeerror
    K=200
    assert values.shape[0]>=K
    # To find a one solution (i.e. one set of centers), one could cluster all 
    # the center again and find the (centers or) clusters of the centers.
    # I'm not sure if that is a good idea but it should work in principal to 
    # aggregate the different solutions.
    #print(values[:,1])
    [muAll, weights] = find_centers_multiple(X = values[:,1:], weights=values[:,0], K=K, n_runs = 1)
    yield muAll
    



