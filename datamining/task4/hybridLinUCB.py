import numpy as np
import math
import sys
from scipy import linalg

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt hybridLinUCB.py

#this code is following the algorithm presented in the lecture (Li et al WWW' 10)
#with coding ideas taken from ucb_basic

class hybridLinUCB:
	def __init__(self):
		self.d_= 6 #dimensions of user features?
		self.k_= 6#dimsension of ?articles? maybe
		#the 2 needed storages
		A0 = np.identity(self.k_, dtype=float)
		b0 = np.zeros((1,self.k_), dtype=float)
		
		#for efficiency allocate temporary matrices one in beginning
		A_temp
	
	#this corresponds to line 4-15 from algorithm2
	def recommend(self, time, user_features, choices):
		betahat=np.matmul(np.linalg.inv(A0),b0)
		
