import numpy as np

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt linUCB_DLM_kmeans.py

# This is the LinUCB with Disjoint Linear Models from: 
# http://rob.schapire.net/papers/www10.pdf
# It regards all users as equal.
# Does not perform any good :(

# Hyperparameters (global constants)
alpha = 100

# Gloobal variables
# Constants:
avali_articles = 0
# Updated vars:
A_a = 0
b_a = 0
curr_chosen_artic = 0

# Extend the features
# Original Features
def feature_map_id(features):
    return np.array(features)
# Quadratic and Linear features
def feature_map_quad(features):
    nump_feats = np.array(features)
    n = len(features)
    mapped_feats = np.zeros((n*(n+1),))
    for k in range(n):
        for i in range(n):
            mapped_feats[k + n * i] = nump_feats[k] * nump_feats[i]
    for k in range(n):
        mapped_feats[n*n+k] = nump_feats[k]
    return mapped_feats

# Add const feature
def feature_map_add1(features):
    return np.array([1] + features)

####
#returns the closest cluster index and the distance to it for each sample
def cluster_points_and_dist_weighted(X, mu, weights, norm_ord = 2):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.multiply(weights, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord))
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

def cluster_points_and_dist(X, mu, norm_ord = 1):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

def cluster_points_and_dist_all(X, mu, norm_ord = 1):
    n_samples = len(X) # X.shape[0]
    # n_features = X.shape[1]
    K = mu.shape[0]
    cluster_dist = np.zeros((n_samples, K))
    for k in range(K):
        cluster_dist[:, k] = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
    return cluster_dist

# Restart = True if no data point gets assigned to a certain cluster
#now also returns n_ass since this should be the weight a cluster gets in the reduction step
def reevaluate_centers(mu, X, clusters, weights):
    K = mu.shape[0]
    ass_weight=np.ones(K)
    newmu=np.zeros((K,X.shape[1]))
    for k in range(K):
        indxs = clusters == k
        
        n_ass = np.sum(indxs)
        #print(n_ass)
        n_weighted=np.sum(weights[indxs])
        ass_weight[k]=n_ass
        if n_ass > 0:
            #newmu[k,:]=np.matmul(np.transpose(weights[indxs]),X[indxs])/n_weighted
            newmu[k,:]=np.mean(X[indxs], axis=0)
        #mu[k,:] = np.sum(weights[indxs]*X[indxs], axis = 0) / n_weighted
        else:
            return [newmu, True, ass_weight]
    #print(np.sum(ass_weight))
    return [newmu, False, ass_weight]


def has_converged(mu, oldmu, tol = 1e-8):
    return np.linalg.norm(mu-oldmu) < tol
    #return False

# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)

def initialize_centers_plus_plus(X, K, p = 3):
    # Initialize to K random centers using Kmeans++
    n_samples = X.shape[0]
    n_features = X.shape[1]
    mu = np.zeros((K, n_features))
    mu[0,:] = X[np.random.randint(n_samples),:]
    dists = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[0,:]), axis = 1)**p
    for k in range(K-1):
        next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0],:]
        # Alternative possibility:
        #next_mu = X[np.argmax(dists),:]
        mu[k+1,:] = next_mu
        dists = np.minimum(dists, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k+1,:]), axis = 1)**p)
    return mu


def find_centers_and_min_dists(X, weights, K = 200, tol = 1e-8):
    n_samples = X.shape[0]
    if weights.shape[0]!=n_samples:
        print("missmatch in dimension between X and weights, supposed to have same first dimensiont")
        return
    min_dists = np.ones(n_samples)
    clusters = np.ones(n_samples, dtype = int)
    restart = True
    maxiter = 11
    while restart:
        mu = initialize_centers_plus_plus(X, K)
        oldclusters = np.copy(clusters)
        restart = False
        print("Iterating...")
        for i in range(maxiter):
            # print("Iteration " + str(i))
            # Assign all points in X to clusters
            [clusters, min_dists] = cluster_points_and_dist(X, mu)
            # Reevaluate centers
            [mu, restart, weights_new] = reevaluate_centers(mu, X, clusters, weights)
            if restart:
                print("Restarting...")
                break
            # Check if the cluster assignments have changed
            if not np.sum(clusters != oldclusters) > 0:
                print("converged")
                return [mu, min_dists, weights_new]
            oldclusters = clusters
            if i == maxiter - 1:
                print("maxiter reached")
    return [mu, min_dists, weights_new]

def find_centers_multiple(X, weights, K = 200, n_runs = 10, tol=1e-8):
    [mu, dists, weights_new] = find_centers_and_min_dists(X, weights, K, tol=tol)
    #for i in range(1000):
    #  print(dists[i])
    indxs=dists==0.0
    print(np.sum(indxs))
    #print(np.min(dists))
    cost = np.sum(dists)
    for n in range(n_runs-1):
        [mu_temp, dists_temp, weights_new_temp] = find_centers_and_min_dists(X, weights, K, tol=tol)
        cost_temp = np.sum(dists_temp)
        if cost_temp < cost:
            print("Found better clusters.")
            [mu, dists, weights_new] = [mu_temp, dists_temp, weights_new_temp]
            cost = cost_temp
    #weights_new should be the number of points assigned to the cluster s.t a cluster with a lot of points
    #becomes more weight in the reduce step
    #try to pass it as key
    return [mu, weights_new]
####

# Quadratic and Linear features
def feature_map_kmeans(features, K = 5):
    nump_feats = np.array(features)
    n = nump_feats.shape[0]
    weights = np.ones(n)
    # print(nump_feats)
    [muAll, weights] = find_centers_multiple(X = nump_feats, weights = weights, K = K, n_runs = 2, tol = 1e-3)
    
    dist_to_cluster = cluster_points_and_dist_all(X = nump_feats, mu = muAll, norm_ord = 2)
    return dist_to_cluster

#this function gets called once in the very beginning
def set_articles(articles):
    global A_a, b_a, avali_articles
    
    # The articles with the transformed features.
    article_ids = articles.keys()
    # avali_articles = {key: feature_map_add1(articles[key]) for key in article_ids} # this does not help for kmeans...
    
    ####
    # get article features using kmeans as on slide 38 of dm-12-bandits-annotated.pdf (NO logistic regression)
    articles_no_key = np.array([articles[key] for key in article_ids])
    dist_to_cluster = feature_map_kmeans(features = articles_no_key, K = 20) # number of clusters K = 6 oder K = 10
    dist_to_cluster = np.exp( -1 * np.square(dist_to_cluster))
    sum_per_row = np.sum(dist_to_cluster, axis = 1)
    dist_to_cluster = dist_to_cluster / sum_per_row[:,None]
    avali_articles = {article_ids[i]: list(dist_to_cluster[i, :]) for i in range(len(article_ids))}
    ####
    
    # Set the number of articles and the number of article features.
    n_articles = len(article_ids)
    artic_feat_dim = len(avali_articles[article_ids[0]])
    print("Total: " + str(n_articles) + " articles and " + str(artic_feat_dim) + " features per article.")
    
    # Initialize A_a to identity matrix and b_a to zero for every article
    A_a = {key: np.identity(artic_feat_dim) for key in article_ids}
    b_a = {key: np.zeros(artic_feat_dim) for key in article_ids}

#this function gets called alternating with recommend
def update(reward):
    global curr_chosen_artic, A_a, b_a, avali_articles
    #~ print(curr_user_features)
    chosen_art_feats = avali_articles[curr_chosen_artic]
    A_a[curr_chosen_artic] += np.outer(chosen_art_feats, chosen_art_feats)
    b_a[curr_chosen_artic] += reward * np.array(chosen_art_feats)


#this function gets called alternating with update (this one first)
def recommend(time, user_features, choices):
    global alpha, curr_chosen_artic, avali_articles
    curr_user_features = user_features
    pt_a_max = 0
    pt_a_argmax = choices[0]
    for key in choices:
        art_feats = avali_articles[key]
        pt_a_new = np.dot(np.linalg.solve(A_a[key], b_a[key]), art_feats) + alpha * np.sqrt(np.dot(art_feats, np.linalg.solve(A_a[key], art_feats)))
        if pt_a_new > pt_a_max:
            pt_a_max = pt_a_new
            pt_a_argmax = key
    curr_chosen_artic = pt_a_argmax
    # print("Article " + str(pt_a_argmax) + " recommended.")
    return pt_a_argmax
