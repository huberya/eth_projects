import numpy as np

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt ucb_basic_handin_kmeans.py

#this looks very similar to what we want to do (however its using a class which we cannot I think)
#I might be wrong though 
#need to adapt it somewhat but looks reasonable (look how class is accessed globaly below)
#https://github.com/Fengrui/HybridLinUCB-python/blob/master/policy_hybrid.py

class HybridUCB:
    def __init__(self):
        self.article_features = {}
    
        # upper bound coefficient
        self.alpha = 2.4 #1 + np.sqrt(np.log(2/delta)/2)
        self.r1 = 0.5
        self.r0 = -20
        # dimension of user features = d
        self.d = 5
        # dimension of article features = k
        self.k = self.d*self.d
        # A0 : matrix to compute hybrid part, k*k
        self.A0 = np.identity(self.k)
        self.A0I = np.identity(self.k)
        # b0 : vector to compute hybrid part, k
        self.b0 = np.zeros((self.k, 1))
        # Aa : collection of matrix to compute disjoint part for each article a, d*d
        self.Aa = {}
        # AaI : collection of matrix to compute disjoint part for each article a, d*d
        self.AaI = {}
        # Ba : collection of matrix to compute hybrid part, d*k
        self.Ba = {}
        # BaT : collection of matrix to compute hybrid part, d*k
        self.BaT = {}
        # ba : collection of vectors to compute disjoin part, d*1
        self.ba = {}
        
        
        # other dicts to spped up computation
        self.AaIba = {}
        self.AaIBa = {}
        self.BaTAaI = {}
        self.theta = {}

        self.beta = np.zeros((self.k, 1))
        
        self.index = {}
        
        self.a_max = 0
        
        self.z = None
        self.zT = None
        self.xaT = None
        self.xa = None

    def get_Aa(self):
        return self.Aa

    def set_Aa(self, Aa):
        self.Aa = Aa
    
    def get_ba(self):
        return self.ba
    
    def set_ba(self, ba):
        self.ba = ba
    
    def get_AaI(self):
        return self.AaI
    
    def set_AaI(self, AaI):
        self.AaI = AaI
    
    def get_theta(self):
        return self.theta

    def set_theta(self, theta):
        self.theta = theta

    # Evaluator will call this function and pass the article features.
    # Check runner.py description for details.
    def set_articles(self, art):
        # init collection of matrix/vector Aa, Ba, ba
        i = 0
        art_len = len(art)
        #pushes back 0 matrices for each article (into list)
        self.article_features = np.zeros((art_len, 1, self.d))
        self.Aa = np.zeros((art_len, self.d, self.d))
        self.AaI = np.zeros((art_len, self.d, self.d))
        self.Ba = np.zeros((art_len, self.d, self.k))
        self.BaT = np.zeros((art_len, self.k, self.d))
        self.ba = np.zeros((art_len, self.d, 1))
        self.AaIba = np.zeros((art_len, self.d, 1))
        self.AaIBa = np.zeros((art_len, self.d, self.k))
        self.BaTAaI = np.zeros((art_len, self.k, self.d))
        self.theta = np.zeros((art_len, self.d, 1))
        #set features for each article (will this work for huge amount?)
        #isnt this kinda stupid?
        for key in art:
            self.index[key] = i
            self.article_features[i] = art[key] # [1:]
            self.Aa[i] = np.identity(self.d)
            self.AaI[i] = np.identity(self.d)
            #this whole part doesnt do anything
            #self.Ba[i] = np.zeros((self.d, self.k))
            #self.BaT[i] = np.zeros((self.k, self.d))
            #self.ba[i] = np.zeros((self.d, 1))
            #self.AaIba[i] = np.zeros((self.d, 1))
            #self.AaIBa[i] = np.zeros((self.d, self.k))
            #self.BaTAaI[i] = np.zeros((self.k, self.d))
            #self.theta[i] = np.zeros((self.d, 1))
            i += 1
    

    # This function will be called by the evaluator.
    # Check task description for details.
    def update(self, reward):
        if reward == -1:
            pass
        elif reward == 1 or reward == 0:
            if reward == 1:
                r = self.r1
            else:
                r = self.r0
            
            self.A0 += np.dot(self.BaTAaI[self.a_max], self.Ba[self.a_max])
            self.b0 += np.dot(self.BaTAaI[self.a_max], self.ba[self.a_max])
            self.Aa[self.a_max] += np.dot(self.xa, self.xaT)
            self.AaI[self.a_max] = np.linalg.inv(self.Aa[self.a_max])
            self.Ba[self.a_max] += np.dot(self.xa, self.zT)
            self.BaT[self.a_max] = np.transpose(self.Ba[self.a_max])
            self.ba[self.a_max] += r * self.xa
            self.AaIba[self.a_max] = np.dot(self.AaI[self.a_max], self.ba[self.a_max])
            self.AaIBa[self.a_max] = np.dot(self.AaI[self.a_max], self.Ba[self.a_max])
            self.BaTAaI[self.a_max] = np.dot(self.BaT[self.a_max], self.AaI[self.a_max])
            
            self.A0 += np.dot(self.z, self.zT) - np.dot(self.BaTAaI[self.a_max], self.Ba[self.a_max])
            self.b0 += r * self.z - np.dot(self.BaT[self.a_max], np.dot(self.AaI[self.a_max], self.ba[self.a_max]))
            self.A0I = np.linalg.inv(self.A0)
            self.beta = np.dot(self.A0I, self.b0)
            self.theta = self.AaIba - np.dot(self.AaIBa, self.beta)#self.AaI[article].dot(self.ba[article] - self.Ba[article].dot(self.beta))
                
        else:
        # error
            print("something unexpected happened")
            pass
    
    # This function will be called by the evaluator.
    # Check task description for details.
    # Use vectorized code to increase speed
    #not too sure about this function
    def recommend(self, timestamp, user_features, articles):
        article_len = len(articles)
        # za : feature of current user/article combination, k*1
        self.xaT = np.array([user_features[1:]])
        self.xa = np.transpose(self.xaT)
        # recommend using hybrid ucb
        # fast vectorized for loops
        
        index = [self.index[article] for article in articles]
        
        article_features_tmp = self.article_features[index]

        zaT_tmp = np.einsum('i,j', article_features_tmp.reshape(-1), user_features[1:]).reshape(article_len, 1, self.k)
        za_tmp = np.transpose(zaT_tmp, (0,2,1))#np.transpose(zaT_tmp,(0,2,1))
    
        #np.dot(self.A0I, np.dot(BaTAaI_tmp, self.xa)) (20, 36, 1)
        A0IBaTAaIxa_tmp = np.transpose(np.dot(np.transpose(np.dot(self.BaTAaI[index], self.xa), (0,2,1)), np.transpose(self.A0I)), (0,2,1))
        
        A0Iza_tmp = np.transpose(np.dot(zaT_tmp, np.transpose(self.A0I)), (0,2,1)) # (20, 36, 1)
        A0Iza_diff_2A0IBaTAaIxa_tmp = A0Iza_tmp - 2*A0IBaTAaIxa_tmp
        
        # np.dot(zaT_tmp, A0Iza_diff_2A0IBaTAaIxa_tmp), (20, 1, 1)
        sa_1_tmp = np.sum(za_tmp.reshape(article_len,self.k,1,1)*A0Iza_diff_2A0IBaTAaIxa_tmp.reshape(article_len, self.k,1,1),-3)
        
        # np.dot(AaIBa_tmp, A0IBaTAaIxa_tmp)
        AaIxa_add_AaIBaA0IBaTAaIxa_tmp = np.dot(self.AaI[index], self.xa) + np.sum(np.transpose(self.AaIBa[index], (0,2,1)).reshape(article_len, self.k,self.d,1)*A0IBaTAaIxa_tmp.reshape(article_len,self.k,1,1),-3)
        sa_2_tmp = np.transpose(np.dot(np.transpose(AaIxa_add_AaIBaA0IBaTAaIxa_tmp,(0,2,1)),self.xa),(0,2,1))
        sa_tmp = sa_1_tmp + sa_2_tmp
        # np.dot(self.xaT, self.theta[article])
        xaTtheta_tmp = np.transpose(np.dot(np.transpose(self.theta[index],(0,2,1)),self.xa),(0,2,1))
        
        max_index = np.argmax(np.dot(zaT_tmp, self.beta) + xaTtheta_tmp + self.alpha * np.sqrt(sa_tmp))
        
        self.z = za_tmp[max_index]
        self.zT = zaT_tmp[max_index]
        art_max = index[max_index]
        
        # article index with largest UCB
        # global a_max, entries
        self.a_max = art_max
        
        # entries += 1
        
        # if entries % 100 == 0:
        #     print entries, evaluated, clicked, clicked / evaluated 
        
        return articles[max_index]


#### kmeans for feature transform
#returns the closest cluster index and the distance to it for each sample
def cluster_points_and_dist_weighted(X, mu, weights, norm_ord = 2):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.multiply(weights, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord))
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

def cluster_points_and_dist(X, mu, norm_ord = 1):
    n_samples = X.shape[0]
    n_features = X.shape[1]
    K = mu.shape[0]
    clusters = np.zeros(n_samples)
    min_dists = np.ones(n_samples) * 100000000
    for k in range(K):
        dists_to_cluster_k = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
        indxs = dists_to_cluster_k < min_dists
        clusters[indxs] = k
        min_dists[indxs] = dists_to_cluster_k[indxs]
    return [clusters, min_dists]

def cluster_points_and_dist_all(X, mu, norm_ord = 1):
    n_samples = len(X) # X.shape[0]
    # n_features = X.shape[1]
    K = mu.shape[0]
    cluster_dist = np.zeros((n_samples, K))
    for k in range(K):
        cluster_dist[:, k] = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k,:]), axis = 1, ord = norm_ord)
    return cluster_dist

# Restart = True if no data point gets assigned to a certain cluster
#now also returns n_ass since this should be the weight a cluster gets in the reduction step
def reevaluate_centers(mu, X, clusters, weights):
    K = mu.shape[0]
    ass_weight=np.ones(K)
    newmu=np.zeros((K,X.shape[1]))
    for k in range(K):
        indxs = clusters == k
        
        n_ass = np.sum(indxs)
        #print(n_ass)
        n_weighted=np.sum(weights[indxs])
        ass_weight[k]=n_ass
        if n_ass > 0:
            #newmu[k,:]=np.matmul(np.transpose(weights[indxs]),X[indxs])/n_weighted
            newmu[k,:]=np.mean(X[indxs], axis=0)
        #mu[k,:] = np.sum(weights[indxs]*X[indxs], axis = 0) / n_weighted
        else:
            return [newmu, True, ass_weight]
    #print(np.sum(ass_weight))
    return [newmu, False, ass_weight]


def has_converged(mu, oldmu, tol = 1e-8):
    return np.linalg.norm(mu-oldmu) < tol
#return False

# Code inspired by:
# https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
# Changed the distance computation to linear complexity!! O(K * #samples)

def initialize_centers_plus_plus(X, K, p = 3):
    # Initialize to K random centers using Kmeans++
    n_samples = X.shape[0]
    n_features = X.shape[1]
    mu = np.zeros((K, n_features))
    mu[0,:] = X[np.random.randint(n_samples),:]
    dists = np.linalg.norm(X - np.outer(np.ones(n_samples), mu[0,:]), axis = 1)**p
    for k in range(K-1):
        next_mu = X[np.random.choice(len(X), 1, p=dists/sum(dists))[0],:]
        # Alternative possibility:
        #next_mu = X[np.argmax(dists),:]
        mu[k+1,:] = next_mu
        dists = np.minimum(dists, np.linalg.norm(X - np.outer(np.ones(n_samples), mu[k+1,:]), axis = 1)**p)
    return mu


def find_centers_and_min_dists(X, weights, K = 200, tol = 1e-8):
    n_samples = X.shape[0]
    if weights.shape[0]!=n_samples:
        print("missmatch in dimension between X and weights, supposed to have same first dimensiont")
        return
    min_dists = np.ones(n_samples)
    clusters = np.ones(n_samples, dtype = int)
    restart = True
    maxiter = 11
    while restart:
        mu = initialize_centers_plus_plus(X, K)
        oldclusters = np.copy(clusters)
        restart = False
        print("Iterating...")
        for i in range(maxiter):
            # print("Iteration " + str(i))
            # Assign all points in X to clusters
            [clusters, min_dists] = cluster_points_and_dist(X, mu)
            # Reevaluate centers
            [mu, restart, weights_new] = reevaluate_centers(mu, X, clusters, weights)
            if restart:
                print("Restarting...")
                break
            # Check if the cluster assignments have changed
            if not np.sum(clusters != oldclusters) > 0:
                print("converged")
                return [mu, min_dists, weights_new]
            oldclusters = clusters
            if i == maxiter - 1:
                print("maxiter reached")
    return [mu, min_dists, weights_new]

def find_centers_multiple(X, weights, K = 200, n_runs = 10, tol=1e-8):
    [mu, dists, weights_new] = find_centers_and_min_dists(X, weights, K, tol=tol)
    #for i in range(1000):
    #  print(dists[i])
    indxs=dists==0.0
    print(np.sum(indxs))
    #print(np.min(dists))
    cost = np.sum(dists)
    for n in range(n_runs-1):
        [mu_temp, dists_temp, weights_new_temp] = find_centers_and_min_dists(X, weights, K, tol=tol)
        cost_temp = np.sum(dists_temp)
        if cost_temp < cost:
            print("Found better clusters.")
            [mu, dists, weights_new] = [mu_temp, dists_temp, weights_new_temp]
            cost = cost_temp
    #weights_new should be the number of points assigned to the cluster s.t a cluster with a lot of points
    #becomes more weight in the reduce step
    #try to pass it as key
    return [mu, weights_new]

# Quadratic and Linear features
def feature_map_kmeans(features, K = 5):
    nump_feats = np.array(features)
    n = nump_feats.shape[0]
    weights = np.ones(n)
    # print(nump_feats)
    [muAll, weights] = find_centers_multiple(X = nump_feats, weights = weights, K = K, n_runs = 1, tol = 1e-3)
    # If it is too slow, then we could use just one run or just use kmeans++ for initialization
    
    dist_to_cluster = cluster_points_and_dist_all(X = nump_feats, mu = muAll, norm_ord = 2)
    return dist_to_cluster

#### end kmeans


HybridUCBObj = None

t = 0
break_point = 0

#this function gets called once in the very beginning
#this might be call by reference
#otherwise im not too certain what this should be good for since it does not
#take any return value from this
def set_articles(articles):
    global HybridUCBObj
    HybridUCBObj = HybridUCB()
    
    # The articles with the transformed features.
    article_ids = articles.keys()
    # get article features using kmeans as on slide 38 of dm-12-bandits-annotated.pdf (NO logistic regression)
    articles_no_key = np.array([articles[key] for key in article_ids])
    dist_to_cluster = feature_map_kmeans(features = articles_no_key, K = 5) # number of clusters K = 6 oder K = 10 = > can be changed in linUCB_DLM_kmeans.py but that is slower.
    # I do not know how we can change the number of article features for that class.
    # I commented [1:] on line 102. Five clusters and five features such that we do not have to remove the first feature.
    dist_to_cluster = np.exp( -1 * np.square(dist_to_cluster))
    sum_per_row = np.sum(dist_to_cluster, axis = 1)
    dist_to_cluster = dist_to_cluster / sum_per_row[:,None]
    articles = {article_ids[i]: list(dist_to_cluster[i, :]) for i in range(len(article_ids))}
    
    HybridUCBObj.set_articles(articles)
    return


#this function gets called alternating with recommend
#update is called with the reward if the recommended is the chosen one (chosen according to log)
#update is called with reward=-1 else
#this should probably do something with policy which is a global variable? (no real other option
#since we cannot pass aditional stuff
def update(reward):
    HybridUCBObj.update(reward)
    return


#this function gets called alternating with update
#the returnvalue of this function is the id of the selected item (i.e. the one to recommend)
def recommend(time, user_features, choices):
    global t
    t+=1
    return HybridUCBObj.recommend(time, user_features, choices)



