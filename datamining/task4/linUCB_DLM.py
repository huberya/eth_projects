import numpy as np

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt linUCB_DLM.py

# This is the LinUCB with Disjoint Linear Models from: 
# http://rob.schapire.net/papers/www10.pdf
# It regards all users as equal.
# Does not perform any good :(

# Hyperparameters (global constants)
alpha = 3

# Gloobal variables
# Constants:
avali_articles = 0
# Updated vars:
A_a = 0
b_a = 0
curr_chosen_artic = 0

# Extend the features
# Original Features
def feature_map_id(features):
    return np.array(features)
# Quadratic and Linear features
def feature_map_quad(features):
    nump_feats = np.array(features)
    n = len(features)
    mapped_feats = np.zeros((n*(n+1),))
    for k in range(n):
        for i in range(n):
            mapped_feats[k + n * i] = nump_feats[k] * nump_feats[i]
    for k in range(n):
        mapped_feats[n*n+k] = nump_feats[k]
    return mapped_feats    
    

#this function gets called once in the very beginning
def set_articles(articles):
    global A_a, b_a, avali_articles
    
    # The articles with the transformed features.
    article_ids = articles.keys()
    avali_articles = {key: feature_map_id(articles[key]) for key in article_ids}
    
    # Set the number of articles and the number of article features.
    n_articles = len(article_ids)
    artic_feat_dim = len(avali_articles[article_ids[0]])
    print("Total: " + str(n_articles) + " articles and " + str(artic_feat_dim) + " features per article.")
    
    # Initialize A_a to identity matrix and b_a to zero for every article
    A_a = {key: np.identity(artic_feat_dim) for key in article_ids}
    b_a = {key: np.zeros(artic_feat_dim) for key in article_ids}

#this function gets called alternating with recommend
def update(reward):
    global curr_chosen_artic, A_a, b_a, avali_articles
    #~ print(curr_user_features)
    chosen_art_feats = avali_articles[curr_chosen_artic]
    A_a[curr_chosen_artic] += np.outer(chosen_art_feats, chosen_art_feats)
    b_a[curr_chosen_artic] += reward * np.array(chosen_art_feats)


#this function gets called alternating with update (this one first)
def recommend(time, user_features, choices):
    global alpha, curr_chosen_artic, avali_articles
    curr_user_features = user_features
    pt_a_max = 0
    pt_a_argmax = choices[0]
    for key in choices:
        art_feats = avali_articles[key]
        pt_a_new = np.dot(np.linalg.solve(A_a[key], b_a[key]), art_feats) + alpha * np.sqrt(np.dot(art_feats, np.linalg.solve(A_a[key], art_feats)))
        if pt_a_new > pt_a_max:
            pt_a_max = pt_a_new
            pt_a_argmax = key
    curr_chosen_artic = pt_a_argmax
    # print("Article " + str(pt_a_argmax) + " recommended.")
    return pt_a_argmax
