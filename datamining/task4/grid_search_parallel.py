# MIT License
#
# Copyright (c) 2016 las.inf.ethz.ch
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Evaluation framework for the Bandit setup (Task4, DM2016)"""

import argparse
import io
import imp
import logging
import numpy as np
import resource
import signal
import sys
from multiprocessing import Process, Value, Pool
from time import sleep
from functools import partial
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def process_line(policy, logline):
    chosen = int(logline.pop(7))
    reward = int(logline.pop(7))
    time = int(logline[0])
    user_features = [float(x) for x in logline[1:7]]
    articles = [int(x) for x in logline[7:]]
    return reward, chosen, policy.recommend(time, user_features, articles)


def evaluate(policy, input_generator):
    score = 0.0
    impressions = 0.0
    n_lines = 0.0
    for line in input_generator:
        n_lines += 1
        reward, chosen, calculated = process_line(
            policy, line.strip().split())
        if calculated == chosen:
            policy.update(reward)
            score += reward
            impressions += 1
        else:
            policy.update(-1)
    if impressions < 1:
        logger.info("No impressions were made.")
        return 0.0
    else:
        score /= impressions
        logger.info("CTR achieved by the policy: %.5f" % score)
        return score


def import_from_file(f):
    """Import code from the specified file"""
    mod = imp.new_module("mod")
    exec f in mod.__dict__
    return mod

def runworker_alpha(source, articles_file, alpha):
    policy = import_from_file(source)
    articles_np = np.loadtxt(articles_file)
    print("func")
    articles={}
    for art in articles_np:
        articles[int(art[0])] = [float(x) for x in art[1:]]
    policy.set_articles(articles, alpha, besttime, bestr0, bestr1)
    with io.open(log_file, 'rb', buffering=1024*1024*512) as inf:
        currval = evaluate(policy, inf)
        print("alpha="+str(alpha)+" r0="+str(r0)+" r1="+str(r1)+"timeparam"+str(timep)+" achiveded:"+str(currval))
        if currval>bestscore.value:
            bestscore.value=currval
            bestalpha.value=alpha
    return True
def testfunc(alphas):
    print("this works alpha="+str(alphas))
    return True
def run(source, log_file, articles_file):
    n_worker=24
    bestalpha=Value('d', 1.2)
    bestr0=Value('d', -20.0)
    bestr1=Value('d', 0.5)
    besttime=Value('d', 0.0)
    bestscore=Value('d', 0.0)
    pool=Pool(processes=n_worker)
    policy = import_from_file(source)
    articles_np = np.loadtxt(articles_file)
    
    alphas=np.linspace(1.0, 3.0, num=30)
    alpha_func=partial(partial(runworker_alpha, source=source), articles_file=articles_file)
    #results = pool.map(testfunc, alphas)
    results = pool.map(alpha_func, alphas)
    #for alpha in frange (1.0, 3.0, 0.1): 
        #pool.apply_async(runworker, args=(policy, articles_np, alpha, bestr0, bestr1, besttime,bestscore))

    for r0 in frange (1.0, 20.0, 1.0):
        pool.apply_async(runworker, args=(policy, articles_np, bestalpha, -r0, bestr1, besttime,bestscore))

    for r1 in frange (0.1, 2.0, 0.1):
        pool.apply_async(runworker, args=(policy, articles_np, bestalpha, bestr0, r1, besttime,bestscore))

    for timep in frange (0.0, 0.00005, 0.000005):
        pool.apply_async(runworker, args=(policy, articles_np, bestalpha, bestr0, bestr1, timep,bestscore))

    #pool.close()
    #pool.join()
    sleep(10)
    print("best found params:")
    print("alpha="+str(bestalpha))
    print("timeparam="+str(besttime))
    print("r0="+str(bestr0))
    print("r1="+str(bestr1))
    return bestscore
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        'log_file', help='File containing the log.')
    parser.add_argument(
        'articles_file', help='File containing the article features.')
    parser.add_argument(
        'source_file', help='.py file implementing the policy.')
    parser.add_argument(
        '--log', '-l', help='Enable logging for debugging', action='store_true')
    args = parser.parse_args()
    with open(args.source_file, "r") as fin:
        source = fin.read()
    run(source, args.log_file, args.articles_file)
