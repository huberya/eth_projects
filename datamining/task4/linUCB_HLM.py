import numpy as np

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt linUCB_HLM.py

# This is the LinUCB with Hybrid Linear Models from: 
# http://rob.schapire.net/papers/www10.pdf
# Probably does not work

# Hyperparameters (global constants)
alpha = 3

# Gloobal variables
# Constants:
avali_articles = 0
user_feat_dim = 6
# Updated vars:
A_a = 0
B_a = 0
b_a = 0
curr_chosen_artic = 0
curr_user_feats = 0
A_0 = 0
b_0 = 0

# Extend the features
# Original Features
def feature_map_id(features):
    return np.array(features)
# Quadratic and Linear features
def feature_map_quad(features):
    nump_feats = np.array(features)
    n = len(features)
    mapped_feats = np.zeros((n*(n+1),))
    for k in range(n):
        for i in range(n):
            mapped_feats[k + n * i] = nump_feats[k] * nump_feats[i]
    for k in range(n):
        mapped_feats[n*n+k] = nump_feats[k]
    return mapped_feats    
# Add const feature
def feature_map_add1(features):
    return np.array([1] + features)

#this function gets called once in the very beginning
def set_articles(articles):
    global A_a, B_a, b_a, A_0, b_0, avali_articles, user_feat_dim
    
    # The articles with the transformed features.
    print(articles)
    print(articles.keys())
    print(articles.shape)
    article_ids = articles.keys()
    avali_articles = {key: feature_map_add1(articles[key]) for key in article_ids}
    
    # Set the number of articles and the number of article features.
    n_articles = len(article_ids)
    artic_feat_dim = len(avali_articles[article_ids[0]])
    print("Total: " + str(n_articles) + " articles and " + str(artic_feat_dim) + " features per article.")
    
    # Initialize A_a to identity matrix and b_a to zero for every article
    A_a = {key: np.identity(artic_feat_dim) for key in article_ids}
    B_a = {key: np.zeros((artic_feat_dim, user_feat_dim * artic_feat_dim)) for key in article_ids}
    b_a = {key: np.zeros(artic_feat_dim) for key in article_ids}
    A_0 = np.identity(user_feat_dim * artic_feat_dim)
    b_0 = np.zeros((user_feat_dim * artic_feat_dim,))

#this function gets called alternating with recommend
def update(reward):
    global A_a, B_a, b_a, A_0, b_0
    #~ print(curr_user_features)
    chosen_art_feats = avali_articles[curr_chosen_artic]
    curr_user_feats_loc = np.reshape(np.outer(curr_user_feats, chosen_art_feats),(-1,))
    key = curr_chosen_artic
    A_0 += np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], B_a[key]))
    b_0 += np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], b_a[key]))
    A_a[key] += np.outer(chosen_art_feats, chosen_art_feats)
    B_a[key] += np.outer(chosen_art_feats, curr_user_feats_loc)
    b_a[key] += reward * np.array(chosen_art_feats)
    A_0 += np.outer(curr_user_feats_loc, curr_user_feats_loc) - np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], B_a[key]))
    b_0 += reward * np.array(curr_user_feats_loc) - np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], b_a[key]))

#this function gets called alternating with update (this one first)
def recommend(time, user_features, choices):
    global curr_chosen_artic, curr_user_feats
    
    # s1 = set([109503, 109494, 109508, 109510, 109473, 109484, 109513, 109511, 109514, 109502, 109453, 109506, 109512, 109509])
    # s1 = set([109503, 109494, 109508, 109510, 109473])
    # s2 = set(choices)
    # choices = list(s2.intersection(s1))
    
    curr_user_feats = feature_map_id(user_features)
    pt_a_max = 0
    pt_a_argmax = choices[0]
    for key in choices:
        art_feats = avali_articles[key]
        curr_user_feats_loc = np.reshape(np.outer(curr_user_feats, art_feats),(-1,))
        beta_hat = np.linalg.solve(A_0, b_0)
        theta_a = np.linalg.solve(A_a[key],b_a[key] - np.matmul(B_a[key],beta_hat))
        st_a = 				(np.dot(curr_user_feats_loc, np.linalg.solve(A_0, curr_user_feats_loc)) 
							- 2*np.dot(curr_user_feats_loc, np.linalg.solve(A_0, np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], art_feats))))
							+ np.dot(art_feats, np.linalg.solve(A_a[key],art_feats)) 
							+ np.dot(art_feats, np.linalg.solve(A_a[key], np.matmul(B_a[key], np.linalg.solve(A_0, np.matmul(B_a[key].transpose(), np.linalg.solve(A_a[key], art_feats)))))))
        pt_a_new = np.dot(curr_user_feats_loc, beta_hat) + np.dot(art_feats, theta_a) + alpha * np.sqrt(st_a)
        if pt_a_new > pt_a_max:
            pt_a_max = pt_a_new
            pt_a_argmax = key
    curr_chosen_artic = pt_a_argmax
    #~ print("Article " + str(pt_a_argmax) + " recommended.")
    return pt_a_argmax
