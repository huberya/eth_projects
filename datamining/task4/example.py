import numpy as np

# To run use:
# python runner.py ./data/webscope-logs.txt ./data/webscope-articles.txt example.py

#this function gets called once in the very beginning
def set_articles(articles):
    pass

#this function gets called alternating with recommend
def update(reward):
    pass

a = 0

#this function gets called alternating with update
def recommend(time, user_features, choices):
    global a
    a = a+1
    #print(a)
    
    # best[0] the id of the webpage
    # best[1] how often someone clicked on the link
    best =  [[109503, 109494, 109508, 109510, 109473, 109484, 109513, 109511, 109514, 109502, 109453, 109506, 109512, 109509, 109501, 109498, 109495, 109515, 109505, 109492, 109519],
             [199, 177, 173, 172, 168, 167, 163, 159, 155, 150, 149, 145, 145, 136, 106, 103, 83, 77, 65, 64, 51]]

    ret = best[0][0] # 109503
        
    # ret = np.random.choice([109503, 109494, 109508, 109510, 109473, 109484, 109513, 109511, 109514, 109502])
    
    # ret = np.random.choice(choices)
    
    return ret
