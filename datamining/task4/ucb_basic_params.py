import numpy as np

# To run use:
# python grid_search.py ./data/webscope-logs.txt ./data/webscope-articles.txt ucb_basic_params.py
# python grid_search_parallel.py ./data/webscope-logs.txt ./data/webscope-articles.txt ucb_basic_params.py

#this looks very similar to what we want to do (however its using a class which we cannot I think)
#I might be wrong though 
#need to adapt it somewhat but looks reasonable (look how class is accessed globaly below)
#https://github.com/Fengrui/HybridLinUCB-python/blob/master/policy_hybrid.py

class HybridUCB:
    def __init__(self):
        self.article_features = {}
        self.article_time = {}
        # upper bound coefficient
        self.alpha = 2.4 #1 + np.sqrt(np.log(2/delta)/2)
        self.timeparam = 0.00005 #weight of article age
        self.r1 = 0.6
        self.r0 = -25
        # dimension of user features = d
        self.d = 5
        # dimension of article features = k
        self.k = self.d*self.d
        # A0 : matrix to compute hybrid part, k*k
        self.A0 = np.identity(self.k)
        self.A0I = np.identity(self.k)
        # b0 : vector to compute hybrid part, k
        self.b0 = np.zeros((self.k))
        # Aa : collection of matrix to compute disjoint part for each article a, d*d
        self.Aa = {}
        # AaI : collection of matrix to compute disjoint part for each article a, d*d
        self.AaI = {}
        # Ba : collection of matrix to compute hybrid part, d*k
        self.Ba = {}
        # ba : collection of vectors to compute disjoin part, d*1
        self.ba = {}
        
        
        #needed
        self.BaTAaIBa={}

        # other dicts to spped up computation (mostly bad I think)
        self.theta = {}

        self.beta = np.zeros((self.k))
        
        self.index = {}
        
        self.a_max = 0
        
        self.z = None
        self.xa = None

    # Evaluator will call this function and pass the article features.
    # Check runner.py description for details.
    def set_articles(self, art, alpha, timeparam, r0, r1):
        # init collection of matrix/vector Aa, Ba, ba
        self.alpha = alpha #1 + np.sqrt(np.log(2/delta)/2)
        self.timeparam = timeparam #weight of article age
        self.r1 = r1
        self.r0 = r0
        i = 0
        art_len = len(art)
        #pushes back 0 matrices for each article (into list)
        self.article_features = np.zeros((art_len, self.d))
        self.article_time=np.zeros((art_len))
        self.Aa = np.zeros((art_len, self.d, self.d))
        self.AaI = np.zeros((art_len, self.d, self.d))
        self.Ba = np.zeros((art_len, self.d, self.k))
        self.ba = np.zeros((art_len, self.d))
        self.BaTAaIBa=np.zeros((art_len, self.k, self.k))
        self.theta = np.zeros((art_len, self.d))
        #set features for each article (will this work for huge amount?)
        for key in art:
            self.index[key] = i
            self.article_features[i] = art[key][1:]
            self.Aa[i] = np.identity(self.d)
            self.AaI[i] = np.identity(self.d)
            i += 1
    

    # This function will be called by the evaluator.
    # Check task description for details.
    def update(self, reward):
        if reward == -1:
            pass
        elif reward == 1 or reward == 0:
            if reward == 1:
                r = self.r1
            else:
                r = self.r0
            
            self.A0 += self.BaTAaIBa[self.a_max]
            self.b0 += np.einsum('ji,j->i', self.Ba[self.a_max],np.einsum('ij,j->i', self.AaI[self.a_max], self.ba[self.a_max]))
            self.Aa[self.a_max] += np.einsum('i,k->ik', self.xa, self.xa)
            self.AaI[self.a_max] = np.linalg.inv(self.Aa[self.a_max])
            self.Ba[self.a_max] += np.einsum('i,k->ik', self.xa, self.z)
            self.ba[self.a_max] += r * self.xa
            #this hurts but I couldn't find a way around
            self.BaTAaIBa[self.a_max] = np.einsum('ji,jk->ik', self.Ba[self.a_max], np.einsum('ij,jk->ik', self.AaI[self.a_max], self.Ba[self.a_max]))
            
            self.A0 += np.einsum('i,j->ij', self.z, self.z) - self.BaTAaIBa[self.a_max]
            self.b0 += r * self.z - np.einsum('ji,j->i', self.Ba[self.a_max], np.einsum('ij,j->i',self.AaI[self.a_max], self.ba[self.a_max]))
            self.A0I = np.linalg.inv(self.A0)
            self.beta = np.dot(self.A0I, self.b0)
            self.theta = np.einsum('...ij,...j->...i',self.AaI, self.ba) - np.einsum('...ij,...j->...i', self.AaI, np.einsum('...ij,...j->...i', self.Ba, self.beta))#self.AaI[article].dot(self.ba[article] - self.Ba[article].dot(self.beta))
                
        else:
        # error
            print("something unexpected happened")
            pass
    
    # This function will be called by the evaluator.
    # Check task description for details.
    # Use vectorized code to increase speed
    #not too sure about this function
    def recommend(self, timestamp, user_features, articles):
        article_len = len(articles)
        # za : feature of current user/article combination, k*1
        self.xa = np.reshape(np.array([user_features[1:]]),(self.d))

        index = [self.index[article] for article in articles]
        global t
        t+=1
        newart = self.article_time[index]==0
        #this should be done differently for performance but i couldnt figure out how
        #however its very rare
        if np.any(newart):
            for i in range(article_len):
                if newart[i]:
                    self.article_time[index[i]]=t

        #this might be bad
        za_tmp = np.einsum('ki,j->kij', self.article_features[index], user_features[1:]).reshape(article_len, self.k)

        zTA0z=np.einsum('...j,...j->...', za_tmp, np.einsum('ij,kj->ki', self.A0I, za_tmp))
        #precompute common part
        AaI_tmp=self.AaI[index]

        A0IBaTAaIxa=np.einsum('ij, ki->kj', self.A0I, np.einsum('...kj, ...k->...j', self.Ba[index], np.dot(AaI_tmp, self.xa))) #this is O(narticles*n²) output is narticles*vector
        zTA0BTAaIxa=np.einsum('...i,...i->...', za_tmp, A0IBaTAaIxa) #this is O(narticles*n²)
        xTAaIx=np.einsum('i,ki->k', self.xa, np.einsum('kij,j->ki', AaI_tmp, self.xa))
        xTAaIBaA0IBaTAaIxa=np.einsum('i,ki->k', self.xa, np.einsum('...ij,...j->...i', AaI_tmp, np.einsum('...ij,...j->...i', self.Ba[index], A0IBaTAaIxa)))

        sa_tmp=zTA0z-2*zTA0BTAaIxa+xTAaIx+xTAaIBaA0IBaTAaIxa

        max_index = np.argmax(np.einsum('ki,i->k',za_tmp, self.beta) + np.einsum('i,ki->k', self.xa, self.theta[index]) + self.alpha * np.sqrt(sa_tmp)+self.timeparam*self.article_time[index])
        self.z = za_tmp[max_index]

        # article index with largest UCB
        # global a_max, entries
        self.a_max = index[max_index]
        
        return articles[max_index]


HybridUCBObj = None
t=0

#this function gets called once in the very beginning
#this might be call by reference
#otherwise im not too certain what this should be good for since it does not
#take any return value from this
def set_articles(articles, alpha, timeparam, r0, r1):
    global HybridUCBObj
    HybridUCBObj = HybridUCB()
    HybridUCBObj.set_articles(articles, alpha, timeparam, r0, r1)
    return


#this function gets called alternating with recommend
#update is called with the reward if the recommended is the chosen one (chosen according to log)
#update is called with reward=-1 else
#this should probably do something with policy which is a global variable? (no real other option
#since we cannot pass aditional stuff
def update(reward):
    HybridUCBObj.update(reward)
    return


#this function gets called alternating with update
#the returnvalue of this function is the id of the selected item (i.e. the one to recommend)
def recommend(time, user_features, choices):
    return HybridUCBObj.recommend(time, user_features, choices)



