(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10582,        359]
NotebookOptionsPosition[      9466,        317]
NotebookOutlinePosition[      9809,        332]
CellTagsIndexPosition[      9766,        329]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"x", "=", 
  RowBox[{"{", 
   RowBox[{"px", ",", "py", ",", "sx", ",", "sy", ",", "phi", ",", "b"}], 
   "}"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"px", ",", "py", ",", "sx", ",", "sy", ",", "phi", ",", "b"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7358191978591995`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"sqrt", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"px", "-", "xa"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"px", "-", "xa"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"py", "-", "ya"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"py", "-", "ya"}], ")"}]}]}], "]"}], "+", "wa"}], ",", " ", 
    RowBox[{
     RowBox[{"sqrt", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"px", "-", "xb"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"px", "-", "xb"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"py", "-", "yb"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"py", "-", "yb"}], ")"}]}]}], "]"}], "+", "wb"}], ",", 
    RowBox[{
     RowBox[{"sqrt", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"px", "-", "xc"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"px", "-", "xc"}], ")"}]}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"py", "-", "yc"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"py", "-", "yc"}], ")"}]}]}], "]"}], "+", "wc"}], ",", 
    RowBox[{"phi", "+", "b", "+", "wg"}], ",", " ", 
    RowBox[{"phi", "+", "wn"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7358192050028696`*^9, 3.735819398053125*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"wa", "+", 
    RowBox[{"sqrt", "[", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"px", "-", "xa"}], ")"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"py", "-", "ya"}], ")"}], "2"]}], "]"}]}], ",", 
   RowBox[{"wb", "+", 
    RowBox[{"sqrt", "[", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"px", "-", "xb"}], ")"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"py", "-", "yb"}], ")"}], "2"]}], "]"}]}], ",", 
   RowBox[{"wc", "+", 
    RowBox[{"sqrt", "[", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"px", "-", "xc"}], ")"}], "2"], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"py", "-", "yc"}], ")"}], "2"]}], "]"}]}], ",", 
   RowBox[{"b", "+", "phi", "+", "wg"}], ",", 
   RowBox[{"phi", "+", "wn"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.735819386988783*^9, 3.735819400677908*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{"D", "[", 
   RowBox[{"z", ",", 
    RowBox[{"{", "x", "}"}]}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7358194111459136`*^9, 3.735819424288289*^9}, {
  3.73581956824574*^9, 3.735819573966571*^9}, {3.735819766597254*^9, 
  3.7358198056282988`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"px", "-", "xa"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xa"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "ya"}], ")"}], "2"]}], "]"}]}], 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"py", "-", "ya"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xa"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "ya"}], ")"}], "2"]}], "]"}]}], "0", "0", "0", 
      "0"},
     {
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"px", "-", "xb"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xb"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "yb"}], ")"}], "2"]}], "]"}]}], 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"py", "-", "yb"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xb"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "yb"}], ")"}], "2"]}], "]"}]}], "0", "0", "0", 
      "0"},
     {
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"px", "-", "xc"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xc"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "yc"}], ")"}], "2"]}], "]"}]}], 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"py", "-", "yc"}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["sqrt", "\[Prime]",
         MultilineFunction->None], "[", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"px", "-", "xc"}], ")"}], "2"], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"py", "-", "yc"}], ")"}], "2"]}], "]"}]}], "0", "0", "0", 
      "0"},
     {"0", "0", "0", "0", "1", "1"},
     {"0", "0", "0", "0", "1", "0"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7358198063938*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.735819581137886*^9, 3.7358195936749206`*^9}, 
   3.7358197695345416`*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7358197718624587`*^9, 3.7358197724561815`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7358196258238926`*^9, 3.735819628526803*^9}}],

Cell[BoxData[""], "Print",
 CellMargins->{{20, 10}, {Inherited, Inherited}},
 GeneratedCell->False,
 CellAutoOverwrite->False,
 CellChangeTimes->{{3.7358196344371796`*^9, 3.735819637890046*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"w", "=", 
   RowBox[{"{", 
    RowBox[{"wa", ",", "wb", ",", "wc", ",", "wg", ",", "wn"}], 
    "}"}]}]}]], "Input",
 CellChangeTimes->{
  3.735819612934199*^9, {3.7358205734001245`*^9, 3.73582058849277*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"wa", ",", "wb", ",", "wc", ",", "wg", ",", "wn"}], "}"}]], "Output",
 CellChangeTimes->{3.7358205895395126`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", 
  RowBox[{"D", "[", 
   RowBox[{"z", ",", 
    RowBox[{"{", "w", "}"}]}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.735820591226894*^9, 3.7358206022261367`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", "0", "0", "0", "0"},
     {"0", "1", "0", "0", "0"},
     {"0", "0", "1", "0", "0"},
     {"0", "0", "0", "1", "0"},
     {"0", "0", "0", "0", "1"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.735820602913533*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7358195639960175`*^9, 3.735819564058502*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.7358194641327047`*^9},
 NumberMarks->False],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.735819458070616*^9, 3.7358194581643395`*^9}}],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.735819428912897*^9}]
},
WindowSize->{958, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"10.3 for Microsoft Windows (64-bit) (October 9, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 146, 4, 31, "Input"],
Cell[729, 28, 166, 4, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[932, 37, 1431, 45, 52, "Input"],
Cell[2366, 84, 1007, 32, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3410, 121, 307, 7, 31, "Input"],
Cell[3720, 130, 3341, 102, 115, "Output"]
}, Open  ]],
Cell[7076, 235, 122, 2, 31, "Input"],
Cell[7201, 239, 96, 1, 31, InheritFromParent],
Cell[CellGroupData[{
Cell[7322, 244, 94, 1, 31, "Input"],
Cell[7419, 247, 194, 4, 23, "Print"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7650, 256, 266, 7, 52, "Input"],
Cell[7919, 265, 152, 3, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8108, 273, 208, 5, 31, "Input"],
Cell[8319, 280, 753, 21, 163, "Output"]
}, Open  ]],
Cell[9087, 304, 94, 1, 31, InheritFromParent],
Cell[9184, 307, 91, 2, 31, "Input"],
Cell[9278, 311, 94, 1, 31, InheritFromParent],
Cell[9375, 314, 87, 1, 52, "Input"]
}
]
*)

(* End of internal cache information *)

