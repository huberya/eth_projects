function [postParticles] = Estimator(prevPostParticles, sens, act, init)
% [postParticles] = Estimator(prevPostParticles, sens, act, init)
%
% The estimator function. The function will be called in two different
% modes: If init==1, the estimator is initialized. If init == 0, the
% estimator does an iteration for a single sample time interval Ts (KC.ts)
% using the previous posterior particles passed to the estimator in
% prevPostParticles and the sensor measurements and control inputs.
%
% You must edit this function.
%
% Inputs:
%   prevPostParticles   previous posterior particles at discrete time k-1,
%                       which corresponds to continuous time t = (k-1)*Ts
%                       The variable is a struct whose fields are arrays
%                       that correspond to the posterior particle states.
%                       The fields are: (N is number of particles)
%                       .x = (2xN) array with the x-locations (metres)
%                       .y = (2xN) array with the y-locations (metres)
%                       .h = (2xN) array with the headings (radians)
%                       The first row in the arrays corresponds to robot A.
%                       The second row corresponds to robot B.
%
%   sens                Sensor measurements at discrete time k (t = k*Ts),
%                       [4x1]-array, an Inf entry indicates no measurement
%                       of the corresponding sensor.
%                       sens(1): distance reported by sensor 1 (metres)
%                       sens(2): distance reported by sensor 2 (metres)
%                       sens(3): distance reported by sensor 3 (metres)
%                       sens(4): distance reported by sensor 4 (metres)
%
%   act                 Control inputs u at discrete time k-1, which are
%                       constant during a time interval Ts:
%                       u(t) = u(k-1) for (k-1)*Ts <= t < k*Ts
%                       [2x1]-array:
%                       act(1): velocity of robot A, u_A(k-1) (metres/second)
%                       act(2): velocity of robot B, u_B(k-1) (metres/second)
%
%   init                Boolean variable indicating wheter the estimator
%                       should be initialized (init = 1) or if a regular
%                       estimator update should be performed (init = 0).
%                       OPTIONAL ARGUMENT. By default, init = 0.
%
% Outputs:
%   postParticles       Posterior particles at discrete time k, which
%                       corresponds to the continuous time t = k*Ts.
%                       The variable is a struct whose fields are arrays
%                       that correspond to the posterior particle states.
%                       The fields are: (N is number of particles)
%                       .x = (2xN) array with the x-locations (metres)
%                       .y = (2xN) array with the y-locations (metres)
%                       .h = (2xN) array with the headings (radians)
%                       The first row in the arrays corresponds to robot A.
%                       The second row corresponds to robot B.
%
% Class:
% Recursive Estimation
% Spring 2018
% Programming Exercise 2
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control

% Check if init argument was passed to estimator:
if(nargin < 4)
    % if not, set to default value:
    init = 0;
end

%% Mode 1: Initialization
% Set number of particles:
N = 5000; % obviously, you will need more particles than 10.
if (init)
    % Do the initialization of your estimator here!
    % These particles are the posterior particles at discrete time k = 0
    % which will be fed into your estimator again at k = 1
    % This has implicit assumption of either both being on top or bottom
    % since particles correspond
    N_rand = round(N/2);
%     postParticles.x = [2*KC.L*ones(1,N); zeros(1,N)]; %robot 1 starts at the right border, robot 2 at the left border
%     postParticles.y = [zeros(2, N_rand), KC.L*ones(2, N-N_rand)];
%     postParticles.h = [pi/2 + pi/2*rand(1, N_rand), -pi + pi/2*rand(1, N-N_rand);
%                        pi/2*rand(1, N_rand), -pi/2 + pi/2*rand(1, N-N_rand)]; %think about probabilities where it !can! look
%     postParticles.x = [2*KC.L*ones(1,N); zeros(1,N)]; %robot 1 starts at the right border, robot 2 at the left border
%     postParticles.y = zeros(2,N); %will be set in loop
%     postParticles.h = zeros(2,N); %will be set in loop
%     
%     for i=1:N
%         ytemp=unidrnd(2,2,1)-1
%         postParticles.y(:,i)=ytemp;
%         %cases for which direction is allowed
%         htemp=zeros(2,1);
%         if ytemp(1,1)==0
%             htemp(1,1) = pi/2 + pi/2*rand(1);
%         else
%             htemp(1,1) = -pi + pi/2*rand(1);
%         end
%         if ytemp(2,1)==0
%             htemp(2,1) = pi/2*rand(1);
%         else
%             htemp(2,1) = -pi/2 + pi/2*rand(1);
%         end
%         postParticles.h(:,i)=htemp;
%     end
%     %rescale y
%     postParticles.y=postParticles.y*KC.L;

    postParticles.x = zeros(2,N);
    postParticles.y = zeros(2,N);
    postParticles.h = zeros(2,N);
    L = KC.L;
    cornerA = round(rand(N,1)); % zero for S1 (2L,0), 1 for S2 (2L,L)
    cornerB = round(rand(N,1)); % zero for S3 (0, L), 1 for S4 (0, 0)
    for (n=1:N)
       % robot A
       if (cornerA(n)==0)
           postParticles.x(1,n)=2*L;
           postParticles.y(1,n)=0;         
           % sample angle in [pi/2,pi]
           postParticles.h(1,n) = pi/2 + rand(1,1)*(pi/2);
       else
           postParticles.x(1,n)=2*L;
           postParticles.y(1,n)=L;        
           % sample angle in [-pi,-pi/2]
           postParticles.h(1,n) = -pi + rand(1,1)*(pi/2);
       end
       % robot B
       if (cornerB(n)==1)
           postParticles.x(2,n)=0;
           postParticles.y(2,n)=L;
           % sample angle in [-pi/2,0]
           postParticles.h(2,n) = -pi/2 + rand(1,1)*(pi/2);
       else
           postParticles.x(2,n)=0;
           postParticles.y(2,n)=0;   
           % sample angle in [0,pi/2]
           postParticles.h(2,n) = rand(1,1)*(pi/2);
       end
    end

    % and leave the function
    return;
end % end init

%% Mode 2: Estimator iteration.
% If init = 0, we perform a regular update of the estimator.

% Implement your estimator here!


%prior update (need to add wal bouncing)
[x, y, h] = prior_update(prevPostParticles.x, prevPostParticles.y, ...
    prevPostParticles.h, act, N);
postParticles.x = x;
postParticles.y = y;
postParticles.h = h;

%measurement update
distance=compute_distance(postParticles.x, postParticles.y, N);

%measurement likelihood (see description)
%check which sensors measured anything (!=Inf)
hasMeasurement=(sens~=Inf);
if sum(hasMeasurement) == 0
    % we cant do posterior update without measurements so exit function
%     disp('No measurements!');
    return;
else
    likelihood=compute_likelihood(distance, sens, hasMeasurement, N);
end

if(sum(likelihood)>0)
    %in this case everything is fine
    %normalize
    weights=likelihood/sum(likelihood);
else
    %in this case we basically have no clue anymore where we are (since all
    %probabilities are zero. Therefore we resample random uniform
    %particles and try posterior update until we get a likelihood > 0 and
    %then proceed
    disp('Likelhood 0!');
    while true
        %create uniform sampled particles
        disp('Resampling!');
        postParticles.x = 2*KC.L*rand(2,N);
        postParticles.y = KC.L*rand(2,N);
        postParticles.h = 2*pi*rand(2,N) - pi;
        %think about headings maybe (for now just leave them as we dont
        %measure them
        %compute distances
        distance=compute_distance(postParticles.x, postParticles.y, N);
        likelihood=compute_likelihood(distance, sens, hasMeasurement, N);
        if(sum(likelihood)>0)
            %in this case everything is fine
            %normalize
            weights=likelihood/sum(likelihood);
            break
        end
    end
end
% % add some noise to resampled particles after resampling
roughening = 1;
if ~roughening
    s=RandStream('mt19937ar');
    postParticles.x(1,:) = datasample(s, postParticles.x(1,:), N, 'Replace', true, 'Weights', weights);
    postParticles.x(2,:) = datasample(s, postParticles.x(2,:), N, 'Replace', true, 'Weights', weights);
    postParticles.y(1,:) = datasample(s, postParticles.y(1,:), N, 'Replace', true, 'Weights', weights);
    postParticles.y(2,:) = datasample(s, postParticles.y(2,:), N, 'Replace', true, 'Weights', weights);
    postParticles.h(1,:) = datasample(s, postParticles.h(1,:), N, 'Replace', true, 'Weights', weights);
    postParticles.h(2,:) = datasample(s, postParticles.h(2,:), N, 'Replace', true, 'Weights', weights);
else
    posvar = 0.0 * KC.L;
    headvar = 0.001 * 2*pi;
    %resample according to weights with replacement
    s=RandStream('mt19937ar');
    postParticles.x(1,:) = datasample(s, postParticles.x(1,:), N, 'Replace', true, 'Weights', weights)+posvar * randn(1,N);
    postParticles.x(2,:) = datasample(s, postParticles.x(2,:), N, 'Replace', true, 'Weights', weights)+posvar * randn(1,N);
    postParticles.y(1,:) = datasample(s, postParticles.y(1,:), N, 'Replace', true, 'Weights', weights)+posvar * randn(1,N);
    postParticles.y(2,:) = datasample(s, postParticles.y(2,:), N, 'Replace', true, 'Weights', weights)+posvar * randn(1,N);
    postParticles.h(1,:) = datasample(s, postParticles.h(1,:), N, 'Replace', true, 'Weights', weights)+ headvar * randn(1,N);
    postParticles.h(2,:) = datasample(s, postParticles.h(2,:), N, 'Replace', true, 'Weights', weights)+ headvar * randn(1,N);

    %make sure particles are still in box (because noise) and headings in
    %interval
    postParticles.x(postParticles.x < 0) = 0;
    postParticles.y(postParticles.y < 0) = 0;
    postParticles.x(postParticles.x > 2 * KC.L) = 2 * KC.L;
    postParticles.y(postParticles.y > KC.L) = KC.L;
    postParticles.h(postParticles.h > pi) = 2*pi - postParticles.h(postParticles.h > pi);
    postParticles.h(postParticles.h < -pi) = 2*pi + postParticles.h(postParticles.h < -pi);
end

end % end estimator

function [x_new, y_new, h_new] = prior_update(x, y, h, act, N)
    % model noise
%     us = rand(2,N);
%     vs = NaN(2,N);
%     vs(us < 1/2) = sqrt(2*us(us < 1/2)*KC.vsbar^2) - KC.vsbar;
%     vs(us >= 1/2) = KC.vsbar*(1 - sqrt(2*(1-us(us >= 1/2))));
    
    %pdfs
    pdfvs = makedist('Triangular', 'a',-KC.vsbar,'b',0,'c',KC.vsbar);
    vs=random(pdfvs, 2, N);
    act_N = repmat(act, 1, N);
    
    x_new = x + KC.ts.*act_N .* (ones(2,N) + vs) .* cos(h);
    y_new = y + KC.ts.*act_N .* (ones(2,N) + vs) .* sin(h);
    
    % loop over each particle
    % PROPER X AND Y UPDATE?
    h_new = h;
    for j =1:2
        for i = 1:size(x,2)
            % model noise
            c = 3/(2*KC.vbar^3);
            u = rand(1);
            vi = nthroot(3/c*u - KC.vbar^3, 3);

            %probably should not be elseif since it can bounce into 2 walls
            %(corner)
            if x_new(j,i) > 2*KC.L
                %h_new(i) = pi/2 - h + vi; i think this is wrong
                h_new(j,i) = (pi-h(j,i))*(1+vi);
                %x_new(i) = x_new(i) - 2*KC.L; i think this is wrong
                x_new(j,i)=4*KC.L-x_new(j,i);
            end
            if x_new(j,i) < 0
                h_new(j,i) = (pi - h(j,i))*(1 + vi);
                x_new(j,i) = -x_new(j,i);
            end
            if y_new(j,i) > KC.L
                h_new(j,i) = - h(j,i)*(1 + vi);
                %y_new(i) = y_new(i) - KC.L;i think this is wrong
                y_new(j,i)=2*KC.L-y_new(j,i);
            end
            if y_new(j,i) < 0
                h_new(j,i) = - h(j,i)*(1 + vi);
                y_new(j,i) = -y_new(j,i);
            end

        end
    end
    
    % heading in intervall
    h_new(h_new > pi) = 2*pi - h_new(h_new > pi);
    h_new(h_new < -pi) = 2*pi + h_new(h_new < -pi);
end

function [distance]=compute_distance(x, y, N)
    % 1:4 are distances of particles (1,:) to sensors 1 to 4. 
    % 5:8 the distances of particles (2,:) respectively
    distance=zeros(8,N); 
    distance(1,:)=sqrt((x(1,:)-2*KC.L*ones(1,N)).^2+y(1,:).^2);
    distance(2,:)=sqrt((x(1,:)-2*KC.L*ones(1,N)).^2+(y(1,:)-KC.L*ones(1,N)).^2);
    distance(3,:)=sqrt(x(1,:).^2+(y(1,:)-KC.L*ones(1,N)).^2);
    distance(4,:)=sqrt(x(1,:).^2+y(1,:).^2);
    distance(5,:)=sqrt((x(2,:)-2*KC.L*ones(1,N)).^2+y(2,:).^2);
    distance(6,:)=sqrt((x(2,:)-2*KC.L*ones(1,N)).^2+(y(2,:)-KC.L*ones(1,N)).^2);
    distance(7,:)=sqrt(x(2,:).^2+(y(2,:)-KC.L*ones(1,N)).^2);
    distance(8,:)=sqrt(x(2,:).^2+y(2,:).^2);
end

function [likelihood] = compute_likelihood(distance, sens, hasMeasurement, N)
    likelihood=ones(1,N); %likelhood of the measurements given two particles (one for A and one for B)
    %the two on top of each other are corresponding
    %pdf for measurements
    pdfwi = makedist('Triangular', 'a',-KC.wbar,'b',0,'c',KC.wbar);

    % Likelihood according to description
    % Sensor 1 & 2 - measuring robot A
    for i = 1:2
        if hasMeasurement(i)
            likelihood = likelihood .* ...
                (KC.sbar*pdf(pdfwi, sens(i)-distance(i+4,:))...
                + (1-KC.sbar)*pdf(pdfwi, sens(i)-distance(i,:)));
%             likelihood = likelihood .* (KC.sbar*...
%                 triangularPulse(-KC.wbar, KC.wbar, distance(i,:)-sens(i))...
%                 +(1-KC.sbar)*triangularPulse(-KC.wbar, KC.wbar, distance(i+4,:)-sens(i)));
        end
    end
    % Sensor 3 & 4 - measuring robot B
    for i = 3:4
        if hasMeasurement(i)
            likelihood = likelihood .* ...
                (KC.sbar*pdf(pdfwi, sens(i)-distance(i,:)) ...
                + (1-KC.sbar)*pdf(pdfwi, sens(i)-distance(i+4,:)));
%             likelihood = likelihood .* (KC.sbar...
%                 *triangularPulse(-KC.wbar, KC.wbar, distance(i+4,:)-sens(i))...
%                 +(1-KC.sbar)*triangularPulse(-KC.wbar, KC.wbar, distance(i,:)-sens(i)));
        end
    end
end

