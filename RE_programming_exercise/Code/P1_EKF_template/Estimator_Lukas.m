function [posEst,linVelEst,oriEst,driftEst,...
          posVar,linVelVar,oriVar,driftVar,estState] = ...
    Estimator(estState,actuate,sense,tm,estConst)
% [posEst,linVelEst,oriEst,driftEst,...
%    posVar,linVelVar,oriVar,driftVar,estState] = 
% Estimator(estState,actuate,sense,tm,estConst)
%
% The estimator.
%
% The function is initialized for tm == 0, otherwise the estimator does an
% iteration step (compute estimates for the time step k).
%
% Inputs:
%   estState        previous estimator state (time step k-1)
%                   May be defined by the user (for example as a struct).
%   actuate         control input u(k), [1x2]-vector
%                   actuate(1): u_t, thrust command
%                   actuate(2): u_r, rudder command
%   sense           sensor measurements z(k), [1x5]-vector, INF entry if no
%                   measurement
%                   sense(1): z_a, distance measurement a
%                   sense(2): z_b, distance measurement b
%                   sense(3): z_c, distance measurement c
%                   sense(4): z_g, gyro measurement
%                   sense(5): z_n, compass measurement
%   tm              time, scalar
%                   If tm==0 initialization, otherwise estimator
%                   iteration step.
%   estConst        estimator constants (as in EstimatorConstants.m)
%
% Outputs:
%   posEst          position estimate (time step k), [1x2]-vector
%                   posEst(1): p_x position estimate
%                   posEst(2): p_y position estimate
%   linVelEst       velocity estimate (time step k), [1x2]-vector
%                   linVelEst(1): s_x velocity estimate
%                   linVelEst(2): s_y velocity estimate
%   oriEst          orientation estimate (time step k), scalar
%   driftEst        estimate of the gyro drift b (time step k), scalar
%   posVar          variance of position estimate (time step k), [1x2]-vector
%                   posVar(1): x position variance
%                   posVar(2): y position variance
%   linVelVar       variance of velocity estimate (time step k), [1x2]-vector
%                   linVelVar(1): x velocity variance
%                   linVelVar(2): y velocity variance
%   oriVar          variance of orientation estimate (time step k), scalar
%   driftVar        variance of gyro drift estimate (time step k), scalar
%   estState        current estimator state (time step k)
%                   Will be input to this function at the next call.
%
%
% Class:
% Recursive Estimation
% Spring 2018
% Programming Exercise 1
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Matthias Hofer, Carlo Sferrazza
% hofermat@ethz.ch
% csferrazza@ethz.ch
%

%% Initialization
if (tm == 0)
    % Do the initialization of your estimator here!
    
    % initial state mean
    rsample = estConst.StartRadiusBound*rand(1);
    thetasample = 2*pi*rand(1);
    % uniformly sampling from disk need correction: http://mathworld.wolfram.com/DiskPointPicking.html
    posEst = [0,0];%[sqrt(rsample)*cos(thetasample),sqrt(rsample)*sin(thetasample)]; % 1x2 matrix
    linVelEst = [0,0]; % 1x2 matrix
    oriEst = 0;%-estConst.RotationStartBound + 2*estConst.RotationStartBound * rand(1); % 1x1 matrix
    driftEst = 0;%estConst.GyroDriftStartBound; % 1x1 matrix
    
    % initial state variance
    posVar = [0.25 * estConst.StartRadiusBound, 0.25 * estConst.StartRadiusBound]; % 1x2 matrix
    linVelVar = [0,0]; % 1x2 matrix
    oriVar = 1/3*estConst.RotationStartBound^2; % 1x1 matrix
    driftVar = 0; % 1x1 matrix
    
    % estimator variance init (initial posterior variance)
    estState.Pm = diag([posVar,linVelVar,oriVar,driftVar]);
    % estimator state
    estState.xm = [posEst,linVelEst,oriEst,driftEst];
    % time of last update
    estState.tm = tm;
    return;
end

%% Estimator iteration.
% get time since last estimator update
dt = tm - estState.tm;
estState.tm = tm; % update measurement update time

% prior update
sx = estState.xm(3);
sy = estState.xm(4);
phi = estState.xm(5);

alpha = -2*cos(phi)*estConst.dragCoefficient*sx;
beta = -2*cos(phi)*estConst.dragCoefficient*sy;
gamma = -sin(phi)*(tanh(actuate(1)) - estConst.dragCoefficient*(sx^2+sy^2));

alpha2 = -2*sin(phi)*estConst.dragCoefficient*sx;
beta2 = -2*sin(phi)*estConst.dragCoefficient*sy;
gamma2 = cos(phi)*(tanh(actuate(1)) - estConst.dragCoefficient*(sx^2+sy^2));

A = [0,0,1,0,0,0;
    0,0,0,1,0,0;
    0,0,alpha,beta,gamma,0;
    0,0,alpha2,beta2,gamma2,0;
    0,0,0,0,0,0;
    0,0,0,0,0,0];

L = [0,0,0;
    0,0,0;
    -cos(phi)*estConst.dragCoefficient*(sx^2+sy^2),0,0;
    -sin(phi)*estConst.dragCoefficient*(sx^2+sy^2),0,0;
    0,estConst.rudderCoefficient*actuate(2),0;
    0,0,1];

Q = [estConst.DragNoise,0,0;
    0,estConst.RudderNoise,0;
    0,0,estConst.GyroDriftNoise];

C = [estConst.dragCoefficient, estConst.rudderCoefficient];
x = ode45(@(t,y) odex(t,y,actuate,C), [estState.tm-dt,estState.tm], estState.xm);
xp = x.y(:,end);

P = ode45(@(t,y) odeP(t,y,A,Q,L), [estState.tm-dt,estState.tm], estState.Pm);
Pp = reshape(P.y(:,end),[6,6]);

% measurement update
sqrt_term_a = sqrt((xp(1)-estConst.pos_radioA(1))^2 + (xp(2)-estConst.pos_radioA(2))^2);
sqrt_term_b = sqrt((xp(1)-estConst.pos_radioB(1))^2 + (xp(2)-estConst.pos_radioB(2))^2);
sqrt_term_c = sqrt((xp(1)-estConst.pos_radioC(1))^2 + (xp(2)-estConst.pos_radioC(2))^2);

H = [(xp(1)-estConst.pos_radioA(1))/sqrt_term_a, (xp(2)-estConst.pos_radioA(2))/sqrt_term_a,0,0,0,0;
    (xp(1)-estConst.pos_radioB(1))/sqrt_term_b, (xp(2)-estConst.pos_radioB(2))/sqrt_term_b,0,0,0,0;
    (xp(1)-estConst.pos_radioC(1))/sqrt_term_c, (xp(2)-estConst.pos_radioC(2))/sqrt_term_c,0,0,0,0;
    0,0,0,0,1,1;
    0,0,0,0,1,0];
M = eye(5);

R = diag([estConst.DistNoiseA,estConst.DistNoiseB,estConst.DistNoiseC,estConst.GyroNoise,estConst.CompassNoise]);

hk = zeros(1,5);
hk(1) = sqrt_term_a;
hk(2) = sqrt_term_b;
hk(3) = sqrt_term_c;
hk(4) = xp(5)+xp(6);
hk(5) = xp(5);

temp = (H*Pp*H' + M*R*M')\eye(5);
K = Pp*H'* temp;
diff = (sense-hk)';
if (diff(3) == Inf)
    diff(3)=0;
end
estState.xm = xp + K*diff;

estState.Pm = (eye(6) - K*H)*Pp;

% Get resulting estimates and variances
% Output quantities
posEst = estState.xm(1:2);
linVelEst = estState.xm(3:4);
oriEst = estState.xm(5);
driftEst = estState.xm(6);

posVar = diag(estState.Pm(1:2,1:2));
linVelVar = diag(estState.Pm(3:4,3:4));
oriVar = estState.Pm(5,5);
driftVar = estState.Pm(6,6);

end
function dPdt = odeP(t,P,A,Q,L)
P = reshape(P,[6,6]);
a = A*P + P*A' + L*Q*L';
dPdt = a(:);
end
function dxdt = odex(t,x,u,C)
dxdt = zeros(6,1);
sx = x(3);
sy = x(4);
phi = x(5);
dxdt(1) = sx;
dxdt(2) = sy;
dxdt(3) = cos(phi)*(tanh(u(1))-C(1)*(sx^2+sy^2));
dxdt(4) = sin(phi)*(tanh(u(1))-C(1)*(sx^2+sy^2));
dxdt(5) = C(2)*u(2);
dxdt(6) = 0;

end
