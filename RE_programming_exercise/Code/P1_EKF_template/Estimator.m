function [posEst,linVelEst,oriEst,driftEst,...
          posVar,linVelVar,oriVar,driftVar,estState] = ...
    Estimator(estState,actuate,sense,tm,estConst)
% [posEst,linVelEst,oriEst,driftEst,...
%    posVar,linVelVar,oriVar,driftVar,estState] = 
% Estimator(estState,actuate,sense,tm,estConst)
%
% The estimator.
%
% The function is initialized for tm == 0, otherwise the estimator does an
% iteration step (compute estimates for the time step k).
%
% Inputs:
%   estState        previous estimator state (time step k-1)
%                   May be defined by the user (for example as a struct).
%   actuate         control input u(k), [1x2]-vector
%                   actuate(1): u_v, drive wheel angular velocity
%                   actuate(2): u_r, drive wheel angle
%   sense           sensor measurements z(k), [1x3]-vector, INF if no
%                   measurement (this is actually 1x5)
%                   sense(1): z_d, distance measurement
%                   sense(2): z_c, compass measurement
%                   sense(3): z_g, gyro measurement
%   tm              time, scalar
%                   If tm==0 initialization, otherwise estimator
%                   iteration step.
%   estConst        estimator constants (as in EstimatorConstants.m)
%
% Outputs:
%   posEst          position estimate (time step k), [1x2]-vector
%                   posEst(1): p_x position estimate
%                   posEst(2): p_y position estimate
%   linVelEst       velocity estimate (time step k), [1x2]-vector
%                   linVelEst(1): s_x velocity estimate
%                   linVelEst(2): s_y velocity estimate
%   oriEst          orientation estimate (time step k), scalar
%   driftEst        estimate of the gyro drift b (time step k), scalar
%   posVar          variance of position estimate (time step k), [1x2]-vector
%                   posVar(1): x position variance
%                   posVar(2): y position variance
%   linVelVar       variance of velocity estimate (time step k), [1x2]-vector
%                   linVelVar(1): x velocity variance
%                   linVelVar(2): y velocity variance
%   oriVar          variance of orientation estimate (time step k), scalar
%   driftVar        variance of gyro drift estimate (time step k), scalar
%   estState        current estimator state (time step k)
%                   Will be input to this function at the next call.
%
%
% Class:
% Recursive Estimation
% Spring 2018
% Programming Exercise 1
%
% --
% ETH Zurich
% Institute for Dynamic Systems and Control
% Raffaello D'Andrea, Matthias Hofer, Carlo Sferrazza
% hofermat@ethz.ch
% csferrazza@ethz.ch
%
% --
% Revision history
% [24.04.18, MH]    first version by Matthias & Carlo

%% Initialization
if (tm == 0)
    % Do the initialization of your estimator here!
    
    % initial state mean
    posEst = [0,0]; % 1x2 matrix center of the circle
    linVelEst = [0,0]; % 1x2 matrix exactly zero
    oriEst = [0]; % 1x1 matrix average between -phibar and +phibar
    driftEst = [0]; % 1x1 matrix gyroscope is bias free
    
    % initial state variance
    % posVar = estConst.StartRadiusBound*[pi, pi]; % 1x2 matrix why not 2x2
    %posVar = [0.25 * estConst.StartRadiusBound^2, 0.25 * estConst.StartRadiusBound^2]; %maybe squared maybe not
    posVar = [0.5 * estConst.StartRadiusBound^2 * pi, 0.5 * estConst.StartRadiusBound^2 * pi];
    linVelVar = [0,0]; % 1x2 matrix exactly zero
    % oriVar = [1/12*4*estConst.RotationStartBound]; % 1x1 matrix variance of uniform distribution
    oriVar = 1/3*estConst.RotationStartBound^2;
    driftVar = [0]; % 1x1 matrix %exactly zero
    
    % estimator variance init (initial posterior variance)
    estState.Pm = diag([posVar, linVelVar, oriVar, driftVar]); 
    % estimator state
    estState.xm = [posEst, linVelEst, oriEst, driftEst];
    % time of last update
    estState.tm = tm;
    return;
end

%% Estimator iteration.

% get time since last estimator update
dt = tm-estState.tm;
estState.tm = tm; % update measurement update time

% prior update
%define state dynamics here:
q1 = @(t,y)[y(3);
    y(4);
    cos(y(5))*(tanh(actuate(1))-estConst.dragCoefficient*(y(3)*y(3)+y(4)*y(4)));
    sin(y(5))*(tanh(actuate(1))-estConst.dragCoefficient*(y(3)*y(3)+y(4)*y(4)));
    estConst.rudderCoefficient*actuate(2);
    0];
sol = ode45(q1, [tm-dt, tm], estState.xm);
estState.xm = sol.y(:, end);

%define matrices for variance propagation here (see description)
A = [0, 0, 1, 0, 0, 0;
     0, 0, 0, 1, 0, 0;
     0, 0, -2*cos(estState.xm(5))*estConst.dragCoefficient*estState.xm(3), ...
     -2*cos(estState.xm(5))*estConst.dragCoefficient*estState.xm(4), ...
     -sin(estState.xm(5))*(tanh(actuate(1)) - estConst.dragCoefficient*(estState.xm(3)^2 + estState.xm(4)^2)), 0;
     0, 0, -2*sin(estState.xm(5))*estConst.dragCoefficient*estState.xm(3), ...
     -2*sin(estState.xm(5))*estConst.dragCoefficient*estState.xm(4), ...
     cos(estState.xm(5))*(tanh(actuate(1)) - estConst.dragCoefficient*(estState.xm(3)^2 + estState.xm(4)^2)), 0;
     0, 0, 0, 0, 0, 0;
     0, 0, 0, 0, 0, 0];% dq/dx
L = [0, 0, 0;
     0, 0, 0;
     -cos(estState.xm(5))*estConst.dragCoefficient*(estState.xm(3)^2 + estState.xm(4)^2), 0, 0;
     -sin(estState.xm(5))*estConst.dragCoefficient*(estState.xm(3)^2 + estState.xm(4)^2), 0, 0;
     0, estConst.rudderCoefficient*actuate(2), 0;
     0, 0, 1]; % dq/dv
Qc = diag([estConst.DragNoise, estConst.RudderNoise, estConst.GyroDriftNoise]); % variance of v
P = @(y) reshape(y, [6, 6]);
q2 = @(t,y) reshape(A*P(y) + P(y)*A' + L*Qc*L', [36, 1]);
sol = ode45(q2, [tm-dt, tm], reshape(estState.Pm, [36, 1]));
estState.Pm = reshape(sol.y(:, end), [6, 6]);

% measurement update

sqrt_term_a = sqrt((estState.xm(1)-estConst.pos_radioA(1))^2 ...
    + (estState.xm(2)-estConst.pos_radioA(2))^2);
sqrt_term_b = sqrt((estState.xm(1)-estConst.pos_radioB(1))^2 ...
    + (estState.xm(2)-estConst.pos_radioB(2))^2);
sqrt_term_c = sqrt((estState.xm(1)-estConst.pos_radioC(1))^2 ...
    + (estState.xm(2)-estConst.pos_radioC(2))^2);


%matrices
H = [(estState.xm(1)-estConst.pos_radioA(1))/sqrt_term_a, ...
    (estState.xm(2)-estConst.pos_radioA(2))/sqrt_term_a, 0, 0, 0, 0;
    
    (estState.xm(1)-estConst.pos_radioB(1))/sqrt_term_b, ...
    (estState.xm(2)-estConst.pos_radioB(2))/sqrt_term_b, 0, 0, 0, 0;
    
    (estState.xm(1)-estConst.pos_radioC(1))/sqrt_term_c, ...
    (estState.xm(2)-estConst.pos_radioC(2))/sqrt_term_c, 0, 0, 0, 0;
    
    0,0,0,0,1,1;
    0,0,0,0,1,0];
M = eye(5);
R = diag([estConst.DistNoiseA, estConst.DistNoiseB, estConst.DistNoiseC, ...
         estConst.GyroNoise, estConst.CompassNoise]);

h = [sqrt_term_a;
     sqrt_term_b;
     sqrt_term_c;
     estState.xm(5) + estState.xm(6);
     estState.xm(5)];
 
infidx = isinf(sense);
sense(infidx) = h(infidx); %removes inf->no mean update but have to take care of variance update

%Kalman Gain
LHS = H*estState.Pm*H' + M*R*M';
RHS = estState.Pm*H';
K = RHS/LHS; %better according to MATLAB
%kalman gain of INF must be 0
K(:,infidx) = 0;

estState.xm = estState.xm + K*(sense' - h);
estState.Pm = (eye(6) - K*H)*estState.Pm;
% Get resulting estimates and variances

% Output quantities
posEst = estState.xm(1:2);
linVelEst = estState.xm(3:4);
oriEst = estState.xm(5);
driftEst = estState.xm(6);

% TO-DO
Pm = diag(estState.Pm);

posVar = Pm(1:2);
linVelVar = Pm(3:4);
oriVar = Pm(5);
driftVar = Pm(6);

end