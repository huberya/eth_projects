function [ J_opt, u_opt_ind ] = LinearProgramming( P, G )
%LINEARPROGRAMMING Value iteration
%   Solve a stochastic shortest path problem by linear programming.
%
%   [J_opt, u_opt_ind] = LinearProgramming(P, G) computes the optimal cost
%   and the optimal control input for each state of the state space.
%
%   Input arguments:
%
%       P:
%           A (MN x MN x L) matrix containing the transition probabilities
%           between all states in the state space for all attainable
%           control inputs. The entry P(i, j, l) represents the transition
%           probability from state i to state j if control input l is
%           applied.
%
%       G:
%           A (MN x L) matrix containing the stage costs of all states in
%           the state space for all attainable control inputs. The entry
%           G(i, l) represents the cost if we are in state i and apply
%           control input l.
%
%   Output arguments:
%
%       J_opt:
%       	A (1 x MN) matrix containing the optimal cost-to-go for each
%       	element of the state space.
%
%       u_opt_ind:
%       	A (1 x MN) matrix containing the indices of the optimal control
%       	inputs for each element of the state space.

% put your code here

% Extract the sizes
NM = size(G,1);
L = size(P,3);

% Find Termination State (assuming only one exists)
tol_term = 1e-8;
term_ind = 0;
for k = 1:NM
    if sum(G(k,:)) < tol_term && abs(sum(P(k,k,:)) - L) < tol_term
        term_ind = k;
    end
end

for l = 1:L
    for k = 1:NM
        if sum(P(k,:,l))<tol_term
            P(k,k,l) = 1;
        end
    end
end

% Remove corresponding matrix entries
P = P([1:term_ind-1 term_ind+1:end], [1:term_ind-1 term_ind+1:end], :);
G = G([1:(term_ind-1) (term_ind+1):end],:);

% Update the size
NM = size(G,1);

% Define Matrix and Vectors for Linear Programming
f = -1 * ones(NM, 1);
A = zeros(L * NM, NM);
b = zeros(L * NM, 1);

% Assemble Matrix and RHS
for k = 1:L
   A((k-1) * NM + 1: k * NM,:) = eye(NM) - P(:,:,k); 
   b((k-1) * NM + 1: k * NM) = G(:,k);
end

% Do the Lin. Prog.
J_opt = linprog(f,A,b)';

% Compute the control inputs
u_opt_ind = ones(1, NM);
tol = 1e-8;
for k = 1:L
   % Find tight constraints
   res = abs(G(:,k) + P(:,:,k) * J_opt' - J_opt') < tol;
   ind = res;
   u_opt_ind(ind) = k;
end

% Reinsert Values for Termination State
u_opt_ind = [u_opt_ind(1:(term_ind-1)) 1 u_opt_ind(term_ind:end)];
J_opt = [J_opt(1:(term_ind-1)) 0 J_opt(term_ind:end)];

end

