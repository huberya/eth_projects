function posnew = SimulateStep(stateSpace, mazeSize, controlSpace, u_opt_ind, pos, P)
%make reproducible
N=mazeSize(1,1);
M=mazeSize(1,2);
[L,~]=size(stateSpace);
ind=0;
for i = 1:N*M
    if all(stateSpace(i,:)==pos)
        ind=i;
        break
    end
end
optL = u_opt_ind(ind);
%optAction = controlSpace(optL,:);
%create random number elem ]0,1[
randN=rand();
totalP=0;
for k = 1:M*N
    if P(ind,k,optL)+totalP>=randN
        posnew = stateSpace(k, :);
        return
    else
        totalP = totalP + P(ind,k,optL);
    end
end
%if it keeps stuck for some reason (maybe output error
posnew=pos;
end