% Artificial and simple example of a SSP

% Optimal Control Input should be: [1,2,1] (or: [1,2,2])
% Optimal Stage cost should be: [2.5,0.5,0]

P= [0.5,0.5,0;
    0.9,0,0.1;
    0,0,1];
P(:,:,2) = [0.5,0.5,0;
            0,0,1;
            0,0,1];
Q=[1,2;1,0.5;0,0];

size_of_P = size(P);
size_of_Q = size(Q);

[ J_opt, u_opt_ind ] = PolicyIteration(P,Q)

[ J_opt, u_opt_ind ] = ValueIteration(P,Q)

[ J_opt, u_opt_ind ] = LinearProgramming(P,Q)