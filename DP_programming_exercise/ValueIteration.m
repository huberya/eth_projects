function [ J_opt, u_opt_ind ] = ValueIteration( P, G )
%VALUEITERATION Value iteration
%   Solve a stochastic shortest path problem by value iteration.
%
%   [J_opt, u_opt_ind] = ValueIteration(P, G) computes the optimal cost and
%   the optimal control input for each state of the state space.
%
%   Input arguments:
%
%       P:
%           A (MN x MN x L) matrix containing the transition probabilities
%           between all states in the state space for all attainable
%           control inputs. The entry P(i, j, l) represents the transition
%           probability from state i to state j if control input l is
%           applied.
%
%       G:
%           A (MN x L) matrix containing the stage costs of all states in
%           the state space for all attainable control inputs. The entry
%           G(i, l) represents the cost if we are in state i and apply
%           control input l.
%
%   Output arguments:
%
%       J_opt:
%       	A (1 x MN) matrix containing the optimal cost-to-go for each
%       	element of the state space.
%
%       u_opt_ind:
%       	A (1 x MN) matrix containing the indices of the optimal control
%       	inputs for each element of the state space.

% put your code here

% Extract the sizes
NM = size(G,1);
L = size(P,3);

% Initialize V
V = zeros(NM, 1);
V_prev = V;

% Iterate
tol_iter = 1e-8;
max_iter = 1000000;
for i = 1:max_iter
    
    % Update V
    V = G(:,1) + P(:,:,1) * V_prev;
    for k = 2:L
        V = min(V, G(:,k) + P(:,:,k) * V_prev);
    end    
    
    % If new value is close to old, break loop.
    if norm(V - V_prev) < tol_iter
       break 
    end
    V_prev = V;
end

% V corresponds to J_opt
J_opt = V';

% Compute the control inputs similar as in LinProg.
u_opt_ind = zeros(1, NM);
tol = 1e-8;
for k = 1:L
   i=L+1-k;
   % Find tight constraints
   res = abs(G(:,i) + P(:,:,i) * J_opt' - J_opt') < tol;
   j = find(res);
   u_opt_ind(j) = i;
end

end

