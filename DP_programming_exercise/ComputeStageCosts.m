function G = ComputeStageCosts( stateSpace, controlSpace, mazeSize, walls, targetCell, holes, resetCell, p_f, c_p, c_r )
%COMPUTESTAGECOSTS Compute stage costs.
% 	Compute the stage costs for all states in the state space for all
%   attainable control inputs.
%
%   G = ComputeStageCosts(stateSpace, controlSpace, disturbanceSpace,
%   mazeSize, walls, targetCell) computes the stage costs for all states in
%   the state space for all attainable control inputs.
%
%   Input arguments:
%
%       stateSpace:
%           A (MN x 2) matrix, where the i-th row represents the i-th
%           element of the state space. Note that the state space also
%           contains the target cell, in order to simplify state indexing.
%
%       controlSpace:
%           A (L x 2) matrix, where the l-th row represents the l-th
%           element of the control space.
%
%       mazeSize:
%           A (1 x 2) matrix containing the width and the height of the
%           maze in number of cells.
%
%   	walls:
%          	A (2K x 2) matrix containing the K wall segments, where the start
%        	and end point of the k-th segment are stored in row 2k-1
%         	and 2k, respectively.
%
%    	targetCell:
%           WRONG its (1x2)
%          	A (2 x 1) matrix describing the position of the target cell in
%         	the maze.
%       holes:
%         	A (H x 2) matrix containg the H holes of the maze. Each row
%         	represents the position of a hole.
%
%   	resetCell:
%         	A (1 x 2) matrix describing the position of the reset cell in
%           the maze.
%
%       p_f:
%           The probability of falling into a hole when the ball is
%           traversing through or to a cell with a hole
%       
%       c_p:
%           Every time the ball bounces into a wall or boundary, we get this number 
%            of time steps as penalty.
%       c_r:
%           Every time the ball falls into a hole, the ball is set to the reset cell
%           at the beginning of the next stage and we get this number of time steps
%           as additional penalty.
%
%   Output arguments:
%
%       G:
%           A (MN x L) matrix containing the stage costs of all states in
%           the state space for all attainable control inputs. The entry
%           G(i, l) represents the cost if we are in state i and apply
%           control input l.

% put your code here
%idea treat border as wall!
[L,~]=size(controlSpace);
N=mazeSize(1,1); %width
M=mazeSize(1,2); %height
G=zeros(M*N,L);
%for simplicity make all positions row vectors
%Though it would be a lot smarter to have positions as column vectors
%since MATLAB is column major
%targetCell = transpose(targetCell);
%disp(targetCell)
%disp(resetCell)
%MATLAB saves matrices as column major
%for i1 = 1:N
%    for i2 = 1:M
for i = 1:M*N
        %in case position is terminal state remain there for all control
        %inputs and continue
        %if i1==A(1,1) && i2==A(1,2)
        if all(stateSpace(i,:) == targetCell(1,:))
            G(i, :) = 0;
            continue
        end
        for l = 1:L
            %check wether move is valid (i.e. there is no wall)
            %if invalid add no transition probabilities and continue
            if IsWall(stateSpace(i,:) , controlSpace(l,:), walls, M, N)==1
                G(i,l) = Inf;
                continue
            end
            %if valid compute probabilities to
            %also check for holes (if moving)
            remainingP=1;
            %there are 9 possible disturbances
            %foreach decide wether there is a wall or not and update
            %accordingly
            %not moving:
            if max(abs(controlSpace(l,:))) == 0
                %only handle disturbances
                tempPos = stateSpace(i,:);
                G = HandleDisturbances(tempPos, G, walls, holes, remainingP, p_f, c_r, c_p, i, l, M, N);
            elseif max(abs(controlSpace(l,:))) == 1
                %check for hole at new pos
                tempPos = stateSpace(i,:) + controlSpace(l,:);
                if IsHole(tempPos, holes) == 1
                    %probability to fall in hole
                    j = IndexofPos(resetCell, stateSpace);
                    G(i, l) = G(i, l) + remainingP*p_f*(c_r+1);
                    %update remaining probability
                    remainingP = remainingP - remainingP*p_f;                    
                end
                %handle disturbances
                G = HandleDisturbances(tempPos, G, walls, holes, remainingP, p_f, c_r, c_p, i, l, M, N);
            elseif max(abs(controlSpace(l,:))) == 2
                %check for hole at intermediate position
                tempPos = stateSpace(i,:)+0.5*controlSpace(l,:);
                if IsHole(tempPos, holes) == 1
                    %probability to fall in hole
                    j = IndexofPos(resetCell, stateSpace);
                    G(i, l) = G(i, l) + remainingP*p_f*(c_r+1);
                    %update remaining probability
                    remainingP = remainingP - remainingP*p_f;  
                end
                %check for hole at new pos
                tempPos = stateSpace(i,:) + controlSpace(l,:);
                if IsHole(tempPos, holes) == 1
                    %probability to fall in hole
                    j = IndexofPos(resetCell, stateSpace);
                    G(i, l) = G(i, l) + remainingP*p_f*(c_r+1);
                    %update remaining probability
                    remainingP = remainingP - remainingP*p_f;              
                end
                G = HandleDisturbances(tempPos, G, walls, holes, remainingP, p_f, c_r, c_p, i, l, M, N);
            end
        end
    %end
end
end



%function to check wether there is a wall from Pos to Pos+Control
%pos and control are (1,2) matrices
%walls (as above):
%          	A (2K x 2) matrix containing the K wall segments, where the start
%        	and end point of the k-th segment are stored in row 2k-1
%         	and 2k, respectively.
%returns 1 if there is a wall and zero otherwise
function T = IsWall(pos, control, walls, M, N)
[K,~]=size(walls);
K=K/2;
T = 0;
%handle border case
newpos = pos+control;
if min(newpos)<=0
    T = 1;
    return
end
if newpos(1,1) > N
    T = 1;
    return
end
if newpos(1,2) > M
    T = 1;
    return
end
for k = 1:K
    %for simplicity transform coordinates, i.e add 0.5 to wall coordinates
    %since wall (1,1) is the to right corner of the field (1,1)
    vec1 = transpose([control(1,:), 0]);
    vec2 = transpose([walls(2*k,:)-walls(2*k-1,:), 0]);
    %exclude parallel vectors
    if cross(vec1, vec2) == 0
        continue
    end
    A = [vec1(1:2), -vec2(1:2)];
    b = transpose(0.5*ones(1,2)+walls(2*k-1,:)-pos);
    y = A\b;
    if all(y>=0)&&all(y<=1)
        T=1;
        return
    end
end
return
end

%function to check wether there is a hole at position pos
%pos is a 1x2 matrix
%holes is a Hx2 matrix
%returns 1 if there is a hole and 0 otherwise
function T = IsHole(pos, holes)
[H,~]=size(holes);
T = 0;
for i = 1:H
    if pos(1,1) == holes(i,1) && pos(1,2) == holes(i,2)
        T = 1;
        return
    end
end
return
end

%function to handle disturbances
%returns the new Probability transition matrix when the ball is at position
%pos and only applys disturbances from now on (controls treated before)
%startIndex is index BEFORE applying control input
function Gnew = HandleDisturbances(pos, G, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N)
Gnew = G;
%w=(0,0)
Gnew(startIndex, controlIndex) = Gnew(startIndex,  controlIndex)+ 1/9*remainingP;
%w=(0,1)
w=[0,1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(1,0)
w=[1,0];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(1,1)
w=[1,1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(0,-1)
w=[0,-1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(-1,0)
w=[-1,0];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(-1,-1)
w=[-1,-1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(1,-1)
w=[1,-1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);

%w=(-1,1)
w=[-1,1];
Gnew = HandlefixedDisturbance(pos, w, Gnew, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N);
end

function Gnew = HandlefixedDisturbance(pos, w, G, walls, holes, remainingP, p_f, c_r, c_p, startIndex, controlIndex, M, N)
Gnew = G;
%disturbance bounces into wall
if IsWall(pos, w, walls, M, N) == 1
    %bounces back into hole
    if IsHole(pos, holes) == 1
        %chance to fall (first part) chance to not fall (second part)
        Gnew(startIndex, controlIndex) = Gnew(startIndex, controlIndex) + 1/9*(p_f*remainingP*(1+c_r+c_p)+(1-p_f)*remainingP*(1+c_p));
    else
        %will bounce back
        Gnew(startIndex, controlIndex) = Gnew(startIndex, controlIndex) + 1/9*remainingP*(1+c_p);
    end
%no wall
else
    posnew=pos+w;
    %new field has a hole
    if IsHole(posnew, holes) == 1
        %chance to fall
        Gnew(startIndex, controlIndex) = Gnew(startIndex, controlIndex) + 1/9*p_f*remainingP*(1+c_r);
        %chance to not fall
        Gnew(startIndex, controlIndex) = Gnew(startIndex, controlIndex) + 1/9*(1-p_f)*remainingP;
    else
        Gnew(startIndex, controlIndex) = Gnew(startIndex, controlIndex) + 1/9*remainingP;
    end
end
return
end

function I = IndexofPos(pos, stateSpace)
[MN,~] = size(stateSpace);
I = 0;
for i = 1:MN
    if all(pos == stateSpace(i,:))
        I = i;
        return
    end
end
if I == 0
    msg = 'position not found in state space. This is an error in construction';
    error(msg)
end
end



