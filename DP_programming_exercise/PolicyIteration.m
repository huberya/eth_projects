function [ J_opt, u_opt_ind ] = PolicyIteration( P, G )
%POLICYITERATION Value iteration
%   Solve a stochastic shortest path problem by policy iteration.
%
%   [J_opt, u_opt_ind] = PolicyIteration(P, G) computes the optimal cost and
%   the optimal control input for each state of the state space.
%
%   Input arguments:
%
%       P:
%           A (MN x MN x L) matrix containing the transition probabilities
%           between all states in the state space for all attainable
%           control inputs. The entry P(i, j, l) represents the transition
%           probability from state i to state j if control input l is
%           applied.
%
%       G:
%           A (MN x L) matrix containing the stage costs of all states in
%           the state space for all attainable control inputs. The entry
%           G(i, l) represents the cost if we are in state i and apply
%           control input l.
%
%   Output arguments:
%
%       J_opt:
%       	A (1 x MN) matrix containing the optimal cost-to-go for each
%       	element of the state space.
%
%       u_opt_ind:
%       	A (1 x MN) matrix containing the indices of the optimal control
%       	inputs for each element of the state space.

% put your code here

% Extract the sizes
NM = size(G,1);
L = size(P,3);

% Find Termination State (assuming only one exists)
tol_term = 1e-8;
term_ind = 0;
for k = 1:NM
    if sum(G(k,:)) < tol_term && abs(sum(P(k,k,:)) - L) < tol_term
        term_ind = k;
    end    
end

% Remove corresponding matrix entries
P = P([1:term_ind-1 term_ind+1:end], [1:term_ind-1 term_ind+1:end], :);
G = G([1:(term_ind-1) (term_ind+1):end],:);

% Update the size
NM = size(G,1);

% Initialize Matrices and Vectors
J_mu = zeros(NM, 1);
J_mu_prev = J_mu;
P_mu = zeros(NM, NM);
G_mu = zeros(NM, 1);

% Initialize with proper policy
mu = ones(1, NM);

% Iterate
tol_iter = 1e-8;
max_iter = 1000000;
for i = 1:max_iter
    
    % Step 1
    for k = 1:NM
       P_mu(k,:) = P(k,:,mu(k)); 
       G_mu(k) = G(k,mu(k));
    end
    
    % Solve LSE
    J_mu = (eye(NM) - P_mu) \ G_mu;
    
    % Step 2
    mu = ones(1,NM);
    currmin = G(:,1) + P(:,:,1) * J_mu;
    for k = 2:L
       % Update the entries for which control input k is better.
       mu(currmin > G(:,k) + P(:,:,k) * J_mu) = k;
       currmin = min(currmin, G(:,k) + P(:,:,k) * J_mu);
    end
    
    % If new value is close to old, break loop.
    if norm(J_mu - J_mu_prev) < tol_iter
       break
    end
    J_mu_prev = J_mu;
end

% Assign correct quantities
J_opt = J_mu';
u_opt_ind = mu;

% Reinsert Values for Termination State
u_opt_ind = [u_opt_ind(1:(term_ind-1)) 1 u_opt_ind(term_ind:end)];
J_opt = [J_opt(1:(term_ind-1)) 0 J_opt(term_ind:end)];

end

